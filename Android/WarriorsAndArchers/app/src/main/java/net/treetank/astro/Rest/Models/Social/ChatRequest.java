package net.treetank.astro.Rest.Models.Social;

import java.util.List;

/**
 * Created by Jacek on 04.02.2017.
 */

public class ChatRequest {
    public int requestType;
    public int channel;
    public int kingdom_id;
    public String last_update;
    public String text;
    public ChatRequest(int requestType, int channel, int kingdom_id, String last_update, String text) {
        this.requestType = requestType;
        this.channel = channel;
        this.kingdom_id = kingdom_id;
        this.last_update = last_update;
        this.text = text;
    }
}
