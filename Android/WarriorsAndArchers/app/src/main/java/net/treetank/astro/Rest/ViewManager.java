package net.treetank.astro.Rest;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;

import net.treetank.astro.Activities.RegisterUserActivity;
import net.treetank.astro.Activities.SplashActivity;
import net.treetank.astro.Activities.SummaryActivity;
import net.treetank.astro.Activities.WrongServer;
import net.treetank.astro.Fragments.HeaderFragment;
import net.treetank.astro.Fragments.WarriorsFragment;

import java.util.ArrayList;

/**
 * Created by Jacek on 29.01.2017.
 */

public class ViewManager {
    public Activity activity;
    public WarriorsFragment fragment;
    public HeaderFragment hf;

    public ViewManager() {

    }

    public void goToSummaryActivity() {
        Activity temp = this.activity;
        Intent intent = new Intent(temp, SummaryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        temp.startActivity(intent);
        temp.finish();
    }

    public void goToRegisterActivity(String mEmail, String auth) {
        Activity temp = this.activity;
        Intent intent = new Intent(temp, RegisterUserActivity.class);
        intent.putExtra("email", mEmail);
        intent.putExtra("auth", auth);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        temp.startActivity(intent);
        temp.finish();
    }

    public void goToServerSelector() {
        Activity temp = this.activity;
        Intent intent = new Intent(temp, WrongServer.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        temp.startActivity(intent);
        temp.finish();
    }
}
