package net.treetank.astro.Rest.Callbacks;

import android.widget.Toast;

import net.treetank.astro.Rest.Models.Login.Login;
import net.treetank.astro.Rest.Models.Login.LoginResult;
import net.treetank.astro.Rest.RestClient;
import net.treetank.astro.Utils.GlobalData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jack on 06.05.2018.
 */

class ReLoginCallback implements Callback<LoginResult>{
    String username;
    String idToken;
    int kingdomId;
    private Runnable run;
    public ReLoginCallback(String username, String idToken, int kingdomId) {
        this.username = username;
        this.idToken = idToken;
        this.kingdomId = kingdomId;
    }

    public void call(Runnable run) {
        this.run = run;
        RestClient.get().loginUser(new Login(username, idToken, kingdomId)).enqueue(this);
    }
    @Override
    public void onResponse(Call<LoginResult> call, Response<LoginResult> response) {
        if(run!=null)
            run.run();
    }

    @Override
    public void onFailure(Call<LoginResult> call, Throwable t) {
        Toast.makeText(GlobalData.getViewManager().activity, "Couldn't reconnect.", Toast.LENGTH_LONG).show();
    }
}
