package net.treetank.astro.Rest.Models.Diplomacy;

import java.util.List;

/**
 * Created by Jacek on 05.03.2017.
 */

public class DiplomaticResponse {
    public int requestType;
    public int responseType;
    public String Recount;
    public Group group;
    public List<Group> groups;
    public Battle battle;
    public List<Battle> battles;
}
