package net.treetank.astro.Rest.Models.Diplomacy;

import net.treetank.astro.Rest.Models.Economy.Summary;

/**
 * Created by Jacek on 05.03.2017.
 */

public class Report {
    public String title;
    public String post_time;
    public String message;
}
