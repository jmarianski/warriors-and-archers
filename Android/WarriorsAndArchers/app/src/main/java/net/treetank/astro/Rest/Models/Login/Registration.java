package net.treetank.astro.Rest.Models.Login;

import java.util.Locale;

public class Registration {
    @SuppressWarnings("unused")
    private String lang;
    @SuppressWarnings("unused")
    private String username;
    @SuppressWarnings("unused")
    private String kingdomName;
    @SuppressWarnings("unused")
    private String auth;
    @SuppressWarnings("unused")
    private int id_solar_system;
    @SuppressWarnings("unused")
    private int idrace;

    public Registration(String username, String auth, String kingdomName, int solar, int race, String lang) {
        this.username = username;
        this.auth = auth;
        this.kingdomName = kingdomName;
        this.id_solar_system = solar;
        this.idrace = race;
        this.lang = lang;
    }
}
