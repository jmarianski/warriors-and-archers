package net.treetank.astro.Rest.Models.Login;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import net.treetank.astro.R;

/**
 * Created by Jack on 04.02.2018.
 */

public class Solar_system {
    @SerializedName("id_solar_system")
    public int id;
    String name;
    public String lang;
    String average_size;
    String number_of_players;

    @Override
    public String toString() {
        return name +" ("+lang+")";
    }

    public String detailedToString(Context c) {
        return String.format(c.getString(R.string.Solar_system_help), name, lang, number_of_players, average_size);
    }
}
