package net.treetank.astro.Fragments.Other;

import android.view.animation.Interpolator;

/**
 * Created by Jack on 05.05.2018.
 */

public class RandomSinusInterpolator implements Interpolator {
    private float constant = (float) Math.random();
    @Override
    public float getInterpolation(float v) {
        float val = (float) (2 + Math.sin(v*constant*16*Math.PI) + Math.sin(v*16*Math.PI))/4;
        return val;
    }
}
