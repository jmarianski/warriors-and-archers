package net.treetank.astro.Rest.Callbacks;


import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import net.treetank.astro.Fragments.ChatFragment;
import net.treetank.astro.Rest.Models.ChatManager;
import net.treetank.astro.Rest.Models.Social.Channel;
import net.treetank.astro.Rest.Models.Social.ChatRequest;
import net.treetank.astro.Rest.Models.Social.ChatResponse;
import net.treetank.astro.Rest.RestClient;
import net.treetank.astro.Utils.GlobalData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jacek on 06.02.2017.
 */

public class ChatCallback implements Callback<ChatResponse> {

    private int tries = 0;
    private ChatRequest r;

    @Override
    public void onResponse(Call<ChatResponse> call, Response<ChatResponse> response) {
        if(response.code()==403)
            retry();
        else if(response.body().requestType==0 && response.body().message.equals("ok"))
            GlobalData.chat.finishedLoadingChannels(response.body().channels);
        else if(response.body().requestType==1 || response.body().requestType==2) {
            tries = 0;
            GlobalData.chat.gotChannel(response.body());
        }
    }

    public void autoUpdate() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                ChatManager cm = GlobalData.chat;
                if(cm.refreshThreads<1 && GlobalData.getViewManager().fragment instanceof ChatFragment) {
                    cm.refreshThreads++;
                    Channel c = GlobalData.chat.channel;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    cm.refreshThreads--;
                    if(c==GlobalData.chat.channel && c!=null)
                        getMessages();
                }
            }
        }).start();
    }

    @Override
    public void onFailure(Call<ChatResponse> call, Throwable t) {
        fail();
    }

    public void fail() {
        if(GlobalData.getViewManager().fragment instanceof ChatFragment && GlobalData.chat.channel!=null) {
            if(tries<3)
                autoUpdate();
            tries++;
        }
    }

    private void retry() {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(GlobalData.getViewManager().activity);
        if(account!=null)
            new ReLoginCallback(GlobalData.getSummary().getUsername(), account.getIdToken(), GlobalData.getSummary().getKingdomId()).call(new Runnable() {
                @Override
                public void run() {
                    RestClient.get().chatRequest(r).enqueue(ChatCallback.this);
                }
            });
    }

    public void call(int requestType, int channel, int kingdom_id, String last_update, String text) {
        r = new ChatRequest(requestType, channel, kingdom_id, last_update, text);
        RestClient.get().chatRequest(r).enqueue(this);
    }

    public void getChannels() {
        call(0, 0, GlobalData.getSummary().getKingdomId(), "", null);
    }

    public void getMessages() {
        Channel channel = GlobalData.chat.channel;
        call(1, channel.id, GlobalData.getSummary().getKingdomId(), channel.local_last_update, null);
    }

    public void sendMessage(String text) {
        Channel channel = GlobalData.chat.channel;
        call(2, channel.id, GlobalData.getSummary().getKingdomId(), channel.local_last_update, text);
    }

}
