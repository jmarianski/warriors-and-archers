package net.treetank.astro.Fragments;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import net.treetank.astro.Logic.BuyLandLogic;
import net.treetank.astro.Utils.GlobalData;
import net.treetank.astro.R;

import android.support.v4.app.Fragment;

import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class BuyLandFragment extends WarriorsFragment {
    @Bind(R.id.land_change)
    public EditText landChangeEditText;
    @Bind(R.id.tv1)
    public TextView textView1;
    @Bind(R.id.tv2)
    public TextView textView2;
    @Bind(R.id.tv3)
    public TextView textView3;
    @Bind(R.id.tv4)
    public TextView textView4;

    @Bind({ R.id.b1,
            R.id.b10,
            R.id.b100,
            R.id.b1k,
            R.id.b10k,
            R.id.b100k,
            R.id.b1m,
            R.id.reset})
    public List<Button> buyButtons;

    @BindString(R.string.BuyLand_1)
    public String buyLand1;
    @BindString(R.string.BuyLand_2)
    public String buyLand2;
    @BindString(R.string.BuyLand_4)
    public String buyLand4;
    @BindString(R.string.BuyLand_5)
    public String buyLand5;

    private BuyLandLogic logic;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void updateUI() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buy_land, container, false);
        ButterKnife.bind(this, view);

        logic = new BuyLandLogic();

        populateViewWithData();

        if(GlobalData.isBlockButtons()) {
            for (Button button : buyButtons) {
                button.setEnabled(false);
            }
        }

        return view;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void populateViewWithData() {
        landChangeEditText.setText(String.format("%d", GlobalData.getCurrentTurn().getResources().getLand()));

        int gold = GlobalData.getSummary().getResources().getGold();
        textView1.setText(String.format(buyLand1, gold));

        int displayValue = logic.generateLandPossibleToBuy();
        textView2.setText(String.format(buyLand2, displayValue));

        int landValueInt2 = GlobalData.getSummary().getResources().getLand() + GlobalData.getCurrentTurn().getResources().getLand();
        textView3.setText(String.format(buyLand4, landValueInt2));

        int goldValueInt2 = GlobalData.getCurrentTurn().getResources().getGold();
        textView4.setText(String.format(buyLand5, -goldValueInt2));
    }

    @OnClick({ R.id.b1,
            R.id.b10,
            R.id.b100,
            R.id.b1k,
            R.id.b10k,
            R.id.b100k,
            R.id.b1m })
    public void onClickBuyValue(Button button) {
        switch(button.getId()) {
            case R.id.b1: {
                add(1);
                break;
            }
            case R.id.b10: {
                add(10);
                break;
            }
            case R.id.b100: {
                add(100);
                break;
            }
            case R.id.b1k: {
                add(1000);
                break;
            }
            case R.id.b10k: {
                add(10000);
                break;
            }
            case R.id.b100k: {
                add(100000);
                break;
            }
            case R.id.b1m: {
                add(1000000);
                break;
            }
        }
    }

    @OnClick(R.id.reset)
    public void onClickReset() {
        reset();
    }

    @OnEditorAction(R.id.land_change)
    public boolean onLandChangeEdit(int actionId, KeyEvent event) {
        if(actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
            updateLogicAndStrings(currentValue());
        }
        return false;
    }

    private int currentValue() {
        return Integer.parseInt(0 + landChangeEditText.getText().toString());
    }

    private void add(int value) {
        updateLogicAndStrings(currentValue() + value);
    }
    private void reset() {
        updateLogicAndStrings(0);
    }

    private void updateLogicAndStrings(int val) {
        int value = logic.setNewLand(val);
        landChangeEditText.setText(String.format("%d", value));
        int goldConsumed = logic.generateConsumedGold(value);
        int newLand = GlobalData.getSummary().getResources().getLand() + value;
        logic.updateStrings(goldConsumed, value);
        textView3.setText(String.format(buyLand4, newLand));
        textView4.setText(String.format(buyLand5, goldConsumed));
    }

}
