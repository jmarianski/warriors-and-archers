package net.treetank.astro.Rest.Models.Diplomacy;

import com.google.gson.annotations.SerializedName;

import net.treetank.astro.Utils.BuildingType;

/**
 * Created by Jacek on 17.03.2017.
 */

public class Soldiers {
    @SerializedName("building")
    public String building;
    BuildingType btype;
    @SerializedName("number")
    public int number;
    @SerializedName("advancement")
    double advancement;
    public Soldiers() {}
    Soldiers(BuildingType building, int number, double advancement) {
        this.btype = building;
        this.building = building.toString();
        this.number = number;
        this.advancement = advancement;
    }

    @Override
    public String toString() {
        return getType().toString()+" "+building+" "+number+" "+advancement;
    }

    public BuildingType getType() {
        return BuildingType.fromString(this.building);
    }

}
