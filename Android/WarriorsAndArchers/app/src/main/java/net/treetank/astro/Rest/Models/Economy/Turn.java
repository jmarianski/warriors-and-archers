package net.treetank.astro.Rest.Models.Economy;

import com.google.gson.annotations.SerializedName;
import net.treetank.astro.Utils.BuildingType;

import java.util.ArrayList;
import java.util.List;

public class Turn {
    @SerializedName("Login")
    @SuppressWarnings("unused")
    private String login;
    @SerializedName("KingdomName")
    private String kingdom;
    @SerializedName("Id")
    private int id;
    @SerializedName("Turns")
    @SuppressWarnings("unused")
    private int turns;
    @SerializedName("Turn")
    @SuppressWarnings("unused")
    private int turn;
    @SerializedName("Buildings")
    private List<Building> buildings;
    @SerializedName("Resources")
    private Resources resources;
    @SerializedName("Recount")
    private String recount;

    public Turn(String login, int kingdom, String recount, int turns) {
        this.login = login;
        this.id = kingdom;
        this.turns = 1;
        this.turn = turns;
        this.recount = recount;
        List<Building> buildings = new ArrayList<>();
        buildings.add(new Building(BuildingType.HOUSE, 0, 0, 0.0f));
        buildings.add(new Building(BuildingType.FIELD, 0, 0, 0.0f));
        buildings.add(new Building(BuildingType.GOLDMINE, 0, 0, 0.0f));
        buildings.add(new Building(BuildingType.MASONS_WORKSHOP, 0, 0, 0.0f));
        buildings.add(new Building(BuildingType.BUILDERS_GUILD, 0, 0, 0.0f));
        buildings.add(new Building(BuildingType.ARMORY, 0, 0, 0.0f));
        buildings.add(new Building(BuildingType.UNIVERSITY, 0, 0, 0.0f));
        buildings.add(new Building(BuildingType.BARRACKS, 0, 0, 0.0f));
        buildings.add(new Building(BuildingType.ARCHERHOUSE, 0, 0, 0.0f));
        this.buildings = buildings;
        this.resources = new Resources(0, 0, 0, 0, 0, 0, 0, 0);
    }

    public Turn(Turn turn) {
        this.login = turn.login;
        this.kingdom = turn.kingdom;
        this.id = turn.id;
        this.turns = turn.turns;
        this.turn = turn.turn;
        this.recount = turn.recount;
        List<Building> buildings = new ArrayList<>();
        Building building = turn.getBuilding(BuildingType.HOUSE);
        buildings.add(new Building(BuildingType.HOUSE, building.getNumber(), building.getEmployment(), building.getAdvancement()));
        building = turn.getBuilding(BuildingType.FIELD);
        buildings.add(new Building(BuildingType.FIELD, building.getNumber(), building.getEmployment(), building.getAdvancement()));
        building = turn.getBuilding(BuildingType.GOLDMINE);
        buildings.add(new Building(BuildingType.GOLDMINE, building.getNumber(), building.getEmployment(), building.getAdvancement()));
        building = turn.getBuilding(BuildingType.MASONS_WORKSHOP);
        buildings.add(new Building(BuildingType.MASONS_WORKSHOP, building.getNumber(), building.getEmployment(), building.getAdvancement()));
        building = turn.getBuilding(BuildingType.BUILDERS_GUILD);
        buildings.add(new Building(BuildingType.BUILDERS_GUILD, building.getNumber(), building.getEmployment(), building.getAdvancement()));
        building = turn.getBuilding(BuildingType.ARMORY);
        buildings.add(new Building(BuildingType.ARMORY, building.getNumber(), building.getEmployment(), building.getAdvancement()));
        building = turn.getBuilding(BuildingType.UNIVERSITY);
        buildings.add(new Building(BuildingType.UNIVERSITY, building.getNumber(), building.getEmployment(), building.getAdvancement()));
        building = turn.getBuilding(BuildingType.BARRACKS);
        buildings.add(new Building(BuildingType.BARRACKS, building.getNumber(), building.getEmployment(), building.getAdvancement()));
        building = turn.getBuilding(BuildingType.ARCHERHOUSE);
        buildings.add(new Building(BuildingType.ARCHERHOUSE, building.getNumber(), building.getEmployment(), building.getAdvancement()));
        this.buildings = buildings;
        this.resources = new Resources(turn.resources.getHappiness(),
                turn.resources.getGold(),
                turn.resources.getFood(),
                turn.resources.getLuxury(),
                turn.resources.getBricks(),
                turn.resources.getArmaments(),
                turn.resources.getPeople(),
                turn.resources.getLand());
    }

    public Building getBuilding(BuildingType buildingType) {
        for (Building building : buildings)
            if (building.getType() == buildingType)
                return building;
        return null;
    }

    public List<Building> getBuildings() {
        return buildings;
    }

    public Resources getResources() {
        return resources;
    }

    public void setResources(Resources resources) {
        this.resources = resources;
    }

    /**
     * Clears object. Useful when turn was accepted.
     */
    public void clear() {
        for(Building b:buildings) {
            b.setNumber(0);
            b.setEmployment(0);
            b.setAdvancement(0);
        }
        resources.clear();
    }
    public void setTurn(int turn) {
        this.turn = turn;
    }
}
