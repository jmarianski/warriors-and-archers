package net.treetank.astro.Logic;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Economy.Building;
import net.treetank.astro.Utils.GlobalData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jack on 14.04.2018.
 */

public class ResourcesLogic {
    private ArrayList<String> resource_types;
    private ArrayList<Integer> textview_id;
    private ArrayList<Integer> resource_names;
    private ArrayList<Integer> resource_graphics;
    private ArrayList<Integer> resource_description;


    public ResourcesLogic() {
        fillArraysWithData();
    }

    private void fillArraysWithData() {
        resource_types = new ArrayList<>();
        resource_types.add("Land");
        resource_types.add("People");
        resource_types.add("Happiness");
        resource_types.add("Gold");
        resource_types.add("Food");
        resource_types.add("Bricks");
        resource_types.add("Bcap");
        resource_types.add("Armaments");
        resource_types.add("Science");
        resource_names = new ArrayList<>();
        resource_names.add(R.string.Land);
        resource_names.add(R.string.People);
        resource_names.add(R.string.Happiness);
        resource_names.add(R.string.Gold);
        resource_names.add(R.string.Food);
        resource_names.add(R.string.Bricks);
        resource_names.add(R.string.Building_capability);
        resource_names.add(R.string.Armaments);
        resource_names.add(R.string.Science);
        resource_description = new ArrayList<>();
        resource_description.add(R.string.Land_desc);
        resource_description.add(R.string.People_desc);
        resource_description.add(R.string.Happiness_desc);
        resource_description.add(R.string.Gold_desc);
        resource_description.add(R.string.Food_desc);
        resource_description.add(R.string.Bricks_desc);
        resource_description.add(R.string.Building_capability_desc);
        resource_description.add(R.string.Armaments_desc);
        resource_description.add(R.string.Science_desc);
        textview_id = new ArrayList<>();
        textview_id.add(R.id.land);
        textview_id.add(R.id.people);
        textview_id.add(R.id.happiness);
        textview_id.add(R.id.gold);
        textview_id.add(R.id.food);
        textview_id.add(R.id.bricks);
        textview_id.add(R.id.bcap);
        textview_id.add(R.id.armaments);
        textview_id.add(R.id.science);
        resource_graphics = new ArrayList<>();
        resource_graphics.add(R.drawable.land);
        resource_graphics.add(R.drawable.people);
        resource_graphics.add(R.drawable.happiness);
        resource_graphics.add(R.drawable.gold);
        resource_graphics.add(R.drawable.food);
        resource_graphics.add(R.drawable.bricks);
        resource_graphics.add(R.drawable.bcap);
        resource_graphics.add(R.drawable.armaments);
        resource_graphics.add(R.drawable.science);
    }

    public ArrayList<String> getList() {
        return resource_types;
    }

    public int index(String code) {
        return resource_types.indexOf(code);
    }

    public String getName(Context context, String code) {
        int index = index(code);
        return getString(context, resource_names, index);
    }

    public String getDescription(Context context, String code) {
        int index = index(code);
        return getString(context, resource_description, index);
    }

    public Drawable getDrawable(Context context, String code) {
        int index = index(code);
        if(index>=0)
            return context.getResources().getDrawable(resource_graphics.get(index));
        else
            return context.getResources().getDrawable(R.drawable.no_image);
    }

    private String getString(Context context, ArrayList<Integer> ids, int id) {
        if(id>=0)
            return context.getString(ids.get(id));
        else
            return context.getString(R.string.no_resource);
    }

    public ArrayList<Integer> getTextViewIds() {
        return textview_id;
    }

    public String getCodeForTextViewId(int id) {
        int index = textview_id.indexOf(id);
        return getList().get(index);
    }

    public int getConsumption(String type) {
        float sum = 0;
        // positive changes in this turn resulting in resource consumption
        for(Building b:GlobalData.getCurrentTurn().getBuildings()) {
            for(Map.Entry<String, Float> e:GlobalData.getRules().getRulesFloat("cost_add_building", "Building").entrySet()) {
                if(e.getKey().equals(type) && b.getNumber()>0)
                    sum += e.getValue()*b.getNumber();
            }
            for(Map.Entry<String, Float> e:GlobalData.getRules().getRulesFloat("cost_add_building", b.getType().toString()).entrySet()) {
                if(e.getKey().equals(type) && b.getNumber()>0)
                    sum += e.getValue()*b.getNumber();
            }
            for(Map.Entry<String, Float> e:GlobalData.getRules().getRulesFloat("cost_add_employee", "Employee").entrySet()) {
                if(e.getKey().equals(type) && b.getEmployment()>0)
                    sum += e.getValue()*b.getEmployment();
            }
            for(Map.Entry<String, Float> e:GlobalData.getRules().getRulesFloat("cost_add_employee", b.getType().toString()).entrySet()) {
                if(e.getKey().equals(type) && b.getEmployment()>0)
                    sum += e.getValue()*b.getEmployment();
            }
        }
        // overall consumption for having this type of building / employee per turn (currently: just employee)
        for(Building b:GlobalData.getSummary().getBuildings()) {
            for(Map.Entry<String, Float> e:GlobalData.getRules().getRulesFloat("turn_cost", "Employee").entrySet()) {
                if(e.getKey().equals(type) && b.getEmployment()>0)
                    sum += e.getValue()*b.getEmployment();
            }
            for(Map.Entry<String, Float> e:GlobalData.getRules().getRulesFloat("turn_cost", b.getType().toString()).entrySet()) {
                if(e.getKey().equals(type) && b.getEmployment()>0)
                    sum += e.getValue()*b.getEmployment();
            }
        }
        sum += GlobalData.getRules().getRuleFloat("turn_cost", "Person", type) * GlobalData.getSummary().getResources().get("People");
        if(type.equals("Gold")) // to calculate costs of land, we use prepared formula
            sum += generateConsumedGoldPerLand(GlobalData.getCurrentTurn().getResources().get("Land"));
        return (int) sum;
    }

    private int generateConsumedGoldPerLand(int value) {
        int x = GlobalData.getRules().getRuleInt("other", "", "price_for_soil_per_soil");
        int land = GlobalData.getSummary().getResources().getLand();
        return (2*land + value + 1)*value*x/2;
    }

    public int getGeneration(String type) {
        float sum = 0;
        // overall consumption for having this type of building / employee per turn (currently: just employee)
        for(Building b:GlobalData.getSummary().getBuildings()) {
            for(Map.Entry<String, Float> e:GlobalData.getRules().getRulesFloat("employees_earn", "Employee").entrySet()) {
                if(e.getKey().equals(type) && b.getEmployment()>0)
                    sum += e.getValue()*b.getEmployment()*b.getAdvancement();
            }
            for(Map.Entry<String, Float> e:GlobalData.getRules().getRulesFloat("employees_earn", b.getType().toString()).entrySet()) {
                if(e.getKey().equals(type) && b.getEmployment()>0)
                    sum += e.getValue()*b.getEmployment()*b.getAdvancement();
            }
        }
        sum += GlobalData.getRules().getRuleFloat("employees_constant", "", type);
        return (int)sum;
    }
}
