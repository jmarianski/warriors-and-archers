package net.treetank.astro.Fragments.SingleEntities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import net.treetank.astro.Fragments.SingleEntities.Pager;
import net.treetank.astro.Fragments.WarriorsFragment;
import net.treetank.astro.Logic.EmploymentLogic;
import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Economy.Building;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EmploymentListFragment extends Fragment {
    @Bind(R.id.unemployed_value)
    public TextView unemployedTextView;
    public List<Integer> namesInts;

    @Bind({ R.id.Farmers_change,
            R.id.MinersGold_change,
            R.id.Masons_change,
            R.id.Builders_change,
            R.id.Scientists_change,
            R.id.Armorers_change,
            R.id.Warriors_change,
            R.id.Archers_change })
    public List<TextView> changeTextViews;

    @Bind({ R.id.Farmers_value,
            R.id.MinersGold_value,
            R.id.Masons_value,
            R.id.Builders_value,
            R.id.Scientists_value,
            R.id.Armorers_value,
            R.id.Warriors_value,
            R.id.Archers_value })
    public List<TextView> valueTextViews;

    @Bind({ R.id.Farmers_advancement,
            R.id.MinersGold_advancement,
            R.id.Masons_advancement,
            R.id.Builders_advancement,
            R.id.Scientists_advancement,
            R.id.Armorers_advancement,
            R.id.Warriors_advancement,
            R.id.Archers_advancement })
    public List<TextView> advancementTextViews;

    @Bind({ R.id.Farmers_max,
            R.id.MinersGold_max,
            R.id.Masons_max,
            R.id.Builders_max,
            R.id.Scientists_max,
            R.id.Armorers_max,
            R.id.Warriors_max,
            R.id.Archers_max })
    public List<TextView> maxTextViews;


    private List<BuildingType> buildingTypes;

    private EmploymentLogic logic;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logic = new EmploymentLogic();
        buildingTypes = logic.getListOfEmployableBuildings();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_employment_list, container, false);
        ButterKnife.bind(this, view);
        populateViewWithSummary();
        populateViewWithTurn();

        setBackgroundOnClick(view);
        return view;
    }
    /**
     * Sets actions for clicking background
     * @param v
     */
    private void setBackgroundOnClick(View v) {
        v.findViewById(R.id.all_the_stuff).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {getParentFragment().getChildFragmentManager().popBackStack();}
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    private void populateViewWithSummary() {
        List<Building> buildings = GlobalData.getSummary().getBuildings();
        for (Building building : buildings) {
            int id = buildingTypes.indexOf(building.getType());
            if(id != -1 && valueTextViews.size()>id) {
                valueTextViews.get(id).setText(String.format("%d", building.getEmployment()));
                advancementTextViews.get(id).setText(String.format("%.1f%%", building.getAdvancement() * 100.0f));
                maxTextViews.get(id).setText(String.format("%d", logic.getMaxPossibleEmployment(building.getType())));
            }
        }
        unemployedTextView.setText(String.format("%d", logic.getCurrentUnemployed()));

    }

    private void populateViewWithTurn() {
        List<Building> buildings = GlobalData.getCurrentTurn().getBuildings();
        for (Building building : buildings) {
            int id = buildingTypes.indexOf(building.getType());
            if(id != -1 && changeTextViews.size()>id) {
                changeTextViews.get(id).setText(String.format("%d", building.getEmployment())); // Current change of employers
            }
        }
    }



}
