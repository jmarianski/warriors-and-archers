package net.treetank.astro.Fragments.SingleEntities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Diplomacy.Battle;
import net.treetank.astro.Utils.GlobalData;

import org.w3c.dom.Text;

import butterknife.ButterKnife;

/**
 * Created by Jacek on 30.03.2017.
 */

public class PlayerListFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        //int b = Integer.parseInt(args.getString("player_id"));
        View rootView = inflater.inflate(R.layout.fragment_kingdom_list, container, false);
        ((TextView)(rootView.findViewById(R.id.tv))).setText(GlobalData.diplomacy.solar_system);
        ((ListView)(rootView.findViewById(R.id.list))).setAdapter(GlobalData.diplomacy.allPlayers);
        ((ListView)(rootView.findViewById(R.id.list))).setOnItemClickListener(GlobalData.diplomacy.allPlayers);
        return rootView;
    }
}
