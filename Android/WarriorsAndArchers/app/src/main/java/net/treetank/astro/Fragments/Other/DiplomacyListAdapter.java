package net.treetank.astro.Fragments.Other;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Diplomacy.Kingdom;
import net.treetank.astro.Utils.GlobalData;

/**
 * Created by Jack on 04.03.2018.
 */
public class DiplomacyListAdapter extends ArrayAdapter<Object> implements AdapterView.OnItemClickListener {

    LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    public DiplomacyListAdapter(Context context) {
        super(context, R.layout.player_list_element);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = vi.inflate(R.layout.player_list_element, null);
        }
        Object o = getItem(position);
        if (o instanceof Kingdom) {
            Kingdom c = (Kingdom) o;
            ((TextView) (convertView.findViewById(R.id.name))).setText(c.name);
            ((TextView) (convertView.findViewById(R.id.params))).setText(String.format("%d", c.land));
        }
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Object o = getItem(position);
        if (o instanceof Kingdom)
            GlobalData.diplomacy.kingdomClicked((Kingdom) o);
    }
}
