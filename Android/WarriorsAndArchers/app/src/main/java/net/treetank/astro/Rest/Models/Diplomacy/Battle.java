package net.treetank.astro.Rest.Models.Diplomacy;


import android.util.Log;

import com.google.gson.annotations.SerializedName;

import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jacek on 05.03.2017.
 */

public class Battle {

    @SerializedName("id")
    public int id;
    @SerializedName("idfrom")
    public int idfrom;
    @SerializedName("idto")
    public int idto;
    @SerializedName("sent")
    public boolean sent;
    @SerializedName("soldiers")
    public List<Soldiers> soldiers;
    public boolean saved = true;
    public boolean saving = false;
    @SerializedName("hash")
    public int hash;
    private transient Battle backup;
    private static ArrayList<BuildingType> types;

    public static ArrayList<BuildingType> getTypes() {
        if(types!=null)
            return types;
        types = new ArrayList<>();
        types.add(BuildingType.BARRACKS);
        types.add(BuildingType.ARCHERHOUSE);
        return types;
    }

    public static Battle make() {
        Battle b = new Battle();
        b.id = -1;
        b.idfrom = GlobalData.getSummary().getKingdomId();
        b.saved = false;
        b.soldiers = new ArrayList<Soldiers>();
        for(BuildingType type:getTypes()) {
            Soldiers s = new Soldiers(type,0,0);
            b.soldiers.add(s);
        }
        return b;
    }

    private Soldiers getSoldiers(int type) {
        ArrayList types = getTypes();

        for(Soldiers s:soldiers) {
            if(types.indexOf(s.btype)==type) {
                return s;
            }
        }
        return null;
    }

    public int minus(int type, int amount) {
        Soldiers s = getSoldiers(type);
        if(s.number>amount) {
            s.number -= amount;
        }
        else {
            s.number = 0;
        }
        return s.number;
    }

    public int plus(int type, int amount) {
        Soldiers s = getSoldiers(type);
        int av = GlobalData.diplomacy.availableSoldiers(type);
        if(av>amount) {
            s.number += amount;
        }
        else {
            s.number += av;
        }
        return s.number;
    }

    public void copyValuesFromNet(Battle battle) {
        if(battle==null)
            return;
        id = battle.id;
        idfrom = battle.idfrom;
        idto = battle.idto;
        sent = battle.sent;
        soldiers = new ArrayList<Soldiers>();
        for(Soldiers s:battle.soldiers) {
            soldiers.add(new Soldiers(s.getType(), s.number, s.advancement));
        }
        saved = true;
        saving = battle.saving;
        backup = battle;
    }

    private boolean compareWithSavedPrivate() {
        if(backup==null)
            return false;
        boolean base = idto == backup.idto && idfrom == backup.idfrom && sent == backup.sent;
        if(!base)
            return false;
        for(Soldiers s:soldiers)
            for(Soldiers s2:backup.soldiers)
                if(s.building.equals(s2.building) && (s.number != s2.number || s.advancement != s2.advancement))
                    return false;
        return true;
    }

    public boolean isSendable() {
        if(idto==0)
            return false;
        for(Soldiers s:soldiers)
            if(s.number>0)
                return true;
        return false;

    }

    public boolean compareWithSaved() {
        boolean result = compareWithSavedPrivate();
        saved = result;
        return result;
    }

    public void restoreBackup() {
        copyValuesFromNet(backup);
        saved = true;
        saving = false;
    }
}
