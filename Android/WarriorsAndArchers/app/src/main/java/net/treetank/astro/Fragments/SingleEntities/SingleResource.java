package net.treetank.astro.Fragments.SingleEntities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.treetank.astro.Logic.ResourcesLogic;
import net.treetank.astro.Logic.TurnLogic;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Callbacks.LoginCallback;
import net.treetank.astro.Utils.BuildingType;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Jack on 14.04.2018.
 */

public class SingleResource extends SingleEntity {

    @Bind(R.id.image)
    ImageView image;
    @Bind(R.id.name)
    TextView name;
    @Bind(R.id.brief_description)
    TextView desc;
    @Bind(R.id.generating)
    TextView generating;
    @Bind(R.id.consuming)
    TextView consuming;
    private String resource_id;
    private ResourcesLogic logic;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.single_resource, container, false);
        ButterKnife.bind(this, rootView);
        Bundle args = getArguments();
        resource_id = args.getString("resource");
        Log.d("astro", "single_res"+resource_id);
        logic = new ResourcesLogic();
        fillStaticData(getContext());
        setBackgroundOnClick(rootView);
        return rootView;
    }

    private void fillStaticData(Context c) {
        image.setImageDrawable(logic.getDrawable(c, resource_id));
        name.setText(logic.getName(c, resource_id));
        desc.setText(logic.getDescription(c, resource_id));
        consuming.setText(String.format(Locale.getDefault(),"%d", logic.getConsumption(resource_id)));
        generating.setText(String.format(Locale.getDefault(),"%d", logic.getGeneration(resource_id)));
    }

    @Override
    public void updateUI() {
    }



    /**
     * Sets actions for clicking background
     * @param v
     */
    private void setBackgroundOnClick(View v) {
        v.findViewById(R.id.background).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {getParentFragment().getChildFragmentManager().popBackStack();}
        });
    }
}
