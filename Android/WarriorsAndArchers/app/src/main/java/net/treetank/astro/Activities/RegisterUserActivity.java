package net.treetank.astro.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import net.treetank.astro.Fragments.Other.ComicStripFragment;
import net.treetank.astro.Fragments.Other.RegisterFragment;
import net.treetank.astro.Fragments.SingleEntities.AddFragment;
import net.treetank.astro.Fragments.SingleEntities.Pager;
import net.treetank.astro.Fragments.SingleEntities.SingleBattleFragment;
import net.treetank.astro.Fragments.SingleEntities.SingleBuildingFragment;
import net.treetank.astro.Fragments.SingleEntities.SingleJobFragment;
import net.treetank.astro.Logic.BuildingsLogic;
import net.treetank.astro.Logic.EmploymentLogic;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Callbacks.RegisterCallback;
import net.treetank.astro.Utils.GlobalData;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterUserActivity extends WarriorsActivity {
    static final int REQUEST_CODE_PICK_ACCOUNT = 1000;
    public static final int RC_SIGN_IN = 1001;
    public GoogleSignInClient mGoogleSignInClient;

    public RegisterUserActivity.ScreenSlidePagerAdapter mPagerAdapter;
    public ViewPager mPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.server_client_id))
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mPager = new ViewPager(this);
        mPager.setBackgroundResource(R.drawable.tlo);
        mPager.setId(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        this.setContentView(mPager);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("setting auth", ""+requestCode);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                Log.d("setting auth", account.getIdToken());
                mPagerAdapter.setAuthToFragment(account.getIdToken());
            } catch (ApiException e) {
                // The ApiException status code indicates the detailed failure reason.
                // Please refer to the GoogleSignInStatusCodes class reference for more information.
                Log.w("Astro", "signInResult:failed code=" + e.getStatusCode());
                Toast.makeText(this, R.string.Login_required, Toast.LENGTH_SHORT).show();
            }


        } else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(this, R.string.Login_required, Toast.LENGTH_SHORT).show();
        }
    }

    public void signOut() {
            mGoogleSignInClient.signOut()
                    .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            finish();
                        }
                    });

    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<Fragment> fragments;
        RegisterFragment rf;
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
            fragments = new ArrayList<>();
        }

        @Override
        public Fragment getItem(int position) {
            Fragment f;
            if(ComicStripFragment.comic_strips.length>position) {
                f = new ComicStripFragment();
                Bundle b = new Bundle();
                b.putInt("number", position);
                f.setArguments(b);
            } else {
                f = new RegisterFragment();
                rf = (RegisterFragment)f;
            }
            return f;
        }

        @Override
        public int getCount() {
            return ComicStripFragment.comic_strips.length+1;
        }

        public void setAuthToFragment(String idToken) {
            Log.d("setting auth", idToken);
            rf.setAuth(idToken);
        }
    }
}
