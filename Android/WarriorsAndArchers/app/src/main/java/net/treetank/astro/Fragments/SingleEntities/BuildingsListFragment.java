package net.treetank.astro.Fragments.SingleEntities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.treetank.astro.Fragments.SingleEntities.Pager;
import net.treetank.astro.Fragments.WarriorsFragment;
import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;
import net.treetank.astro.Logic.BuildingsLogic;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Economy.Building;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BuildingsListFragment extends Fragment {
    @Bind(R.id.you_can_build_summary)
    public TextView canBuildTextView;

    public BuildingsLogic logic;

    @Bind({ R.id.Houses_change,
            R.id.Fields_change,
            R.id.GoldMines_change,
            R.id.MasonsWorkshops_change,
            R.id.BuildersGuilds_change,
            R.id.Universities_change,
            R.id.Armories_change,
            R.id.Barracks_change,
            R.id.ArcherHouses_change })
    public List<TextView> changeTextViews;


    @Bind({ R.id.Houses_value,
            R.id.Fields_value,
            R.id.GoldMines_value,
            R.id.MasonsWorkshops_value,
            R.id.BuildersGuilds_value,
            R.id.Universities_value,
            R.id.Armories_value,
            R.id.Barracks_value,
            R.id.ArcherHouses_value })
    public List<TextView> valueTextViews;

    private List<BuildingType> buildingTypes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logic = new BuildingsLogic();
        buildingTypes = logic.getListOfBuildableBuildings();
    }

    public void updateUI() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buildings_summary, container, false);
        ButterKnife.bind(this, view);

        populateViewWithSummary();
        populateViewWithTurn();
        setBackgroundOnClick(view);
        return view;
    }
    /**
     * Sets actions for clicking background
     * @param v
     */
    private void setBackgroundOnClick(View v) {
        v.findViewById(R.id.all_the_stuff).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {getParentFragment().getChildFragmentManager().popBackStack();}
        });
    }


    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void populateViewWithSummary() {
        List<Building> buildings = GlobalData.getSummary().getBuildings();
        for (Building building : buildings) {
            int id = buildingTypes.indexOf(building.getType());
            if(id!=-1 && valueTextViews.size()>id)
                valueTextViews.get(id).setText(String.format("%d", building.getNumber()));
        }
        canBuildTextView.setText(String.format("%d", logic.canBuildAmount(BuildingType.ERROR)));
    }

    private void populateViewWithTurn() {
        List<Building> buildings = GlobalData.getCurrentTurn().getBuildings();
        for (Building building : buildings) {
            int id = buildingTypes.indexOf(building.getType());
            if(id!=-1 && changeTextViews.size()>id)
                changeTextViews.get(id).setText(String.format("%d", building.getNumber()));
        }
    }

    @OnClick(R.id.you_can_build_tv)
    void makeToastForYouCanBuild() {
        String text = logic.buildableReason(BuildingType.ERROR);
        Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
    }

}