package net.treetank.astro.Fragments;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.treetank.astro.Fragments.Other.DiplomacyListAdapter;
import net.treetank.astro.Fragments.Other.RandomSinusInterpolator;
import net.treetank.astro.Fragments.SingleEntities.Pager;
import net.treetank.astro.Fragments.SingleEntities.PlayerListFragment;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Diplomacy.Kingdom;
import net.treetank.astro.Rest.Models.DiplomacyRequestManager;
import net.treetank.astro.Rest.Models.Economy.Building;
import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

/**
 * Created by Jacek on 19.03.2017.
 */

public class DiplomacyFragment extends WarriorsFragment {

    private DiplomacyRequestManager manager;
    private boolean load = false;


    private static ArrayList<Point> points = new ArrayList<>();
    private static ArrayList<Integer> strings = new ArrayList<>();
    private boolean textViewsAdded = false;
    private View view;

    static {
        points.add(new Point(-150, -52)); // war reports
        points.add(new Point(147, -74)); // battles
        strings.add(R.string.war_reports);
        strings.add(R.string.battles);
    }

    @Bind( R.id.progressBar)
    public ProgressBar loading;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_war_room, container, false);
        ButterKnife.bind(this, view);
        manager = GlobalData.diplomacy;
        if(load)
            showLoading();
        else
            hideLoading();

        if(manager.allPlayers ==null) {
            manager.allPlayers = new DiplomacyListAdapter(getContext());
        }
        ListView lv = (ListView) getActivity().findViewById(R.id.list);
        if(lv != null)
            lv.setAdapter(manager.allPlayers);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                addTextViews();
            }
        });
        return view;
    }




    private void addTextViews() {
        if(textViewsAdded)
            return;
        Point center = new Point((int)(0.5*view.getWidth()), (int)(0.5*view.getHeight()));
        ((RelativeLayout) view.findViewById(R.id.tv_container)).removeAllViewsInLayout();
        if(getContext()==null) {
            return;
        }
        for(int i=0; i<points.size(); i++) {
            Point p = points.get(i);
                Drawable d = view.findViewById(R.id.bg_image).getBackground();
                int x = (int) (p.x/(d.getIntrinsicWidth()/2f/view.getWidth()));
                int y = (int) (p.y/(d.getIntrinsicHeight()/2f/view.getHeight()));
                TextView tv = new TextView(getContext());
                tv.setBackgroundColor(Color.argb(100,0,0,0));
                tv.setText(strings.get(i));
                tv.setPadding(8,0,8,0);
                tv.measure(0,0);
                x -= tv.getMeasuredWidth()/2;
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.setMargins(center.x + x, center.y + y+90,0,0);
                tv.setLayoutParams(lp);
                ((RelativeLayout) view.findViewById(R.id.tv_container)).addView(tv);
        }
        textViewsAdded = true;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @OnTouch({ R.id.bg_image })
    public boolean onTouchBg(View touchedView, MotionEvent event) {
        MotionEvent.PointerCoords coords = new MotionEvent.PointerCoords();
        event.getPointerCoords(0, coords);
        float x = coords.x - 0.5f*touchedView.getWidth();
        float y = coords.y - 0.5f*touchedView.getHeight();
        Drawable d = touchedView.getBackground();
        x *= d.getIntrinsicWidth()/2f/touchedView.getWidth(); // 2 stands for 2 times scaling of an image
        y *= d.getIntrinsicHeight()/2f/touchedView.getHeight();
        Point result = null;
        double dist = Double.MAX_VALUE;
        for(Point p:points) {
            double new_dist = Math.pow(p.x - x,2)+ Math.pow(p.y - y,2);
            if(result == null || new_dist<dist) {
                result = p;
                dist = new_dist;
            }
        }
        int position = points.indexOf(result);
        if(dist<2500) // 50x50px
            onClickName(position);
        return false;
    }


    public void onClickName(int position) {
        if(position==0)
            showReports();
        else
            onClickBattles();
    }


    public void onClickBattles() {
        if(manager.canShowBattles())
            showBattles();
        else {
            showLoading();
            manager.loadPlayers();
        }
    }

    public void clickPlayers() {
        PlayerListFragment plf = new PlayerListFragment();
        Bundle b = new Bundle();
        plf.setArguments(b);
        getChildFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.alpha_in, R.anim.alpha_out, R.anim.alpha_in, R.anim.alpha_out)
                .add(R.id.parent, plf)
                .addToBackStack(null)
                .commit();
    }

    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
    }

    public void hideLoading() {
        loading.setVisibility(View.GONE);
    }

    public void showBattles() {
        Pager mbf = new Pager();
        Bundle b = new Bundle();
        b.putInt("type", 2);
        b.putInt("position", 0);
        mbf.setArguments(b);
        getChildFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.alpha_in, R.anim.alpha_out, R.anim.alpha_in, R.anim.alpha_out)
                .add(R.id.parent, mbf)
                .addToBackStack(null)
                .commit();
    }


    public void showReports() {
        if(GlobalData.getSummary().reports.size()==0) {
            Toast.makeText(getContext(), getString(R.string.no_reports), Toast.LENGTH_LONG).show();
            return;
        }
        Pager mbf = new Pager();
        Bundle b = new Bundle();
        b.putInt("type", 3);
        b.putInt("position", 0);
        mbf.setArguments(b);
        getChildFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.alpha_in, R.anim.alpha_out, R.anim.alpha_in, R.anim.alpha_out)
                .add(R.id.parent, mbf)
                .addToBackStack(null)
                .commit();
    }

    public void playerClicked(Kingdom o) {
        if(o!=null)
            GlobalData.diplomacy.battle.idto = o.id;
        TextView tv = (TextView) getActivity().findViewById(R.id.target);
        tv.setText(String.format("Attack on %s", o.name));
    }

    @Override
    public void updateUI() {

    }


}
