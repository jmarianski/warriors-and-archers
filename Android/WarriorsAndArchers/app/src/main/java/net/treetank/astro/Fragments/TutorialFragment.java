package net.treetank.astro.Fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.treetank.astro.Fragments.SingleEntities.EmploymentListFragment;
import net.treetank.astro.R;

import java.util.HashSet;
import java.util.Set;


public class TutorialFragment extends Fragment {

    public TutorialFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    View.OnClickListener OCL = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TutorialFragment.this.getView().setVisibility(View.GONE);
            SharedPreferences sp = getSharedPreferences();
            Set<String> set = new HashSet<String>();
            Set<String> set2 = sp.getStringSet("tutorials", new HashSet<String>());
            for (String s:set2) {set.add(s);}
            Fragment f = getParentFragment();
            set.add(f.getClass().getSimpleName());
            sp.edit().putStringSet("tutorials", set).apply();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_tutorial, container, false);
        Fragment f = getParentFragment();
        if(f instanceof SummaryFragment) {
            ((TextView)(v.findViewById(R.id.text))).setText(R.string.tutorial_1);
        } else if(f instanceof BuildingsFragment) {
            ((TextView)(v.findViewById(R.id.text))).setText(R.string.tutorial_2);
        } else if(f instanceof EmploymentListFragment) {
            ((TextView)(v.findViewById(R.id.text))).setText(R.string.tutorial_3);
        } else if(f instanceof BuyLandFragment) {
            ((TextView)(v.findViewById(R.id.text))).setText(R.string.tutorial_4);
        }else if(f instanceof DiplomacyFragment) {
            ((TextView)(v.findViewById(R.id.text))).setText(R.string.tutorial_5);
        }
        SharedPreferences sp = getSharedPreferences();
        Set<String> set = sp.getStringSet("tutorials", new HashSet<String>());
        if(set.contains(f.getClass().getSimpleName()))
            v.setVisibility(View.GONE);
        v.findViewById(R.id.x).setOnClickListener(OCL);
            return v;
    }

    public SharedPreferences getSharedPreferences() {
        String sharedPreferencesFile = getResources().getString(R.string.Shared_preference_file);
    return getContext().getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE);
}

    public void updateUI() {

    }

}
