package net.treetank.astro.Logic;

import android.util.Log;

import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Economy.Building;
import net.treetank.astro.Rest.Models.Economy.Summary;
import net.treetank.astro.Rest.Models.Economy.Turn;
import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmploymentLogic {
    private ArrayList<BuildingType> employableBuildings;

    public EmploymentLogic() {
        fillArraysWithData();
    }

    private void fillArraysWithData() {
        employableBuildings = new ArrayList<>();
        employableBuildings.add(BuildingType.FIELD);
        employableBuildings.add(BuildingType.GOLDMINE);
        employableBuildings.add(BuildingType.MASONS_WORKSHOP);
        employableBuildings.add(BuildingType.BUILDERS_GUILD);
        employableBuildings.add(BuildingType.UNIVERSITY);
        employableBuildings.add(BuildingType.ARMORY);
        employableBuildings.add(BuildingType.BARRACKS);
        employableBuildings.add(BuildingType.ARCHERHOUSE);
    }

    public HashMap<String, Float> costOfEmployment(BuildingType type) {
        HashMap<String, Float> cost = new HashMap<>();
        for(Map.Entry<String, Float> e:GlobalData.getRules().getRulesFloat("cost_add_employee", "Building").entrySet()) {
            float val = cost.get(e.getKey())==null?0:cost.get(e.getKey());
            cost.put(e.getKey(), val + e.getValue());
        }
        for(Map.Entry<String, Float> e:GlobalData.getRules().getRulesFloat("cost_add_employee", type.toString()).entrySet()) {
            float val = cost.get(e.getKey())==null?0:cost.get(e.getKey());
            cost.put(e.getKey(), val + e.getValue());
        }
        return cost;
    }

    public int canEmploy(BuildingType type) {
        Building building = GlobalData.getSummary().getBuilding(type);
        Building changedBuilding = GlobalData.getCurrentTurn().getBuilding(type);
        int min = getCurrentUnemployed();
        int all_slots = (building.getNumber() + changedBuilding.getNumber()) * GlobalData.getRules().getRuleInt("other", "", "employees_per_building");
        int slots = all_slots - (building.getEmployment() + changedBuilding.getEmployment());
        if(slots<min)
            min = slots;
        HashMap<String, Float> cost = costOfEmployment(building.getType());
        // now that we have all costs for buildings
        for(Map.Entry<String, Float> e:cost.entrySet()) {
            int val = (int) ((GlobalData.getSummary().getResources().get(e.getKey()) + GlobalData.getCurrentTurn().getResources().get(e.getKey())) / e.getValue());
            if(val<min)
                min = val;
        }
        return min;
    }

    public String employmentReason(BuildingType type) {
        Building building = GlobalData.getSummary().getBuilding(type);
        Building changedBuilding = GlobalData.getCurrentTurn().getBuilding(type);
        HashMap<String, Float> cost = costOfEmployment(type);
        int unemployed = getCurrentUnemployed();
        String result = getString(R.string.Employment_1)+" "+unemployed;
        if(type!=BuildingType.ERROR) {
            int all_slots = (building.getNumber() + changedBuilding.getNumber()) * GlobalData.getRules().getRuleInt("other", "", "employees_per_building");
            int slots = all_slots - (building.getEmployment() + changedBuilding.getEmployment());
            result += "\n"+getString(R.string.Employment_5)+" "+slots;
        }
        for(Map.Entry<String, Float> e:cost.entrySet()) {
            int resources =  GlobalData.getSummary().getResources().get(e.getKey()) + GlobalData.getCurrentTurn().getResources().get(e.getKey());
            float value =  e.getValue();
            result += "\n"+""+ e.getKey()+ ": " +value +" "+getString(R.string.per)+"; "+resources+" "+getString(R.string.available);
        }
        return result;
    }

    private String getString(int id) {
        return GlobalData.getViewManager().activity.getString(id);
    }


    private void consumeResources(BuildingType type, int amount) {
        if(amount==0)
            return;
        Building changedBuilding = GlobalData.getCurrentTurn().getBuilding(type);
        int change = 0;
        if(changedBuilding.getEmployment()<=0 && amount<0) {

        } else if(changedBuilding.getEmployment()>0 && amount<0) {
            if(changedBuilding.getEmployment() + amount>=0)
                change = amount;
            else
                change = -changedBuilding.getEmployment();
        } else if(changedBuilding.getEmployment()>0 && amount>0) {
            change = amount;
        } else if(changedBuilding.getEmployment()+amount>0)  // amount >0 i changedBuilding.getNumber()<0
            change = changedBuilding.getEmployment()+amount;
        if(change==0)
            return;
        HashMap<String, Float> resourcesToConsume = costOfEmployment(changedBuilding.getType());
        for(Map.Entry<String, Float> e:resourcesToConsume.entrySet()) {
            GlobalData.getCurrentTurn().getResources().add(e.getKey(), -(int)Math.floor(e.getValue()*change));
        }
    }

    public int changeAmountOfEmployees(BuildingType type, int change) {
        int canEmploy = canEmploy(type);
        Building building = GlobalData.getSummary().getBuilding(type);
        Building changedBuilding = GlobalData.getCurrentTurn().getBuilding(type);
        if(canEmploy<change) {
            change = canEmploy;
        }
        if(building.getEmployment() + changedBuilding.getEmployment() + change<0)
            change = - (building.getEmployment() + changedBuilding.getEmployment());
        consumeResources(building.getType(), change);


        int finalChange = changedBuilding.getEmployment() + change;
        changedBuilding.setEmployment(finalChange);
        return finalChange;
    }

    public int getMaxPossibleEmployment(BuildingType type) {
        return (GlobalData.getSummary().getBuilding(type).getNumber() + GlobalData.getCurrentTurn().getBuilding(type).getNumber()) * GlobalData.getRules().getRuleInt("other", "", "employees_per_building");
    }

    public int getCurrentUnemployed() {
        int unemployed = GlobalData.getSummary().getResources().getPeople();
        ArrayList<List<Building>> buildingsList = new ArrayList<>();
        buildingsList.add(GlobalData.getSummary().getBuildings());
        buildingsList.add(GlobalData.getCurrentTurn().getBuildings());

        // according to summary & changes
        for (List<Building> buildings : buildingsList) {
            for (Building building : buildings) {
                int id = employableBuildings.indexOf(building.getType());
                if (id != -1) {
                    unemployed -= building.getEmployment();
                }
            }
        }
        return unemployed;
    }


    public ArrayList<BuildingType> getListOfEmployableBuildings() {

        return employableBuildings;
    }
}
