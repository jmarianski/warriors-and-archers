package net.treetank.astro.Fragments.SingleEntities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.support.v7.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.treetank.astro.Logic.EmploymentLogic;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Economy.Building;
import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SingleJobFragment extends SingleEntity {

    Building building, buildingChanges;
    EmploymentLogic logic = new EmploymentLogic();

    @Bind(R.id.name)
    TextView name;

    @Bind(R.id.value)
    TextView value;

    @Bind(R.id.image)
    ImageView image;

    @Bind(R.id.unemployed_value2)
    public TextView unemployedTextView;

    @Bind(R.id.canEmploy)
    public TextView canEmploy;


    @Bind(R.id.change)
    TextView change;

    @Bind(R.id.max)
    TextView maximum;

    @Bind(R.id.advancement)
    TextView advancement;

    @Bind(R.id.brief_description)
    TextView desc;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.single_job, container, false);
        ButterKnife.bind(this, rootView);
        Bundle args = getArguments();
        BuildingType b = BuildingType.fromString(args.getString("building"));
        if(b!=BuildingType.ERROR) {
            building = GlobalData.getSummary().getBuilding(b);
            buildingChanges = GlobalData.getCurrentTurn().getBuilding(b);
            name.setText(building.getJobName(this.getContext()));
            value.setText(String.format("%d", building.getEmployment()));
            image.setImageDrawable(getContext().getResources().getDrawable(building.getJobDrawableId()));
            change.setText(String.format("%d", buildingChanges.getEmployment()));
            maximum.setText(String.format("%d", building.getNumber()+buildingChanges.getNumber()));
            advancement.setText(String.format("%.1f%%", building.getAdvancement() * 100.0f));
            desc.setText(building.getJobDescription());
        }
        updateUnemployed();
        setBackgroundOnClick(rootView);
        return rootView;
    }

    public void updateUI() {
        updateUnemployed();
    }

    /**
     * Sets actions for clicking background
     * @param v
     */
    private void setBackgroundOnClick(View v) {
        v.findViewById(R.id.background).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {getParentFragment().getChildFragmentManager().popBackStack();}
        });
    }


    @OnClick(R.id.canemploy_text)
    public void canemploy_onclick() {
        String text = logic.employmentReason(building.getType());
        Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.plus)
    public void plus() {
        int changeValue = Integer.parseInt(change_value2.getText().toString());
        int current = logic.changeAmountOfEmployees(building.getType(), changeValue);
        updateChangeTextView(current);
    }
    @OnClick(R.id.minus)
    public void minus() {
        int changeValue = Integer.parseInt(change_value2.getText().toString());
        int current = logic.changeAmountOfEmployees(building.getType(), -changeValue);
        updateChangeTextView(current);
    }

    private void updateChangeTextView(int changeValue) {
        change.setText(String.format("%d", changeValue));
        TextView currentChangeTextView = getTextViewToUpdate();
        LinearLayout tl = getTable();
        if(currentChangeTextView!=null)
            currentChangeTextView.setText(String.format("%d", changeValue));
        updateUnemployed();
        GlobalData.getViewManager().fragment.updateUI();
        if(tl==null)
            return;
        tl.requestLayout();
        tl.invalidate();
    }

    private void updateUnemployed() {
        int can = logic.canEmploy(building.getType());
        int unemployed = logic.getCurrentUnemployed();
        if(unemployedTextView!=null)
            unemployedTextView.setText(String.format("%d", unemployed));
        if(canEmploy!=null)
            canEmploy.setText(String.format("%d", can));
    }

    TextView getTextViewToUpdate() {
        switch(building.getType()) {
            case FIELD: return (TextView) getActivity().findViewById(R.id.Farmers_change);
            case GOLDMINE: return (TextView) getActivity().findViewById(R.id.MinersGold_change);
            case BUILDERS_GUILD: return (TextView) getActivity().findViewById(R.id.Builders_change);
            case MASONS_WORKSHOP: return (TextView) getActivity().findViewById(R.id.Masons_change);
            case UNIVERSITY: return (TextView) getActivity().findViewById(R.id.Scientists_change);
            case ARMORY: return (TextView) getActivity().findViewById(R.id.Armorers_change);
            case BARRACKS: return (TextView) getActivity().findViewById(R.id.Warriors_change);
            case ARCHERHOUSE: return (TextView) getActivity().findViewById(R.id.Archers_change);
            default: return null;

        }
    }

    LinearLayout getTable() {
        return (LinearLayout) getActivity().findViewById(R.id.all_the_stuff);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
