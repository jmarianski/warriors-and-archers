package net.treetank.astro.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import net.treetank.astro.Utils.GlobalData;

/**
 * Created by Jacek on 29.01.2017.
 */

public class WarriorsActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalData.getViewManager().activity = this;
    }
}
