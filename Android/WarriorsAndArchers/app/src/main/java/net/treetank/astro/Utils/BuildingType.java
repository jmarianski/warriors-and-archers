package net.treetank.astro.Utils;

public enum BuildingType {
    HOUSE("House"),
    FIELD("Field"),
    GOLDMINE("Goldmine"),
    MASONS_WORKSHOP("MasonsWorkshop"),
    BUILDERS_GUILD("BuildersGuild"),
    UNIVERSITY("University"),
    ARMORY("Armory"),
    BARRACKS("Barracks"),
    ARCHERHOUSE("ArcherHouse"),
    ERROR("Error");

    private String stringValue;
    BuildingType(String toString) {
        stringValue = toString;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    public static BuildingType fromString(String type) {
        switch (type) {
            case "House":
                return HOUSE;
            case "Field":
                return FIELD;
            case "Goldmine":
                return GOLDMINE;
            case "MasonsWorkshop":
                return MASONS_WORKSHOP;
            case "BuildersGuild":
                return BUILDERS_GUILD;
            case "Armory":
                return ARMORY;
            case "Barracks":
                return BARRACKS;
            case "ArcherHouse":
                return ARCHERHOUSE;
            case "University":
                return UNIVERSITY;
            default:
                return ERROR;
        }
    }
}
