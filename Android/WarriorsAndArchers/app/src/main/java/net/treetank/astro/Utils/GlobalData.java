package net.treetank.astro.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.ChatManager;
import net.treetank.astro.Rest.Models.DiplomacyRequestManager;
import net.treetank.astro.Rest.Models.Economy.Summary;
import net.treetank.astro.Rest.Models.Economy.Turn;
import net.treetank.astro.Rest.Models.Login.LoginResult;
import net.treetank.astro.Rest.Models.Rules;
import net.treetank.astro.Rest.ViewManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Map;

public class GlobalData {
    public static LoginResult login;
    private static Turn currentTurn;
    private static Summary summary;
    private static Boolean blockButtons;
    private static Rules rules;
    private static ViewManager vm = new ViewManager();
    public static boolean turn = false;
    public static ChatManager chat = new ChatManager();
    public static DiplomacyRequestManager diplomacy = new DiplomacyRequestManager();
    public static Player player = new Player();
    public static HelpData help = new HelpData();

    public static ViewManager getViewManager() {return vm; }

    public static Summary getSummary() {
        return summary;
    }

    public static Rules getRules() {
        if(rules==null)
            rules = new Rules();
        return rules;
    }

    public static void clearData() {
        diplomacy.clearData();
        chat.clearData();
    }

    public static void setSummary(Summary summary) {
        GlobalData.summary = summary;
    }

    public static Turn getCurrentTurn() {
        return currentTurn;
    }

    public static void setCurrentTurn(Turn currentTurn) {
        GlobalData.currentTurn = currentTurn;
    }


    public static Boolean isBlockButtons() {
        return blockButtons;
    }

    public static void setBlockButtons(Boolean blockButtons) {
        GlobalData.blockButtons = blockButtons;
    }

    private static String file_name(String prefix, String url) {
        Uri uri = new Uri.Builder().scheme("http").encodedPath(url).build();
        String fname = prefix+"_"+uri.getLastPathSegment().replace(" ", "_");
        return fname;
    }

    public static void startMusicDownload() {
        GlobalData.player.musicDownloadFinished();
        // no longer downloading stuff
        /*
        ArrayList<Pair<String, String>> to_download = new ArrayList<>();
        for(Map.Entry<String, String[]> entry:summary.download.entrySet()) {
            String prefix = entry.getKey();
            for(String url:entry.getValue()) {
                String fname = file_name(prefix, url);
                File f = vm.activity.getBaseContext().getFileStreamPath(fname);
                if(!f.exists()) {
                    to_download.add(new Pair<>(prefix, url));
                }
                if(prefix.equals("Music") && f.exists()) {
                    GlobalData.player.addPlayableMusic(fname);
                }
            }
        }
        if(to_download.size()>0)
            new AsyncDownload().execute(to_download, new Handler());
        */
    }

    public static void updateTheme(Context c) {

        switch(c.getSharedPreferences(c.getString(R.string.Shared_preference_file), Context.MODE_PRIVATE).getInt("settings_text_size", 11)) {
            case 9: c.setTheme(R.style.BaseTheme_9); break;
            case 10: c.setTheme(R.style.BaseTheme_10); break;
            case 11: c.setTheme(R.style.BaseTheme_11); break;
            case 12: c.setTheme(R.style.BaseTheme_12); break;
            case 13: c.setTheme(R.style.BaseTheme_13); break;
            default: throw new IllegalArgumentException("Wrong text size");
        }
    }

    public static SharedPreferences getSharedPreferences() {
        return getViewManager().activity.getSharedPreferences(getViewManager().activity.getString(R.string.Shared_preference_file), Context.MODE_PRIVATE);
    }

    static class AsyncDownload extends AsyncTask {

        @Override
        protected void onPostExecute(Object integer) {
            super.onPostExecute(integer);
            Context c = GlobalData.getViewManager().activity.getApplicationContext();
            Toast.makeText(c, c.getString(R.string.other_download_finished), Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Integer doInBackground(Object[] arrayLists) {

            if(!(arrayLists[0] instanceof ArrayList) || !(arrayLists[1] instanceof Handler))
                return -1;
            Handler handler = (Handler) arrayLists[1];
                ArrayList<Pair<String, String>> urls = (ArrayList<Pair<String, String>>)arrayLists[0];
                for(Pair<String, String> pair:urls) {
                    String prepend = pair.first;
                    String url = pair.second;
                    try {
                        String fname = file_name(prepend, url);
                        int count;
                        URL u = new URL(url);
                        URLConnection conection = u.openConnection();
                        conection.connect();
                        InputStream input = new BufferedInputStream(u.openStream(),
                                8192);
                        final String fileName = fname;
                        FileOutputStream output = vm.activity.openFileOutput(fileName, Context.MODE_PRIVATE);
                        int lenghtOfFile = conection.getContentLength();

                        byte data[] = new byte[1024];

                        long total = 0;

                        while ((count = input.read(data)) != -1) {
                            total += count;
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                            // writing data to file
                            output.write(data, 0, count);
                            Log.d("Download", ((int) ((total * 100) / lenghtOfFile)+"%"));
                        }

                        // flushing output
                        output.flush();

                        // closing streams
                        output.close();
                        input.close();
                        if(prepend.equals("Music"))
                            handler.post(new Runnable() {public void run() {GlobalData.player.addPlayableMusic(fileName);}});
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            return null;
        }
    }

}
