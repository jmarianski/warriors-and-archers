package net.treetank.astro.Fragments.SingleEntities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import net.treetank.astro.Logic.DiplomacyLogic;
import net.treetank.astro.R;
import net.treetank.astro.Utils.GlobalData;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddFragment extends SingleEntity {

    public int type;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.single_new, container, false);
        ButterKnife.bind(this, rootView);
        setBackgroundOnClick(rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.add)
    public void onClick() {

        Toast.makeText(getContext(), "test", Toast.LENGTH_LONG).show();
        GlobalData.diplomacy.current.addItem(type);
        // do something
    }

    private void setBackgroundOnClick(View v) {
        v.findViewById(R.id.background).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {getParentFragment().getChildFragmentManager().popBackStack();}
        });
    }

    public void updateUI() {
        // do nothing
    }
}
