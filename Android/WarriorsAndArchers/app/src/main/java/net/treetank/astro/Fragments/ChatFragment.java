package net.treetank.astro.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.treetank.astro.R;
import net.treetank.astro.Rest.Callbacks.ChatCallback;
import net.treetank.astro.Rest.Models.ChatManager;
import net.treetank.astro.Rest.Models.Social.Channel;
import net.treetank.astro.Rest.Models.Social.Message;
import net.treetank.astro.Utils.GlobalData;

import java.util.ArrayList;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ChatFragment extends WarriorsFragment {

    ChatManager chat;
    @Bind(R.id.progressBar)
    ProgressBar bar;
    @Bind(R.id.list)
    ListView lv;
    @Bind(R.id.chat)
    EditText chatbox;
    int position;

    public ChatFragment() {
        chat = GlobalData.chat;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_list, container, false);
        ButterKnife.bind(this, view);
        Context context = view.getContext();
        ListView listview = (ListView) view.findViewById(R.id.list);
        setOnEditorActionListener();
        if(chat.chatAdapter !=null) {
            listview.setAdapter(chat.chatAdapter);
        }
        else {
            ChatAdapter ca = chat.chatAdapter = new ChatAdapter(context);
            listview.setAdapter(ca);
            chat.callback.getChannels();
        }
        listview.setOnItemClickListener(chat.chatAdapter);
        updateUI();
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        position = lv.getFirstVisiblePosition();
    }

    @Override
    public void onResume() {
        super.onResume();
        lv.setSelection(position);
    }

    private void setOnEditorActionListener() {
        chatbox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if(actionId==EditorInfo.IME_ACTION_SEND) {
                    String text = textView.getText().toString();
                    chat.callback.sendMessage(text);
                    textView.setText("");
                }
                return false;
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        if(GlobalData.chat.channel!=null) {
            GlobalData.chat.clickedBackInsideChannel();
            return true;
        }
        return false;
    }

    public class ChatAdapter extends ArrayAdapter<Object> implements AdapterView.OnItemClickListener {

        LayoutInflater vi = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        public ChatAdapter(Context context) {
            super(context, R.layout.chat_message);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView==null) {
                convertView = vi.inflate(R.layout.chat_message, null);
            }
            Object o = getItem(position);
            if(o instanceof Channel) {
                Channel c = (Channel) o;
                ((TextView) (convertView.findViewById(R.id.name))).setText(c.name);
                ((TextView) (convertView.findViewById(R.id.date))).setText(c.last_update);
                ((TextView) (convertView.findViewById(R.id.text))).setText(c.desc);
            }
            if(o instanceof Message) {
                Message m = (Message) o;
                ((TextView) (convertView.findViewById(R.id.name))).setText(m.name);
                ((TextView) (convertView.findViewById(R.id.date))).setText(m.time);
                ((TextView) (convertView.findViewById(R.id.text))).setText(m.text);
            }
            return convertView;
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Object o = getItem(position);
            if(o instanceof Channel)
                GlobalData.chat.channelClicked((Channel)o);
        }
    }

    public void updateUI() {
        if(chat.loading)
            bar.setVisibility(View.VISIBLE);
        else
            bar.setVisibility(View.GONE);
        if(chat.channel==null)
            chatbox.setVisibility(View.GONE);
        else
            chatbox.setVisibility(View.VISIBLE);

    }

    public void scrollBottom() {
        int y = lv.getScrollY();
        Log.d("test", lv.getLastVisiblePosition()+""+lv.getCount());
        if(lv.getLastVisiblePosition()>lv.getCount()-3)
            forceScrollBottom();
    }

    public void forceScrollBottom() {
        lv.setSelection(lv.getCount()-1);
    }

}
