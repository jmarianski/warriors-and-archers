package net.treetank.astro.Rest.Models.Social;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jacek on 04.02.2017.
 */

public class ChatResponse {

    @SerializedName("RequestType")
    public int requestType;
    @SerializedName("Message")
    public String message;
    @SerializedName("Channels")
    public List<Channel> channels;
    @SerializedName("Channel")
    public Channel channel;
    @SerializedName("Messages")
    public List<Message> messages;
}
