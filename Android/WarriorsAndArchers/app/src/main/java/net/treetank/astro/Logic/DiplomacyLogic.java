package net.treetank.astro.Logic;

import net.treetank.astro.Fragments.SingleEntities.Pager;
import net.treetank.astro.Rest.Models.Diplomacy.Battle;
import net.treetank.astro.Rest.Models.Diplomacy.Soldiers;
import net.treetank.astro.Rest.Models.Economy.Building;
import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jacek on 05.03.2017.
 */

public class DiplomacyLogic {

    ArrayList<BuildingType> soldierTypes;

    public DiplomacyLogic() {
        soldierTypes = new ArrayList<>();
        soldierTypes.add(BuildingType.BARRACKS);
        soldierTypes.add(BuildingType.ARCHERHOUSE);
    }


    public List<Battle> getListOfBattles() {
        return GlobalData.diplomacy.battles;
    }


    public int getAllSoldiers(int type) {
        int all;
        Building b = GlobalData.getSummary().getBuilding(soldierTypes.get(type));
        all = b.getEmployment();
        return all;
    }
}
