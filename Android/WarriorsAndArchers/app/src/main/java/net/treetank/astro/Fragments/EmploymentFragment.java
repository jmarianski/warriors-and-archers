package net.treetank.astro.Fragments;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.treetank.astro.Fragments.Other.RandomSinusInterpolator;
import net.treetank.astro.Fragments.SingleEntities.Pager;
import net.treetank.astro.Logic.BuildingsLogic;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Economy.Building;
import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnTouch;

/**
 * Created by Jack on 02.03.2018.
 */

public class EmploymentFragment extends WarriorsFragment {


    private static ArrayList<Point> points = new ArrayList<>();
    private static ArrayList<BuildingType> buildings = new ArrayList<>();
    private boolean textViewsAdded = false;

    static {
        points.add(new Point(165, 85)); // castle
        points.add(new Point(-197, -20)); // farm
        points.add(new Point(-140, -20)); // goldmine
        points.add(new Point(-85, -20)); // masonry
        points.add(new Point(-29, -20)); // builders guild
        points.add(new Point(23, -20)); // science

        points.add(new Point(77, -20)); // forges
        points.add(new Point(130, -20)); // swords
        points.add(new Point(180, -20)); // arrows
        buildings.add(BuildingType.ERROR);
        buildings.add(BuildingType.FIELD);
        buildings.add(BuildingType.GOLDMINE);
        buildings.add(BuildingType.MASONS_WORKSHOP);
        buildings.add(BuildingType.BUILDERS_GUILD);
        buildings.add(BuildingType.UNIVERSITY);
        buildings.add(BuildingType.ARMORY);
        buildings.add(BuildingType.BARRACKS);
        buildings.add(BuildingType.ARCHERHOUSE);
    }



    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_employment, container, false);
        ButterKnife.bind(this, view);

        textViewsAdded = false;
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                addTextViews();
            }
        });
        return view;
    }


    private void addTextViews() {
        if(textViewsAdded)
            return;
        BuildingsLogic bl = new BuildingsLogic();
        Point center = new Point((int)(0.5*view.getWidth()), (int)(0.5*view.getHeight()));
        ((RelativeLayout) view.findViewById(R.id.tv_container)).removeAllViewsInLayout();
        if(getContext()==null) {
            return;
        }
        for(int i=0; i<points.size(); i++) {
            Point p = points.get(i);
            BuildingType bt = buildings.get(i);
            Building b = GlobalData.getSummary().getBuilding(bt);
            Building b2 = GlobalData.getCurrentTurn().getBuilding(bt);
            if(b!=null) {
                Drawable d = view.findViewById(R.id.bg_image).getBackground();
                int x = (int) (p.x/(d.getIntrinsicWidth()/2f/view.getWidth()));
                int y = (int) (p.y/(d.getIntrinsicHeight()/2f/view.getHeight()));
                TextView tv = new TextView(getContext());
                tv.setBackgroundColor(Color.argb(100,0,0,0));
                tv.setText(String.format("%s/%s", b.getEmployment()+b2.getEmployment(), b.getNumber()+b2.getNumber()));
                tv.setPadding(8,0,8,0);
                tv.measure(0,0);
                x -= tv.getMeasuredWidth()/2;
                y -= tv.getMeasuredHeight()/2;
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.setMargins(center.x + x, center.y + y-140 - tv.getMeasuredHeight()*(i%2),0,0);
                tv.setLayoutParams(lp);
                ((RelativeLayout) view.findViewById(R.id.tv_container)).addView(tv);
            }
        }
        textViewsAdded = true;
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @OnTouch({ R.id.bg_image })
    public boolean onTouchBg(View touchedView, MotionEvent event) {
        MotionEvent.PointerCoords coords = new MotionEvent.PointerCoords();
        event.getPointerCoords(0, coords);
        float x = coords.x - 0.5f*touchedView.getWidth();
        float y = coords.y - 0.5f*touchedView.getHeight();
        Drawable d = touchedView.getBackground();
            x *= d.getIntrinsicWidth()/2f/touchedView.getWidth(); // 2 stands for 2 times scaling of an image
            y *= d.getIntrinsicHeight()/2f/touchedView.getHeight();
        Point result = null;
        double dist = Double.MAX_VALUE;
        for(Point p:points) {
            double new_dist = Math.pow(p.x - x,2)+ Math.pow(p.y - y,2);
            if(result == null || new_dist<dist) {
                result = p;
                dist = new_dist;
            }
        }
        int position = points.indexOf(result);
        if(dist<10000) // 100x100px
            onClickName(position);
        return false;
    }


    public void onClickName(int position) {
        Pager mbf = new Pager();
        Bundle b = new Bundle();
        b.putInt("type", 1);
        b.putInt("position", position);
        mbf.setArguments(b);
        getChildFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.alpha_in, R.anim.alpha_out, R.anim.alpha_in, R.anim.alpha_out)
                .add(R.id.parent, mbf)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void updateUI() {
        textViewsAdded = false;
        view.invalidate();
    }
}
