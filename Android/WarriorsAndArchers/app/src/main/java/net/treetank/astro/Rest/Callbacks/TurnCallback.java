package net.treetank.astro.Rest.Callbacks;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import net.treetank.astro.Fragments.SummaryFragment;
import net.treetank.astro.Logic.BuildingsLogic;
import net.treetank.astro.Logic.BuyLandLogic;
import net.treetank.astro.Logic.TurnLogic;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Economy.Summary;
import net.treetank.astro.Rest.Models.Economy.Turn;
import net.treetank.astro.Rest.RestClient;
import net.treetank.astro.Utils.GlobalData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jacek on 31.01.2017.
 */

public class TurnCallback implements Callback<Summary> {

    TurnLogic logic;

    public TurnCallback(TurnLogic logic) {
        this.logic = logic;
    }




    public void call(){
        RestClient.get().sendTurn(GlobalData.getCurrentTurn()).enqueue(this);
    }


    private void requestFinished() {
        GlobalData.turn = false;
        FragmentTransaction fragmentTransaction = GlobalData.getViewManager().hf.getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.summary_fragment_container, new SummaryFragment());
        fragmentTransaction.commit();
        GlobalData.getViewManager().hf.animationHourglass.setOneShot(true);
        GlobalData.setBlockButtons(false);
        GlobalData.getViewManager().hf.updateUI();
    }

    public void sendTurnSuccess() {
        GlobalData.player.success();
        Toast.makeText(getContext(), R.string.Turn_saved, Toast.LENGTH_SHORT).show();
        requestFinished();
    }

    public void sentTurnFailure(Throwable error) {
        String text = getContext().getResources().getString(R.string.Something_went_wrong) + " " + error.getMessage();
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
        Log.e("Turn_callback", text);
        requestFinished();
    }

    public void boundsFailure() {

        BuildingsLogic bLogic;
        TurnLogic tl = new TurnLogic();
        bLogic = new BuildingsLogic();
        String error = "";
        if(bLogic.getAvailableBricks()<0)
            error +=getContext().getString(R.string.error_1)+"\n";
        if(bLogic.getAvailableLand()<0)
            error +=getContext().getString(R.string.error_2)+"\n";
        if(bLogic.getAvailableBCap()<0)
            error +=getContext().getString(R.string.error_3)+"\n";
        if(!tl.checkGoldConsumption())
            error +=getContext().getString(R.string.error_4)+"\n";
        if(!tl.checkValidityOfEmployment())
            error +=getContext().getString(R.string.error_5)+"\n";
         if(!tl.checkValidityOfBuildings())
             error +=getContext().getString(R.string.error_6)+"\n";
        Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
        requestFinished();
    }

    private Context getContext() {
        return GlobalData.getViewManager().activity;
    }

    @Override
    public void onResponse(Call<Summary> call, Response<Summary> response) {
        if(response.code()!=200) {
            retry();
            return;
        }
        if(!response.body().getRecount().equals(GlobalData.getSummary().getRecount())) {
            // recount in progress or new
            String text = getContext().getResources().getString(R.string.Turn_error_recount);
            sentTurnFailure(new Throwable(text));
            return;
        }
        logic.saveChanges(response.body());
        sendTurnSuccess();
    }

    public void retry() {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(GlobalData.getViewManager().activity);
        if(account!=null)
            new ReLoginCallback(GlobalData.getSummary().getUsername(), account.getIdToken(), GlobalData.getSummary().getKingdomId()).call(new Runnable() {
                @Override
                public void run() {
                    TurnCallback.this.call();
                }
            });
    }

    @Override
    public void onFailure(Call<Summary> call, Throwable t) {
        sentTurnFailure(t);
    }
}
