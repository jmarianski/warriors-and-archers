package net.treetank.astro.Rest.Models;

import net.treetank.astro.Rest.Models.Economy.Summary;

/**
 * Created by Jacek on 05.03.2017.
 */

public class Rule {
    public String category;
    public String subcategory;
    public String constant;
    public String value;
}
