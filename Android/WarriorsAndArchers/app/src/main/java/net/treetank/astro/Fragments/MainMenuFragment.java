package net.treetank.astro.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import net.treetank.astro.Activities.SummaryActivity;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Callbacks.LoginCallback;
import net.treetank.astro.Rest.Models.Diplomacy.Kingdom;
import net.treetank.astro.Utils.GlobalData;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static net.treetank.astro.Activities.RegisterUserActivity.RC_SIGN_IN;
import static net.treetank.astro.Utils.GlobalData.getSummary;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainMenuFragment extends Fragment {

    @Bind(R.id.spinner)
    public Spinner kingdoms;
    List<Kingdom> kingdoms_array;
    boolean buttonsDisabled = false;
    private GoogleSignInClient mGoogleSignInClient;


    public MainMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);
        ButterKnife.bind(this, view);
        kingdoms_array = GlobalData.getSummary().getKingdoms();
        ArrayAdapter<Kingdom> dataAdapter3 = new ArrayAdapter<>(getContext(),
                R.layout.spinner, kingdoms_array);
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kingdoms.setAdapter(dataAdapter3);
        int id = GlobalData.getSummary().getKingdomId();
        for(int i=0; i<dataAdapter3.getCount(); i++)
            if(dataAdapter3.getItem(i).id==id)
                kingdoms.setSelection(i);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.server_client_id))
                .build();
        Log.d("token", getString(R.string.server_client_id));
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
        return view;
    }

    @OnClick(R.id.button3)
    public void relogin() {
        if(buttonsDisabled)
            return;
        buttonsDisabled = true;

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @OnClick(R.id.button2)
    public void cont() {
        if(buttonsDisabled)
            return;
        ((SummaryActivity)(GlobalData.getViewManager().activity)).dismissMainMenu();
    }

    @OnClick(R.id.button4)
    public void register() {
        if(buttonsDisabled)
            return;
        getView().findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        new LoginCallback(getSummary().getUsername() , null, 0).call();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);

                int id = ((Kingdom)(kingdoms.getSelectedItem())).id;
                new LoginCallback(account.getEmail(), account.getIdToken(), id).call();
            } catch (ApiException e) {
                // The ApiException status code indicates the detailed failure reason.
                // Please refer to the GoogleSignInStatusCodes class reference for more information.
                Log.w("Astro", "signInResult:failed code=" + e.getStatusCode());
                Toast.makeText(getContext(), R.string.Login_required, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.button5)
    public void signout() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        getActivity().finish();
                    }
                });

    }
}
