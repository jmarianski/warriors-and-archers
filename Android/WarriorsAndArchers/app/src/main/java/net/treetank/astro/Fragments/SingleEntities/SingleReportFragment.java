package net.treetank.astro.Fragments.SingleEntities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Diplomacy.Report;
import net.treetank.astro.Utils.GlobalData;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jack on 04.03.2018.
 */

public class SingleReportFragment  extends SingleEntity {
    private Report report;
    private View root;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.single_war_report, container, false);
        setRetainInstance(false);
        ButterKnife.bind(this, root);
        Bundle args = getArguments();
        int r = args.getInt("report");
        if(r< GlobalData.getSummary().reports.size()) {
            report = GlobalData.getSummary().reports.get(r);
            fillLayout(report);
        }
        setBackgroundOnClick(root);
        updateUI();
        return root;
    }

    private void setBackgroundOnClick(View v) {
        v.findViewById(R.id.background).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {getParentFragment().getChildFragmentManager().popBackStack();}
        });
    }

    private void fillLayout(Report report) {
        if(report==null)
            return;
        ((TextView)(root.findViewById(R.id.title))).setText(report.title);
        ((TextView)(root.findViewById(R.id.text))).setText(report.message);
        ((TextView)(root.findViewById(R.id.date))).setText(report.post_time);
    }

    @OnClick(R.id.title)
    void onTitleClick() {
        Toast.makeText(getContext(), report.post_time, Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateUI() {
    }
}
