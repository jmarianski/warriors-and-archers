package net.treetank.astro.Fragments.Other;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.ArraySet;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import net.treetank.astro.Activities.RegisterUserActivity;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Callbacks.RegisterCallback;
import net.treetank.astro.Rest.Models.Login.Login;
import net.treetank.astro.Rest.Models.Login.Race;
import net.treetank.astro.Rest.Models.Login.Solar_system;
import net.treetank.astro.Utils.GlobalData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jack on 30.01.2018.
 */

public class RegisterFragment extends Fragment {


    @Bind(R.id.user_email)
    public TextView userEmail;
    @Bind(R.id.user_name)
    public EditText userNameEditText;
    @Bind(R.id.user_kingdom)
    public EditText userKingdomEditText;
    @Bind(R.id.user_race)
    public Spinner race;
    @Bind(R.id.solar_system)
    public Spinner solar_system;
    @Bind(R.id.lang)
    public Spinner lang;

    private String mEmail;
    private List<Solar_system> mSystems;
    private List<Race> mRaces;
    private String auth;
    private Object[] languages;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_register_user, container, false);
        ButterKnife.bind(this, view);
        mEmail = getActivity().getIntent().getStringExtra("email");
        auth = getActivity().getIntent().getStringExtra("auth");
        userEmail.setText(mEmail);
        setSpinners();
        return view;
    }

    public void setSpinners() {
        Locale[] locales = Locale.getAvailableLocales();
        ArrayList<String> codes = new ArrayList<String>();
        ArrayList<String> names = new ArrayList<String>();
        for(Locale l:locales) {
            String lang = l.getDisplayLanguage(l);
            lang = lang.substring(0,1).toUpperCase(l) + lang.substring(1);
            lang += " ("+l.getLanguage()+")";
            if(!names.contains(lang)) {
                codes.add(l.getLanguage());
                names.add(lang);
            }
        }
        languages = codes.toArray();
        ArrayAdapter<Object> dataAdapter3 = new ArrayAdapter<>(getContext(),
                R.layout.spinner, names.toArray());
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lang.setAdapter(dataAdapter3);
        lang.setSelection(codes.indexOf(Locale.getDefault().getLanguage()));
        final ArrayAdapter<Solar_system> dataAdapter = new ArrayAdapter<>(getContext(),
                R.layout.spinner, GlobalData.login.solar_systems);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        solar_system.setAdapter(dataAdapter);
        solar_system.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Solar_system item = ((Solar_system)(solar_system.getSelectedItem()));
                if(item.id<=0)
                    return;
                String text = item.detailedToString(getContext());
                Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        lang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String lang_code = (String) languages[adapterView.getSelectedItemPosition()];
                Solar_system ss = ((Solar_system)(GlobalData.login.solar_systems.get(0)));
                Solar_system ss2 = ((Solar_system)(GlobalData.login.solar_systems.get(1)));
                ss.lang = lang_code;
                ss2.lang = lang_code;
                solar_system.setSelection(solar_system.getSelectedItemPosition(), true);
                dataAdapter.notifyDataSetChanged();
                solar_system.invalidate();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        ArrayAdapter<Race> dataAdapter2 = new ArrayAdapter<>(getContext(),
                R.layout.spinner, GlobalData.login.races);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        race.setAdapter(dataAdapter2);
    }

    @OnClick(R.id.registerButton)
    public void registerUser() {
        Intent signInIntent = ((RegisterUserActivity)(GlobalData.getViewManager().activity)).mGoogleSignInClient.getSignInIntent();
        getActivity().startActivityForResult(signInIntent, RegisterUserActivity.RC_SIGN_IN);
    }

    public void setAuth(String idToken) {
        Log.d("setting auth", idToken);
        RegisterCallback register = new RegisterCallback();
        String language = (String)languages[lang.getSelectedItemPosition()];
        register.call(userNameEditText.getText().toString(), idToken, userKingdomEditText.getText().toString(), (Solar_system)(solar_system.getSelectedItem()), (Race)(race.getSelectedItem()), language);
    }



    @OnClick(R.id.logoutButton)
    public void signout() {
        ((RegisterUserActivity)(GlobalData.getViewManager().activity)).signOut();
    }
}
