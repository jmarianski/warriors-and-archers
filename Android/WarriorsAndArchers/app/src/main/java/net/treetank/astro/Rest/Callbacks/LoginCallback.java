package net.treetank.astro.Rest.Callbacks;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.treetank.astro.Activities.SplashActivity;
import net.treetank.astro.Activities.SummaryActivity;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Login.Login;
import net.treetank.astro.Rest.Models.Economy.Summary;
import net.treetank.astro.Rest.Models.Economy.Turn;
import net.treetank.astro.Rest.Models.Login.LoginResult;
import net.treetank.astro.Rest.RestClient;
import net.treetank.astro.Utils.GlobalData;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jacek on 29.01.2017.
 */

public class LoginCallback implements Callback<LoginResult>{
    private final int kingdom;
    String mEmail;
    private String auth;
    public boolean canCall;
    public LoginCallback(String mEmail, String auth, int kingdom) {
        this.mEmail = mEmail;
        this.auth = auth;
        this.kingdom = kingdom;
        canCall = true;
    }


    public void putEmailToSharedPreferences(String userEmail, String kingdom) {
        String sharedPreferencesFile = GlobalData.getViewManager().activity.getResources().getString(R.string.Shared_preference_file);
        SharedPreferences sharedPreferences = GlobalData.getViewManager().activity.getApplicationContext().getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("email", userEmail);
        editor.putString("kingdom", kingdom);
        editor.apply();
    }


    private boolean isDeviceOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) GlobalData.getViewManager().activity.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        Log.d("test", connectivityManager.toString());
        return networkInfo != null && networkInfo.isConnected();
    }
    private void finish() {
        if(GlobalData.getViewManager().activity!=null)
            GlobalData.getViewManager().activity.finish();
    }

    @Override
    public void onResponse(Call<LoginResult> call, Response<LoginResult> response) {
        LoginResult result = response.body();
        if(response.code()==404) {
            Toast.makeText(GlobalData.getViewManager().activity, R.string.Something_went_wrong, Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        else if(response.code()==405) {
            GlobalData.getViewManager().goToServerSelector();
            return;
        }
        if(result!=null) {
            if(result.Recount) {
                Toast.makeText(GlobalData.getViewManager().activity, R.string.recount_in_progress, Toast.LENGTH_LONG).show();
                canCall = true;
                //finish();
            } else if(result.summary==null) {
                GlobalData.login = result;
                GlobalData.getViewManager().goToRegisterActivity(mEmail, auth);
            }
            else {
                Summary summary = result.summary;
                putEmailToSharedPreferences(mEmail, summary.getKingdomName());
                GlobalData.clearData();
                GlobalData.setSummary(summary);
                GlobalData.setCurrentTurn(new Turn(mEmail, summary.getKingdomId(), summary.getRecount(), summary.getTurns()));
                if(GlobalData.getViewManager().hf!=null)
                    GlobalData.getViewManager().hf.updateUI();
                GlobalData.getSharedPreferences().edit().putInt("last_kingdom", summary.getKingdomId()).apply();
                if(GlobalData.getViewManager().activity instanceof SplashActivity) {
                    GlobalData.getViewManager().goToSummaryActivity();
                    GlobalData.startMusicDownload();
                } else if(GlobalData.getViewManager().activity instanceof SummaryActivity) {
                    // dismiss MainMenuFragment
                    ((SummaryActivity)GlobalData.getViewManager().activity).dismissMainMenu();
                }
            }
            return;
        }
        Toast.makeText(GlobalData.getViewManager().activity, R.string.Something_went_wrong, Toast.LENGTH_LONG).show();
        GlobalData.getViewManager().goToServerSelector();
    }

    public void call() {
        if(!canCall)
            Toast.makeText(GlobalData.getViewManager().activity, R.string.cant_call, Toast.LENGTH_LONG).show();
        if (isDeviceOnline()) {
            canCall = false;
            Login login = new Login(mEmail, auth, kingdom);
            RestClient.get().loginUser(login).enqueue(this);
        } else {
            Toast.makeText(GlobalData.getViewManager().activity, R.string.Offline, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void onFailure(Call<LoginResult> call, Throwable t) {
        String text = GlobalData.getViewManager().activity.getResources().getString(R.string.Something_went_wrong)+" "+t.getMessage();
        Toast.makeText(GlobalData.getViewManager().activity, text, Toast.LENGTH_LONG).show();
        SharedPreferences sp = GlobalData.getViewManager().activity.getSharedPreferences(GlobalData.getViewManager().activity.getResources().getString(R.string.Shared_preference_file), Context.MODE_PRIVATE);
        sp.edit().putString("server_url", RestClient.ROOT).commit();
        finish();
    }

}
