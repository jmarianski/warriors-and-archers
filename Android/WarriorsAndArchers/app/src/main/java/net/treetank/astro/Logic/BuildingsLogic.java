package net.treetank.astro.Logic;

import android.util.Log;

import net.treetank.astro.R;
import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;
import net.treetank.astro.Rest.Models.Economy.Building;
import net.treetank.astro.Rest.Models.Economy.Summary;
import net.treetank.astro.Rest.Models.Economy.Turn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BuildingsLogic {
    private ArrayList<BuildingType> buildableBuildings;


    public BuildingsLogic() {
        fillArraysWithData();
    }

    private void fillArraysWithData() {
        buildableBuildings = new ArrayList<>();
        buildableBuildings.add(BuildingType.HOUSE);
        buildableBuildings.add(BuildingType.FIELD);
        buildableBuildings.add(BuildingType.GOLDMINE);
        buildableBuildings.add(BuildingType.MASONS_WORKSHOP);
        buildableBuildings.add(BuildingType.BUILDERS_GUILD);
        buildableBuildings.add(BuildingType.UNIVERSITY);
        buildableBuildings.add(BuildingType.ARMORY);
        buildableBuildings.add(BuildingType.BARRACKS);
        buildableBuildings.add(BuildingType.ARCHERHOUSE);
    }

    public int changeAmountOfBuildings(BuildingType type, int change) {
        Building building = GlobalData.getSummary().getBuilding(type);
        Building changedBuilding = GlobalData.getCurrentTurn().getBuilding(type);
        int buildingChange = changedBuilding.getNumber();
        int canBuild = canBuildAmount(type, change);
        if(canBuild<change)
            change = canBuild;
        int changeVal = change;

        int buildings = building.getNumber();
        change = buildingChange + change;
        int employmentValue = building.getEmployment();
        int employmentChange = changedBuilding.getEmployment();

        if (-change > buildings) {
            change = -buildings;
        }

        if ((employmentValue + employmentChange) > (buildings + change)*GlobalData.getRules().getRuleInt("other", "", "employees_per_building")) {
            EmploymentLogic el = new EmploymentLogic();
            employmentChange = buildings + change - (employmentValue + employmentChange);
            el.changeAmountOfEmployees(building.getType(), employmentChange);
        }
        consumeResources(type, changeVal);
        changedBuilding.setNumber(change);
        return change;
    }

    private void consumeResources(BuildingType type, int amount) {
        if(amount==0)
            return;
        Building changedBuilding = GlobalData.getCurrentTurn().getBuilding(type);
        int change = 0;
        if(changedBuilding.getNumber()<=0 && amount<0) {
            // return land, it goes automatically
        } else if(changedBuilding.getNumber()>0 && amount<0) {
            if(changedBuilding.getNumber() + amount>=0)
                change = amount;
            else
                change = -changedBuilding.getNumber();
        } else if(changedBuilding.getNumber()>0 && amount>0) {
            change = amount;
        } else if(changedBuilding.getNumber()+amount>0)  // amount >0 i changedBuilding.getNumber()<0
                change = changedBuilding.getNumber()+amount;
        if(change==0)
            return;
        HashMap<String, Float> resourcesToConsume = calculateResourcesOnAddToConsume(changedBuilding.getType(), change);
        for(Map.Entry<String, Float> e:resourcesToConsume.entrySet()) {
            GlobalData.getCurrentTurn().getResources().add(e.getKey(), -(int)Math.floor(e.getValue()));
        }
    }

    private HashMap calculateResourcesOnAddToConsume(BuildingType type, int change_plus) {
        HashMap<String, Float> cost = new HashMap<>();
        for(Map.Entry<String, Float> e:GlobalData.getRules().getRulesFloat("cost_add_building", "Building").entrySet()) {
            float val = cost.get(e.getKey())==null?0:cost.get(e.getKey());
            cost.put(e.getKey(), val + e.getValue()*change_plus);
        }
        for(Map.Entry<String, Float> e:GlobalData.getRules().getRulesFloat("cost_add_building", type.toString()).entrySet()) {
            float val = cost.get(e.getKey())==null?0:cost.get(e.getKey());
            cost.put(e.getKey(), val + e.getValue()*change_plus);
        }
        return cost;
    }

    public String buildableReason(BuildingType type) {
        String result = getString(R.string.Land)+": "+getAvailableLand();
        HashMap<String, Float> cost = calculateResourcesOnAddToConsume(type, 1);
        for(Map.Entry<String, Float> e:cost.entrySet()) {
            int resources =  GlobalData.getSummary().getResources().get(e.getKey()) + GlobalData.getCurrentTurn().getResources().get(e.getKey());
            float value =  e.getValue();
            result += "\n"+""+ e.getKey()+ ": " +value +" " +getString(R.string.per)+"; "+resources+" "+getString(R.string.available);
        }
        return result;
    }

    private String getString(int id) {
        return GlobalData.getViewManager().activity.getString(id);
    }

    public ArrayList<BuildingType> getListOfBuildableBuildings() {

        return buildableBuildings;
    }

    public int getCurrentlyBuiltBuildingsNumber(boolean onlyPlusValues) {
        int number = 0;
        List<Building> buildings = GlobalData.getCurrentTurn().getBuildings();
        for (Building building : buildings) {
            int amountOfBuildings = building.getNumber();
            number += ((amountOfBuildings > 0 || !onlyPlusValues) ? amountOfBuildings : 0);
        }
        return number;
    }

    public int getAllBuildingsPreviouslyBuilt() {
        int number = 0;
        List<Building> buildings = GlobalData.getSummary().getBuildings();
        for (Building building : buildings) {
            int amountOfBuildings = building.getNumber();
            number +=  amountOfBuildings;
        }
        return number;

    }

    public int getAvailableBCap() {
        return GlobalData.getSummary().getResources().getBuildingCapacity() + GlobalData.getCurrentTurn().getResources().getBuildingCapacity();
    }

    public int getAvailableBricks() {
        return GlobalData.getSummary().getResources().getBricks() + GlobalData.getCurrentTurn().getResources().getBricks();
    }

    public int getCurrLandUsage() {
        return (getAllBuildingsPreviouslyBuilt() + getCurrentlyBuiltBuildingsNumber(false))*GlobalData.getRules().getRuleInt("max_per_object", "Building", "Land");
    }

    public int getAvailableLand() {
        return GlobalData.getSummary().getResources().getLand() - getCurrLandUsage();
    }

    public int canBuildAmount(BuildingType type, int change) {
        int canBuildAmount = canBuildAmount(type);
        return canBuildAmount<change?canBuildAmount:change;
    }


    public int canBuildAmount(BuildingType btype) {
        int min = getAvailableLand() / GlobalData.getRules().getRuleInt("max_per_object", "Building", "Land");
        HashMap<String, Float> cost = calculateResourcesOnAddToConsume(btype, 1);
        // now that we have all costs for buildings
        for(Map.Entry<String, Float> e:cost.entrySet()) {
            int val = (int) ((GlobalData.getSummary().getResources().get(e.getKey()) + GlobalData.getCurrentTurn().getResources().get(e.getKey())) / e.getValue());
            if(val<min)
                min = val;
        }
        if(min<0)
            min = 0;
        return min;
    }
}
