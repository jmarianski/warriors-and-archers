package net.treetank.astro.Fragments;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import net.treetank.astro.Fragments.SingleEntities.EmploymentListFragment;
import net.treetank.astro.Fragments.SingleEntities.Pager;
import net.treetank.astro.Logic.TurnLogic;
import net.treetank.astro.Utils.GlobalData;
import net.treetank.astro.R;

import butterknife.Bind;
import butterknife.BindDrawable;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HeaderFragment extends Fragment {
    private static final String TAG = "HeaderFragment";

    private TurnLogic logic;

    @Bind(R.id.next_turn)
    public Button nextTurnButton;
    @Bind(R.id.turns)
    public TextView turnsTextView;
    @BindDrawable(R.drawable.klepsydra)
    public Drawable drawableHourglass;
    @BindDrawable(R.drawable.klepsydra00_grey)
    public Drawable drawableHourglassGrey;

    private FragmentManager fragmentManager;
    public AnimationDrawable animationHourglass;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logic = new TurnLogic();
        GlobalData.getViewManager().hf = HeaderFragment.this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_header, container, false);
        ButterKnife.bind(this, view);
        if(GlobalData.turn)
            launchHourglass();
        fragmentManager = getFragmentManager();
        updateUI();

        return view;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        GlobalData.getViewManager().hf = null;
    }

    @OnClick({ R.id.summaryHeaderButton,
            R.id.buildingsHeaderButton,
            R.id.employmentHeaderButton,
            R.id.landHeaderButton,
            R.id.settingsButton,
            R.id.chatButton,
            R.id.warButton,
            R.id.helpButton})
    public void onClickSummary(Button button) {
        GlobalData.player.click();
        while(getFragmentManager().getBackStackEntryCount()>0) {
            Log.d("Back stack count", getFragmentManager().getBackStackEntryCount()+", therefore popping back stack");
            fragmentManager.popBackStackImmediate();
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch(button.getId()) {
            case R.id.summaryHeaderButton: {
                fragmentTransaction
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.summary_fragment_container, new SummaryFragment());
                break;
            }
            case R.id.buildingsHeaderButton: {
                fragmentTransaction
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.summary_fragment_container, new BuildingsFragment());
                break;
            }
            case R.id.employmentHeaderButton: {
                fragmentTransaction
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.summary_fragment_container, new EmploymentFragment());
                break;
            }
            case R.id.landHeaderButton: {
                fragmentTransaction
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.summary_fragment_container, new BuyLandFragment());
                break;
            }
            case R.id.settingsButton: {
                fragmentTransaction
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.summary_fragment_container, new SettingsFragment());
                break;
            }
            case R.id.chatButton: {
                fragmentTransaction
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.summary_fragment_container, new ChatFragment());
                break;
            }
            case R.id.warButton: {
                fragmentTransaction
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.summary_fragment_container, new DiplomacyFragment());
                break;
            }
            case R.id.helpButton: {
                Pager p = new Pager();
                Bundle b = new Bundle();
                b.putInt("type", 5);
                b.putInt("position", 0);
                p.setArguments(b);
                fragmentTransaction
                        .setCustomAnimations(R.anim.alpha_in, R.anim.alpha_out, R.anim.alpha_in, R.anim.alpha_out)
                        .addToBackStack(null)
                        .replace(R.id.main_menu_fragment_container, p);
                break;
            }
        }
        fragmentTransaction.commit();
    }

    public void launchHourglass() {
        animationHourglass = (AnimationDrawable) (ContextCompat.getDrawable(getContext(), R.drawable.klepsydra));
        nextTurnButton.setBackgroundDrawable(animationHourglass);
        nextTurnButton.setClickable(false);
        animationHourglass.start();
    }

    @OnClick(R.id.next_turn)
    public void onClickNextTurn(Button button) {
        Toast.makeText(getContext(), R.string.Next_turn, Toast.LENGTH_SHORT).show();

        GlobalData.setBlockButtons(true);
        launchHourglass();
        GlobalData.turn = true;

        logic.executeTurn();
    }

    public void updateUI() {
        int turns = GlobalData.getSummary().getTurns();
        int max_turns = GlobalData.getRules().getRuleInt("other", "", "num_turns_per_recount");
        turnsTextView.setText(String.format("%d/%d", turns, max_turns));
        if(turns>=max_turns) {
            nextTurnButton.setClickable(false);
            nextTurnButton.setBackgroundDrawable(drawableHourglassGrey);
        }else {
            nextTurnButton.setClickable(true);
            nextTurnButton.setBackgroundDrawable(drawableHourglass);
        }
    }

}
