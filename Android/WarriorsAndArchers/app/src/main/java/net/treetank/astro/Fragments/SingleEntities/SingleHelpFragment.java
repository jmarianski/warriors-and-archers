package net.treetank.astro.Fragments.SingleEntities;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import net.treetank.astro.R;
import net.treetank.astro.Utils.GlobalData;

/**
 * A simple {@link Fragment} subclass.
 */
public class SingleHelpFragment extends Fragment {


    public SingleHelpFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_help, container, false);
        int page = getArguments().getInt("position", 0);
        WebView web =(view.findViewById(R.id.web));
        web.setBackgroundColor(0);
        web.loadUrl("file:///android_asset/help/"+ GlobalData.help.lang+"/"+page+".html");
        web.setOnTouchListener(OTL);
        return view;
    }

    View.OnTouchListener OTL = new View.OnTouchListener() {

        public boolean onTouch(View v, MotionEvent event) {
            WebView.HitTestResult hr = ((WebView)v).getHitTestResult();
            if(hr.getType()== WebView.HitTestResult.SRC_ANCHOR_TYPE) {
                String webpage = hr.getExtra();
                try {
                    String after_hash = webpage.substring(webpage.indexOf("#")+1);
                    Log.d("astro", after_hash);
                    int page = Integer.parseInt(after_hash);
                    go_to_page(page);
                } catch (IllegalArgumentException e) {}
                catch (IndexOutOfBoundsException e) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(webpage));
                    startActivity(browserIntent);
                }
            }
            return false;
        }
    };

    public void go_to_page(int page) {
        for (Fragment f : getFragmentManager().getFragments()) {
            if (f instanceof Pager) {
                Log.d("astro", ((Pager) f).getType()+"");
                Log.d("astro", ((Pager) f).getChildCount()+"");
                 if (((Pager) (f)).getType() == 5 && ((Pager) f).getChildCount() > page) {
                    ((Pager) f).mPager.setCurrentItem(page, true);
                }
            }
        }
    }

}
