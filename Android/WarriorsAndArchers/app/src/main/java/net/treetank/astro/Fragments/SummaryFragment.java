package net.treetank.astro.Fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.treetank.astro.Fragments.SingleEntities.Pager;
import net.treetank.astro.Logic.ResourcesLogic;
import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Economy.Summary;

import java.util.Locale;

import butterknife.OnClick;

public class SummaryFragment extends WarriorsFragment {

    ResourcesLogic logic;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void updateUI() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_summary, container, false);
        logic = new ResourcesLogic();
        populateTableData(view);
        return view;
    }

    private void populateTableData(View v) {
        Summary summary = GlobalData.getSummary();
        setValue(v, R.id.land_value, summary.getResources().getLand());
        setValue(v, R.id.people_value, summary.getResources().getPeople());
        setValue(v, R.id.happiness_value, summary.getResources().getHappiness());
        setValue(v, R.id.gold_value, summary.getResources().getGold());
        setValue(v, R.id.food_value, summary.getResources().getFood());
        setValue(v, R.id.bricks_value, summary.getResources().getBricks());
        setValue(v, R.id.armaments_value, summary.getResources().getArmaments());
        setValue(v, R.id.bcap_value, summary.getResources().getBuildingCapacity());
        setValue(v, R.id.science_value, summary.getResources().getScience());
        if(summary.changes!=null) {
            setChange(v, R.id.land_change,  summary.changes.getResources().getLand());
            setChange(v, R.id.people_change,  summary.changes.getResources().getPeople());
            setChange(v, R.id.happiness_change,  summary.changes.getResources().getHappiness());
            setChange(v, R.id.gold_change,  summary.changes.getResources().getGold());
            setChange(v, R.id.food_change,  summary.changes.getResources().getFood());
            setChange(v, R.id.bricks_change,  summary.changes.getResources().getBricks());
            setChange(v, R.id.armaments_change,  summary.changes.getResources().getArmaments());
            setChange(v, R.id.bcap_change,  summary.changes.getResources().getBuildingCapacity());
            setChange(v, R.id.science_change,  summary.changes.getResources().getScience());
        }
        View.OnClickListener OCL = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = logic.getCodeForTextViewId(view.getId());
                onClickName(code);
            }
        };
        for(Integer id: logic.getTextViewIds()) {
            v.findViewById(id).setOnClickListener(OCL);
        }
    }

    private void setValue(View v, int id, int value) {
        TextView text = (TextView) v.findViewById(id);
        text.setText(String.format(Locale.getDefault(), "%d",value));
    }

    private void setChange(View v, int id, int value) {
        TextView text = (TextView) v.findViewById(id);
        text.setText(String.format(Locale.getDefault(), "%d",value));
        if(value<0)
            text.setTextColor(0xffff0000);
        else if(value>0)
            text.setTextColor(0xff00cc00);
        else
            text.setTextColor(0xffffffff);
    }

    public void onClickName(String code) {
        Pager mbf = new Pager();
        Bundle b = new Bundle();
        b.putInt("type", 4);
        b.putInt("position", logic.index(code));
        mbf.setArguments(b);
        getChildFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.alpha_in, R.anim.alpha_out, R.anim.alpha_in, R.anim.alpha_out)
                .add(R.id.parentContainer, mbf)
                .addToBackStack(null)
                .commit();
    }

}
