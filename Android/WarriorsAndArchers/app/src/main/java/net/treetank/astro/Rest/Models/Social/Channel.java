package net.treetank.astro.Rest.Models.Social;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jacek on 06.02.2017.
 */

public class Channel {
    @SerializedName("id")
    public int id;
    @SerializedName("channelname")
    public String name;
    @SerializedName("channeldesc")
    public String desc;
    @SerializedName("last_update")
    public String last_update;
    public String local_last_update = "0000-00-00 00:00:00";
    public List<Message> messages;
}
