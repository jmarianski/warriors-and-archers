package net.treetank.astro.Logic;

import net.treetank.astro.Rest.Models.Economy.Summary;
import net.treetank.astro.Rest.Models.Economy.Turn;
import net.treetank.astro.Utils.GlobalData;

public class BuyLandLogic {


    public void updateStrings(int goldConsumed, int value) {
        GlobalData.getCurrentTurn().getResources().setGold(-goldConsumed);
        GlobalData.getCurrentTurn().getResources().setLand(value);
    }

    public int generateLandPossibleToBuy() {
        // it works as long as it's the only source of gold spending
        int x = GlobalData.getRules().getRuleInt("other", "", "price_for_soil_per_soil");
        double sx = Math.sqrt(x);

        int gold = GlobalData.getSummary().getResources().getGold();
        int land = GlobalData.getSummary().getResources().getLand();
        if(gold<=0)
            return 0;
        double N = (Math.sqrt(8*gold + 4*land*land*x + 4*land*x + x) - 2*land*sx - sx)/2/sx;
        return (int) N;
    }

    public int generateConsumedGold(int value) {
        int x = GlobalData.getRules().getRuleInt("other", "", "price_for_soil_per_soil");
        int land = GlobalData.getSummary().getResources().getLand();
        return (2*land + value + 1)*value*x/2;
    }

    public int setNewLand(int newLand) {
        int maximum = generateLandPossibleToBuy();
        if(newLand>maximum)
            newLand = maximum;
        if(newLand<0)
            newLand = 0;
        int goldConsumed = generateConsumedGold(newLand);
        GlobalData.getCurrentTurn().getResources().setGold(-goldConsumed);
        GlobalData.getCurrentTurn().getResources().setLand(newLand);
        return newLand;
    }
}

