package net.treetank.astro.Rest.Models.Diplomacy;

import java.util.List;

/**
 * Created by Jacek on 05.03.2017.
 */

public class Group {
    public String name;
    public List<Kingdom> kingdoms;
}
