package net.treetank.astro.Rest.Models;

import net.treetank.astro.Utils.GlobalData;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jacek on 15.01.2018.
 */

public class Rules {


    public HashMap<String, Integer> getRulesInt(String category, String subcategory) {
        HashMap<String, String> subresult = getRules(category,subcategory);
        HashMap<String, Integer> result = new HashMap<>();
        for(Map.Entry<String, String> e:subresult.entrySet()) {
            result.put(e.getKey(), Integer.parseInt(e.getValue()));
        }
        return result;
    }

    public HashMap<String, Float> getRulesFloat(String category, String subcategory) {
        HashMap<String, String> subresult = getRules(category,subcategory);
        HashMap<String, Float> result = new HashMap<>();
        for(Map.Entry<String, String> e:subresult.entrySet()) {
            result.put(e.getKey(), Float.parseFloat(e.getValue()));
        }
        return result;
    }

    public int getRuleInt(String category, String subcategory, String constant) {
        return Integer.parseInt(getRule(category, subcategory, constant));
    }

    public float getRuleFloat(String category, String subcategory, String constant) {
        return Float.parseFloat(getRule(category, subcategory, constant));
    }

    public String getRule(String category, String subcategory, String constant) {
        for(Rule r:GlobalData.getSummary().rules)
            if(r.category.equals(category) && r.subcategory.equals(subcategory) && r.constant.equals(constant))
                return r.value;
        return "0";
    }

    public HashMap<String, String>  getRules(String category, String subcategory) {
        HashMap<String, String> result = new HashMap<>();
        for(Rule r:GlobalData.getSummary().rules)
            if(r.category.equals(category) && r.subcategory.equals(subcategory))
                result.put(r.constant, r.value);
        return result;
    }

}
