package net.treetank.astro.Rest.Models;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import net.treetank.astro.Activities.SummaryActivity;
import net.treetank.astro.Fragments.ChatFragment;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Callbacks.ChatCallback;
import net.treetank.astro.Rest.Models.Social.Channel;
import net.treetank.astro.Rest.Models.Social.ChatResponse;
import net.treetank.astro.Rest.Models.Social.Message;
import net.treetank.astro.Rest.RestClient;
import net.treetank.astro.Utils.GlobalData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jacek on 12.02.2017.
 */

public class ChatManager {

    public ChatFragment.ChatAdapter chatAdapter;
    public ChatCallback callback = new ChatCallback();
    public boolean loading = true;
    public Channel channel = null;
    public int viewingWhat = 0;
    public List<Channel> channels = null;
    public int refreshThreads = 0;

    public void finishedLoadingChannels(List<Channel> channels) {
        this.channels = channels;
        updateLoading(false);
        chatAdapter.addAll(channels);
        chatAdapter.notifyDataSetChanged();
    }

    public void channelClicked(Channel o) {
        this.channel = o;
        chatAdapter.clear();
        if(channel.messages==null) {
            updateLoading(true);
            chatAdapter.notifyDataSetChanged();
            callback.getMessages();
        } else {
            updateLoading(false);
            chatAdapter.addAll(channel.messages);
            ((ChatFragment) (GlobalData.getViewManager().fragment)).forceScrollBottom();
            callback.autoUpdate();
        }
    }

    private void updateLoading(boolean var) {
        loading = var;
        if(GlobalData.getViewManager().fragment instanceof ChatFragment) {
            ((ChatFragment) (GlobalData.getViewManager().fragment)).updateUI();
        }
    }

    public void clickedBackInsideChannel() {
        viewingWhat = 0;
        channel = null;
        updateLoading(false);
        chatAdapter.clear();
        chatAdapter.addAll(channels);
    }

    public void gotChannel(ChatResponse response) {
        List<Message> messages = response.messages;
        if(messages!=null) {
            for (Channel c : channels) {
                if (c.id == channel.id) { // jeżeli wiadomości należą do tego channelu, uzupełnij je
                    if (c.messages == null)
                        c.messages = new ArrayList<Message>();
                    c.local_last_update = response.channel.last_update;
                    c.messages.addAll(messages);
                }
            }
            if (this.channel.id == channel.id) { // jeżeli channel aktualny jest równy temu z requestu, uzupełnij adapter
                int count = chatAdapter.getCount();
                chatAdapter.addAll(messages);
                this.channel.local_last_update = ((Message)(messages.toArray()[messages.size()-1])).time;
                if(count==0)
                    ((ChatFragment) (GlobalData.getViewManager().fragment)).forceScrollBottom();
                else
                    ((ChatFragment) (GlobalData.getViewManager().fragment)).scrollBottom();
            }
            if(!(GlobalData.getViewManager().fragment instanceof ChatFragment) && messages.size()>0)
                makeASound();
        }
        updateLoading(false);
        callback.autoUpdate();
    }

    private void makeASound() {
        if(GlobalData.getViewManager().activity!=null) {
            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(GlobalData.getViewManager().activity)
                            .setSmallIcon(R.drawable.button_summary)
                            .setContentTitle("New message")
                            .setContentText("You've just received a new message!");
            Intent notificationIntent = new Intent(GlobalData.getViewManager().activity, SummaryActivity.class);

            PendingIntent contentIntent = PendingIntent.getActivity(GlobalData.getViewManager().activity, 0, notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            builder.setContentIntent(contentIntent);
            builder.setAutoCancel(true);
            builder.setStyle(new NotificationCompat.InboxStyle());
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder.setSound(alarmSound);
            NotificationManager manager = (NotificationManager) GlobalData.getViewManager().activity.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(1, builder.build());
        }
    }

    public void clearData() {
        updateLoading(false);
        chatAdapter = null;
    }
}
