package net.treetank.astro.Rest.Models.Login;

public class Login {
    @SuppressWarnings("unused")
    private String login;
    @SuppressWarnings("unused")
    private String auth;
    private int kingdom;


    public Login(String login, String auth, int kingdom) {
        this.login = login;
        this.auth = auth;
        if(kingdom>0)
            this.kingdom = kingdom;

    }
}