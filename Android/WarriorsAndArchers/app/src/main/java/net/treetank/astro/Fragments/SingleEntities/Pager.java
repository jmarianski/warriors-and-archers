package net.treetank.astro.Fragments.SingleEntities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.treetank.astro.Logic.BuildingsLogic;
import net.treetank.astro.Logic.DiplomacyLogic;
import net.treetank.astro.Logic.EmploymentLogic;
import net.treetank.astro.Logic.ResourcesLogic;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Diplomacy.Battle;
import net.treetank.astro.Utils.GlobalData;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Pager extends Fragment {
    public ScreenSlidePagerAdapter mPagerAdapter;
    public ViewPager mPager;
    private int type;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager, container, false);
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) view.findViewById(R.id.pager);
        type = getArguments().getInt("type");
        mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager(), type);
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(getArguments().getInt("position"));

        GlobalData.diplomacy.current = this;
    return view;
    }

    public int getType() {
        return type;
    }

    public int getChildCount() {
        return mPagerAdapter.getCount();
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private int type;
        ArrayList<Fragment> fragments;
        List<?> element_ids;
        public ScreenSlidePagerAdapter(FragmentManager fm, int type) {
            super(fm);
            fragments = new ArrayList<>();
            this.type = type;
            switch(type) {
                default: {
                    BuildingsLogic buildingsLogic = new BuildingsLogic();
                    element_ids = buildingsLogic.getListOfBuildableBuildings();
                    break;}
                case 1: {
                    EmploymentLogic employmentLogic = new EmploymentLogic();
                    element_ids = employmentLogic.getListOfEmployableBuildings();
                    break;}
                case 2: {
                    element_ids = GlobalData.diplomacy.battles;
                    break;}
                case 3: {
                    element_ids = GlobalData.getSummary().reports;
                    break;}
                case 4: {
                    element_ids = new ResourcesLogic().getList();
                    break;}
                case 5: {
                    element_ids = null;
                    break;}
            }
        }

        @Override
        public Fragment getItem(int position) {
            switch(type) {
                default: {
                    Fragment f;
                    if(position==0) {
                        f = new BuildingsListFragment();
                    } else {
                        Object btype = element_ids.get(position-1);
                        f = new SingleBuildingFragment();
                        Bundle b = new Bundle();
                        b.putString("building", btype.toString());
                        f.setArguments(b);
                    }
                    return f;}
                case 1: {
                    Fragment f;
                    if(position==0) {
                        f = new EmploymentListFragment();
                    } else {
                        Object btype = element_ids.get(position-1);
                        f = new SingleJobFragment();
                        Bundle b = new Bundle();
                        b.putString("building", btype.toString());
                        f.setArguments(b);
                    }
                    return f;}
                case 2: {
                    int size = GlobalData.diplomacy.battles.size();
                    if(position>=size) {
                        AddFragment f = new AddFragment();
                        f.type = 2;
                        return f;
                    }
                    Fragment f = new SingleBattleFragment();
                    Bundle b = new Bundle();
                    b.putInt("battle", position); // toString has id
                    f.setArguments(b);
                    return f;}
                case 3: {
                    Fragment f = new SingleReportFragment();
                    Bundle b = new Bundle();
                    b.putInt("report", position); // toString has id
                    f.setArguments(b);
                    return f;}
                case 4: {
                    Fragment f = new SingleResource();
                    Bundle b = new Bundle();
                    b.putString("resource", (String)(element_ids.get(position))); // toString has id
                    f.setArguments(b);
                    return f;}
                case 5: {
                    Fragment f = new SingleHelpFragment();
                    Bundle b = new Bundle();
                    b.putInt("position", position); // toString has id
                    f.setArguments(b);
                    return f;}
            }
        }

        @Override
        public int getCount() {
            if(type==5)
                return GlobalData.help.count;
            if(type>=3)
                return element_ids.size();
            return element_ids.size()+1;
            //return element_ids.size();
        }
    }
    public void addItem(int type) {
        switch(type) {
            case 2:
                GlobalData.diplomacy.battles.add(Battle.make());
            break;
        }
        //mPagerAdapter.notifyDataSetChanged();
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(mPagerAdapter.getCount()-2,true);
    }

    public void removeItem(int hash) {
        for(int i=0; i<GlobalData.diplomacy.battles.size(); i++) {
            if(GlobalData.diplomacy.battles.get(i).hash==hash)
                GlobalData.diplomacy.battles.remove(i);
        }
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(mPagerAdapter.getCount()-1,true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
