package net.treetank.astro.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.audiofx.Equalizer;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import net.treetank.astro.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Jacek on 21.01.2018.
 */

public class Player {
    private MediaPlayer music;
    private MediaPlayer sound;
    private String last_played_sound = "";
    private ArrayList<String> music_list = new ArrayList<>();
    private String currently_played_list = "main_menu";
    private boolean music_turned_down = false;

    private void playMusicNow(MediaPlayer mp) {
        //File f = GlobalData.getViewManager().activity.getFilesDir().getAbsoluteFile();
        //File[] files = f.listFiles();
        AssetFileDescriptor chosen;
        String path;
        String ext;
        try {
            String[] list = GlobalData.getViewManager().activity.getAssets().list("music/"+currently_played_list);
            chosen = GlobalData.getViewManager().activity.getAssets().openFd("music/"+currently_played_list+"/"+list[(int)(Math.round(Math.random()*(list.length-1)))]);
            mp.setDataSource(chosen.getFileDescriptor(), chosen.getStartOffset(), chosen.getLength());
            mp.prepare();
            mp.start();
            playing = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static boolean playing = false;

    private MediaPlayer.OnCompletionListener OCL = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            if(mediaPlayer==sound) {
                sound.reset();
                if(music_turned_down)
                    turnDownMusic(false);
                return;
            }
            mediaPlayer.reset();
            if(playing)
                playMusicNow(mediaPlayer);
        }
    };

    public void musicDownloadFinished() {
        if(GlobalData.getViewManager().activity==null)
            return;
        if(music ==null) {
            music = new MediaPlayer();
            sound = new MediaPlayer();
            music.setOnCompletionListener(OCL);
            sound.setOnCompletionListener(OCL);
            updateVolumeAndPlay(); // automatic play
            updateSoundVolume();
        } else if(!music.isPlaying())
            music.start();
    }

    public void updateVolumeAndPlay() {
        if(GlobalData.getViewManager().activity!=null) {
            SharedPreferences sp = GlobalData.getViewManager().activity.getSharedPreferences(GlobalData.getViewManager().activity.getString(R.string.Shared_preference_file), Context.MODE_PRIVATE);
            int volint = sp.getInt("settings_music_volume",50);
            float volume = (float) (1-Math.log(100 - volint)/Math.log(100));
            if(volint==100) volume = 1;
            if(!music.isPlaying() && volint>0) // cant pause music, therefore it must have been not started
                playMusicNow(music);
            try {
                music.setVolume(volume, volume);
            } catch (IllegalStateException e) {e.printStackTrace();}
        }
    }
    public void updateSoundVolume() {
        if(GlobalData.getViewManager().activity!=null) {
            SharedPreferences sp = GlobalData.getViewManager().activity.getSharedPreferences(GlobalData.getViewManager().activity.getString(R.string.Shared_preference_file), Context.MODE_PRIVATE);
            int volint = sp.getInt("settings_sound_volume",50);
            float volume = (float) (1-Math.log(100 - volint)/Math.log(100));
            if(volint==100) volume = 1;
            try {
                sound.setVolume(volume, volume);
            } catch (IllegalStateException e) {} // doin' nothin
        }
    }

    public void next(String directory) {
        if(directory!=null && directory.length()>0)
            currently_played_list = directory;
        if(music!=null && music.isPlaying())
            music.reset();
        playMusicNow(music);
    }

    public void play() {
        if(music!=null) {
            music.start();
        }
    }

    public void pause() {
        if(music!=null) {
            if(music.isPlaying())
                music.pause();
        }
    }

    public void click() {
        try {
            String[] list = GlobalData.getViewManager().activity.getAssets().list("sounds/page");
            String chosen = list[(int)(Math.round(Math.random()*(list.length-1)))];
            playSound("page/"+chosen);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void success() {
        String[] sounds = {"clock.mp3"};
        int index = (int) Math.round(Math.random()*(sounds.length-1));
        turnDownMusic(true);
        playSound(sounds[index]);
    }

    private void playSound(String sound_name) {
        try {
            if(sound==null)
                return;
            if(sound.isPlaying() && last_played_sound.equals(sound_name)) {
                sound.seekTo(0); // else do nothing
            } else {
                if(sound.isPlaying()) {
                    sound.reset();
                }
                AssetFileDescriptor chosen_sound = GlobalData.getViewManager().activity.getAssets().openFd("sounds/"+sound_name);
                Log.d("astro", sound_name);
                Log.d("astro", chosen_sound.getFileDescriptor().toString());
                sound.setDataSource(chosen_sound.getFileDescriptor(), chosen_sound.getStartOffset(), chosen_sound.getLength());
                sound.prepare();
                sound.start();
                last_played_sound = sound_name;
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void turnDownMusic(boolean lower) {
        SharedPreferences sp = GlobalData.getViewManager().activity.getSharedPreferences(GlobalData.getViewManager().activity.getString(R.string.Shared_preference_file), Context.MODE_PRIVATE);
        try {
            int volint = sp.getInt("settings_music_volume",50)/(lower?3:1);
            float volume = (float) (1-Math.log(100 - volint)/Math.log(100));
            if(volint==100) volume = 1;
            music.setVolume(volume, volume);
            music_turned_down = lower;

        } catch (IllegalStateException e) {e.printStackTrace();}

    }


    private static String getSoundPath(String name) {
        File dir = GlobalData.getViewManager().activity.getFilesDir().getAbsoluteFile();
        File[] files = dir.listFiles();
        for(File f:files) {
            if(f.getName().equals(name))
                return f.getAbsolutePath();
        }
        return null;
    }

    public void addPlayableMusic(String fname) {
        music_list.add(fname);
        if(music_list.size()==1)
            musicDownloadFinished();
    }
}
