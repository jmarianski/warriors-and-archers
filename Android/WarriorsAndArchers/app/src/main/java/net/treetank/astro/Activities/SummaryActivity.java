package net.treetank.astro.Activities;

import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import net.treetank.astro.Fragments.HeaderFragment;
import net.treetank.astro.Fragments.MainMenuFragment;
import net.treetank.astro.Fragments.SummaryFragment;
import net.treetank.astro.Fragments.WarriorsFragment;
import net.treetank.astro.Utils.GlobalData;
import net.treetank.astro.R;

import java.util.List;

public class SummaryActivity extends WarriorsActivity {

    private Boolean once = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        if (findViewById(R.id.summary_fragment_container) != null) {

            if (savedInstanceState != null) {
                return;
            }
            GlobalData.updateTheme(this);
            SummaryFragment firstFragment = new SummaryFragment();
            HeaderFragment headerFragment = new HeaderFragment();

            firstFragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().add(R.id.header_fragment_container, headerFragment).commit();
            getSupportFragmentManager().beginTransaction().add(R.id.summary_fragment_container, firstFragment).commit();
            showMainMenu();
        }
        if(!once) {
            GlobalData.setBlockButtons(false);
            once = true;
        }
    }

    public void showMainMenu() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.main_menu_fragment_container, new MainMenuFragment())
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        GlobalData.player.play();
    }

    @Override
    protected void onPause() {
        super.onPause();
        GlobalData.player.pause();
    }

    private boolean onBackPressed(FragmentManager fm) {
        List<Fragment> fragList = fm.getFragments();
        if (fragList != null && fragList.size() > 0) {
            for (Fragment frag : fragList) {
                if (frag == null) {
                    continue;
                }
                if (frag.isVisible()) {
                    if (onBackPressed(frag.getChildFragmentManager())) {
                        return true;
                    }
                }
            }
        }
        if (fm != null) {
            if (fm.getBackStackEntryCount() > 0) {
                fm.popBackStack();
                return true;
            }
            if(GlobalData.getViewManager().fragment instanceof WarriorsFragment)
                return ((WarriorsFragment) GlobalData.getViewManager().fragment).onBackPressed();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (onBackPressed(fm)) {
            return;
        }
        GlobalData.getViewManager().fragment = null;
        GlobalData.getViewManager().activity = null;
        super.onBackPressed();
    }

    public void dismissMainMenu() {
        for(Fragment fragment:getSupportFragmentManager().getFragments()){
            if(fragment instanceof MainMenuFragment)
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.alpha_in, R.anim.alpha_out, R.anim.alpha_in, R.anim.alpha_out)
                        .remove(fragment)
                        .commit();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.summary_fragment_container, new SummaryFragment()).commit();
        GlobalData.player.next("standard");
    }
}
