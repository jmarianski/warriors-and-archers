package net.treetank.astro.Rest.Callbacks;

import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import net.treetank.astro.Fragments.DiplomacyFragment;
import net.treetank.astro.Rest.Models.Diplomacy.Battle;
import net.treetank.astro.Rest.Models.Diplomacy.DiplomaticRequest;
import net.treetank.astro.Rest.Models.Diplomacy.DiplomaticResponse;
import net.treetank.astro.Rest.Models.Diplomacy.Kingdom;
import net.treetank.astro.Rest.Models.Social.ChatRequest;
import net.treetank.astro.Rest.RestClient;
import net.treetank.astro.Utils.GlobalData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jacek on 19.03.2017.
 */

public class DiplomacyCallback  implements Callback<DiplomaticResponse> {

    @Override
    public void onResponse(Call<DiplomaticResponse> call, Response<DiplomaticResponse> response) {
        if(response.isSuccessful()) {
            DiplomaticResponse r = response.body();
            if(r.Recount=="true") {
                onRecount();
                return;
            }
            if(response.code()!=200) {
                retry(r.requestType, r.battle);
                return;
            }
            if(r.responseType==1) {
                if(r.requestType==5) {
                    GlobalData.diplomacy.removeBattle(r.battle.hash);
                }
                if(r.requestType==3) {
                    for(Battle b:GlobalData.diplomacy.battles)
                        if(b.hash==r.battle.hash)
                            b.copyValuesFromNet(r.battle);
                    GlobalData.diplomacy.battleSaved();
                }
                if(r.requestType==4) {
                    for(Battle b:GlobalData.diplomacy.battles)
                        if(b.id==r.battle.id)
                            b.copyValuesFromNet(r.battle);
                    GlobalData.diplomacy.battleSaved();
                }
                if(r.requestType==2) {
                    GlobalData.diplomacy.battles = new ArrayList<Battle>();
                    for(Battle b:r.battles) {
                        Battle battle = Battle.make();
                        battle.copyValuesFromNet(b);
                        battle.saved = true;
                        GlobalData.diplomacy.battles.add(battle);
                    }
                    GlobalData.diplomacy.battlesLoaded();
                }
                if(r.requestType==1) {
                    GlobalData.diplomacy.solar_system = r.group.name;
                    List<Kingdom> players =  r.group.kingdoms;
                    if(players==null)
                        players = new ArrayList<Kingdom>();
                    GlobalData.diplomacy.players = players;
                    GlobalData.diplomacy.allPlayers.clear();
                    GlobalData.diplomacy.allPlayers.addAll(players);
                    GlobalData.diplomacy.playersLoaded();
                    getBattles();
                }
            }
        }
    }

    public void onRecount() {

    }

    public void retry(final int requestType, final Battle battle) {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(GlobalData.getViewManager().activity);
        if(account!=null)
            new ReLoginCallback(GlobalData.getSummary().getUsername(), account.getIdToken(), GlobalData.getSummary().getKingdomId()).call(new Runnable() {
                @Override
                public void run() {
                    DiplomacyCallback.this.call(requestType, battle);
                }
            });
    }

    @Override
    public void onFailure(Call<DiplomaticResponse> call, Throwable t) {

    }

    private void call(int requestType, Battle b) {
        DiplomaticRequest r = new DiplomaticRequest(requestType, b);
        RestClient.get().diplomaticRequest(r).enqueue(this);
    }

    public void getPlayers() {
        call(1, null);
    }

    public void getBattles() {
        call(2, null);
    }
    public void addBattle(Battle b) {
        call(3, b);
    }
    public void editBattle(Battle b) {
        call(4, b);
    }
    public void deleteBattle(Battle b) {
        call(5, b);
    }
}
