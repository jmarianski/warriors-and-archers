package net.treetank.astro.Fragments.SingleEntities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import net.treetank.astro.R;

import java.security.Key;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.OnEditorAction;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;

import static android.view.inputmethod.EditorInfo.IME_ACTION_DONE;

public abstract class SingleEntity extends Fragment {
    protected Handler handler;
    private Timer timer;
    @Nullable @Bind(R.id.change_value2)
    EditText change_value2;
    final String entityString = this.getClass().getSimpleName();

    @Nullable @OnTextChanged(R.id.change_value2)
    public void onChangeChanged() {
        if(change_value2.isFocused()) {
            SharedPreferences sp = getSharedPreferences();
            try {
                sp.edit().putInt("change_" + entityString, Integer.parseInt(change_value2.getText().toString())).apply();
            } catch (NumberFormatException e) {
                sp.edit().putInt("change_" + entityString, 0).apply();
            }
        }
    }


    public SingleEntity() {
    }

    @Override
    public void onResume() {
        super.onResume();
        if(change_value2!=null)
            change_value2.setText(String.valueOf(getSharedPreferences().getInt("change_" + entityString, 10)));
        handler = new Handler();
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                final Handler h = SingleEntity.this.handler;
                if(h!=null) {
                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            updateUI();
                            if(change_value2!=null && !change_value2.isFocused()) {
                                if(isAdded())
                                    change_value2.setText(String.valueOf(getSharedPreferences().getInt("change_" + entityString, 10)));
                            }
                        }
                    });
                }
            }
        }, 1000, 200+(int)(Math.random()*200));
    }

    public abstract void updateUI();

    @Override
    public void onPause() {
        super.onPause();
        timer.cancel();
    }

    SharedPreferences getSharedPreferences() {
        return  getContext().getSharedPreferences(getString(R.string.Shared_preference_file), Context.MODE_PRIVATE);
    }
}
