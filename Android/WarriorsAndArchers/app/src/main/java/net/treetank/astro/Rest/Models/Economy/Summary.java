package net.treetank.astro.Rest.Models.Economy;

import net.treetank.astro.Rest.Models.Diplomacy.Kingdom;
import net.treetank.astro.Rest.Models.Diplomacy.Report;
import net.treetank.astro.Rest.Models.Rule;
import net.treetank.astro.Utils.BuildingType;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class Summary {
    @SerializedName("Username")
    @SuppressWarnings("unused")
    private String username;
    @SerializedName("KingdomName")
    @SuppressWarnings("unused")
    private String kingdomName;
    @SerializedName("KingdomId")
    private int kingdomId;
    @SerializedName("Turns")
    private int turns;
    @SerializedName("Turn_Message")
    private String turn_message;
    @SerializedName("Buildings")
    private List<Building> buildings;
    @SerializedName("Resources")
    private Resources resources;
    @SerializedName("Kingdoms")
    private List<Kingdom> kingdoms;
    @SerializedName("Recount")
    private String recount;
    @SerializedName("Reports")
    public List<Report> reports;
    @SerializedName("Rules")
    public List<Rule> rules;
    @SerializedName("Changes")
    public Turn changes;
    @SerializedName("Download")
    public Map<String, String[]> download;

    public Summary() {}

    public Summary(String recount, String username, String kingdomName, int turns, List<Building> buildings, Resources resources) {
        this.recount = recount;
        this.username = username;
        this.kingdomName = kingdomName;
        this.turns = turns;
        this.buildings = buildings;
        this.resources = resources;
    }

    public int getTurns() {
        return turns;
    }

    public Building getBuilding(BuildingType buildingType) {
        for (Building building : buildings)
            if (building.getType() == buildingType)
                return building;
        return null;
    }

    public List<Building> getBuildings() {
        return buildings;
    }

    public Resources getResources() {
        return resources;
    }

    public void setResources(Resources resources) {
        this.resources = resources;
    }

    public String getKingdomName() {
        return kingdomName;
    }

    public String getRecount() {
        return recount;
    }

    public int getKingdomId() {
        return kingdomId;
    }

    public String getTurnMessage() {
        return turn_message;
    }

    public List<Kingdom> getKingdoms() {
        return kingdoms;
    }

    public String getUsername() {
        return username;
    }
}
