package net.treetank.astro.Rest.Models.Diplomacy;

/**
 * Created by Jacek on 05.03.2017.
 */

public class Kingdom {
    public int id;
    public String name;
    public int land;

    @Override
    public String toString() {
        return name + " " + land;
    }
}
