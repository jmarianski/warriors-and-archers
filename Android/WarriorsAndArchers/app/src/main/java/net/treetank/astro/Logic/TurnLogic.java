package net.treetank.astro.Logic;

import net.treetank.astro.Rest.Callbacks.TurnCallback;
import net.treetank.astro.Rest.Models.Economy.Building;
import net.treetank.astro.Rest.Models.Economy.Resources;
import net.treetank.astro.Rest.Models.Economy.Summary;
import net.treetank.astro.Rest.Models.Economy.Turn;
import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;

import java.util.List;

public class TurnLogic {

    BuildingsLogic bLogic;
    BuyLandLogic blLogic;


    public TurnLogic() {
        bLogic = new BuildingsLogic();
        blLogic = new BuyLandLogic();
    }
    public boolean checkValidityOfEmployment() {
        for(Building b:GlobalData.getSummary().getBuildings()) {
            Building b2 = GlobalData.getCurrentTurn().getBuilding(b.getType());
            if(b.getEmployment()+b2.getEmployment()>b.getNumber()+b2.getNumber() || b.getEmployment()+b2.getEmployment()<0)
                return false;
        }
        return true;
    }
    public boolean checkValidityOfBuildings() {
        // mostly a precaution
        for(Building b:GlobalData.getSummary().getBuildings()) {
            Building b2 = GlobalData.getCurrentTurn().getBuilding(b.getType());
            if(b.getNumber()+b2.getNumber()<0)
                return false;
        }
        return true;
    }

    public boolean checkGoldConsumption() {
        return blLogic.generateConsumedGold(GlobalData.getCurrentTurn().getResources().getLand())<=GlobalData.getSummary().getResources().getGold();
    }

    private void turnPhase1() {
        // no longer blocking ui in turn logic
    }

    /** Checking whether all the variables are ok*/
    private boolean turnPhase2() {
        return bLogic.getAvailableBricks()>=0
                && bLogic.getAvailableLand()>=0
                && bLogic.getAvailableBCap()>=0
                && checkGoldConsumption()
                && checkValidityOfEmployment()
                && checkValidityOfBuildings();
    }



    public void saveChanges(Summary turn) {
        GlobalData.setSummary(turn);
        GlobalData.getCurrentTurn().clear();
        GlobalData.getCurrentTurn().setTurn(turn.getTurns());
    }


    public void executeTurn() {

        turnPhase1();
        if(turnPhase2()) {
            sendRequest();
        }
        else {
            new TurnCallback(this).boundsFailure();
        }
    }

    private void sendRequest() {
        new TurnCallback(this).call();
    }


}
