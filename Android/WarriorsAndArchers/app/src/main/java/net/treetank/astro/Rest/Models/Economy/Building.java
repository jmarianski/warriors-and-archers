package net.treetank.astro.Rest.Models.Economy;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import net.treetank.astro.R;
import net.treetank.astro.Utils.BuildingType;

public class Building {
    @SerializedName("Type")
    private String type;
    @SerializedName("Number")
    private int number;
    @SerializedName("Employment")
    private int employment;
    @SerializedName("Advancement")
    private float advancement;

    public Building(BuildingType type, int number, int employment, float advancement) {
        this.type = type.toString();
        this.number = number;
        this.employment = employment;
        this.advancement = advancement;
    }

    public BuildingType getType() {
        return BuildingType.fromString(this.type);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getEmployment() {
        return employment;
    }

    public void setEmployment(int employment) {
        this.employment = employment;
    }

    public float getAdvancement() {
        return advancement;
    }

    public void setAdvancement(float advancement) {
        this.advancement = advancement;
    }

    public int getBuildingDrawableId() {
        switch(getType()) {
            case HOUSE: return R.drawable.house;
            case FIELD: return R.drawable.field;
            case GOLDMINE: return R.drawable.mine;
            case BUILDERS_GUILD: return R.drawable.builders_guild;
            case MASONS_WORKSHOP: return R.drawable.icon_masons_workshop;
            case UNIVERSITY: return R.drawable.university;
            case ARMORY: return R.drawable.forge;
            case BARRACKS: return R.drawable.barracks;
            case ARCHERHOUSE: return R.drawable.archery;
            default: return R.drawable.no_image;
        }
    }
    public int getJobDrawableId() {
        switch(getType()) {
            case FIELD: return R.drawable.farmer;
            case GOLDMINE: return R.drawable.miner;
            case BUILDERS_GUILD: return R.drawable.builder;
            case MASONS_WORKSHOP: return R.drawable.mason;
            case UNIVERSITY: return R.drawable.scientist;
            case ARMORY: return R.drawable.smith;
            case BARRACKS: return R.drawable.warrior;
            case ARCHERHOUSE: return R.drawable.archer;
            default: return R.drawable.no_image;
        }
    }

    public int getNameId() {
        switch(getType()) {
            case HOUSE: return R.string.Houses;
            case FIELD: return R.string.Fields;
            case GOLDMINE: return R.string.GoldMines;
            case BUILDERS_GUILD: return R.string.BuildersGuilds;
            case MASONS_WORKSHOP: return R.string.MasonsWorkshops;
            case UNIVERSITY: return R.string.Universities;
            case ARMORY: return R.string.Armories;
            case BARRACKS: return R.string.Barracks;
            case ARCHERHOUSE: return R.string.ArcherHouses;
            case ERROR: return R.string.Houses;
            default: return R.string.Houses;
        }
    }

    public String getName(Context c) {
        return c.getResources().getString(getNameId());
    }

    public int getJobNameId() {
        switch(getType()) {
            case FIELD: return R.string.Farmers;
            case GOLDMINE: return R.string.MinersGold;
            case BUILDERS_GUILD: return R.string.Builders;
            case MASONS_WORKSHOP: return R.string.Masons;
            case UNIVERSITY: return R.string.Scientists;
            case ARMORY: return R.string.Armorers;
            case BARRACKS: return R.string.Warriors;
            case ARCHERHOUSE: return R.string.Archers;
            case ERROR: return R.string.Farmers;
            default: return R.string.Farmers;
        }
    }
    public int getJobDescription() {
        switch(getType()) {
            case FIELD: return R.string.Farmers_description;
            case GOLDMINE: return R.string.MinersGold_description;
            case BUILDERS_GUILD: return R.string.Builders_description;
            case MASONS_WORKSHOP: return R.string.Masons_description;
            case UNIVERSITY: return R.string.Scientists_description;
            case ARMORY: return R.string.Armorers_description;
            case BARRACKS: return R.string.Warriors_description;
            case ARCHERHOUSE: return R.string.Archers_description;
            case ERROR: return R.string.Farmers_description;
            default: return R.string.Farmers_description;
        }
    }
    public int getBuildingDescription() {
        switch(getType()) {
            case HOUSE: return R.string.Houses_description;
            case FIELD: return R.string.Fields_description;
            case GOLDMINE: return R.string.GoldMines_description;
            case BUILDERS_GUILD: return R.string.BuildersGuilds_description;
            case MASONS_WORKSHOP: return R.string.MasonsWorkshops_description;
            case UNIVERSITY: return R.string.Universities_description;
            case ARMORY: return R.string.Armories_description;
            case BARRACKS: return R.string.Barracks_description;
            case ARCHERHOUSE: return R.string.ArcherHouses_description;
            case ERROR: return R.string.Houses_description;
            default: return R.string.Houses_description;
        }
    }

    public String getJobName(Context c) {
        return c.getResources().getString(getJobNameId());
    }
}
