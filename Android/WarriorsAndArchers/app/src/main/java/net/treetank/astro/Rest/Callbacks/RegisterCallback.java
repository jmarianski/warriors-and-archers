package net.treetank.astro.Rest.Callbacks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import net.treetank.astro.Activities.SummaryActivity;
import net.treetank.astro.Rest.Models.Economy.Summary;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Economy.Turn;
import net.treetank.astro.Rest.Models.Login.Race;
import net.treetank.astro.Rest.Models.Login.Registration;
import net.treetank.astro.Rest.Models.Login.Solar_system;
import net.treetank.astro.Rest.RestClient;
import net.treetank.astro.Utils.GlobalData;

/**
 * Created by Jacek on 04.02.2017.
 */

public class RegisterCallback implements Callback<Summary> {

    String mEmail;

    @Override
    public void onResponse(Call<Summary> call, Response<Summary> response) {
        Activity a = GlobalData.getViewManager().activity;
        Summary summary = response.body();
        if(summary==null) {
            Toast.makeText(GlobalData.getViewManager().activity, R.string.Something_went_wrong, Toast.LENGTH_SHORT).show();
            //a.finish();
        }
        else if(!summary.getRecount().equals("true")) {
            String sharedPreferencesFile = a.getResources().getString(R.string.Shared_preference_file);
            SharedPreferences sharedPreferences = a.getApplicationContext().getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("email", mEmail).apply();
            editor.putString("kingdom", summary.getKingdomName()).apply();

            GlobalData.setSummary(summary);
            GlobalData.setCurrentTurn(new Turn(mEmail, summary.getKingdomId(), summary.getRecount(), summary.getTurns()));

            Intent intent = new Intent(GlobalData.getViewManager().activity, SummaryActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            a.startActivity(intent);
            a.finish();
        }
        else {
            Toast.makeText(GlobalData.getViewManager().activity, R.string.Something_went_wrong, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Call<Summary> call, Throwable t) {
        Toast.makeText(GlobalData.getViewManager().activity, R.string.Something_went_wrong, Toast.LENGTH_SHORT).show();
        //GlobalData.getViewManager().activity.finish();
    }

    public void call(String username, String auth, String kingdom, Solar_system selectedItem, Race item, String lang) {
        Registration registration = new Registration(username, auth, kingdom, selectedItem.id, item.id, lang);
        RestClient.get().registerUser(registration).enqueue(this);
    }
}
