package net.treetank.astro.Rest.Models.Social;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jacek on 06.02.2017.
 */

public class Message {

    @SerializedName("id")
    public int id;
    @SerializedName("idchannel")
    public int idchannel;
    @SerializedName("name")
    public String name;
    @SerializedName("time")
    public String time;
    @SerializedName("text")
    public String text;

}
