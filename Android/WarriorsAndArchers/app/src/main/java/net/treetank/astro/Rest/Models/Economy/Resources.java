package net.treetank.astro.Rest.Models.Economy;
import com.google.gson.annotations.SerializedName;
import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;

public class Resources {
    @SerializedName("Happiness")
    private int happiness;
    @SerializedName("Gold")
    private int gold;
    @SerializedName("Food")
    private int food;
    @SerializedName("Luxury")
    private int luxury;
    @SerializedName("Bricks")
    private int bricks;
    @SerializedName("Armaments")
    private int armaments;
    @SerializedName("People")
    private int people;
    @SerializedName("Land")
    private int land;
    @SerializedName("Science")
    private int science;
    @SerializedName("Bcap")
    private int bcap;

    public Resources(int happiness, int gold, int food, int luxury, int bricks, int armaments, int people, int land) {
        this.happiness = happiness;
        this.gold = gold;
        this.food = food;
        this.luxury = luxury;
        this.bricks = bricks;
        this.armaments = armaments;
        this.people = people;
        this.land = land;
    }

    public int getHappiness() {
        return happiness;
    }
    public int getPeople() {
        return people;
    }
    public int getLand() {
        return land;
    }

    public void setHappiness(int happiness) {
        this.happiness = happiness;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getFood() {
        return food;
    }

    public void setFood(int food) {
        this.food = food;
    }

    public int getLuxury() {
        return luxury;
    }

    public void setLuxury(int luxury) {
        this.luxury = luxury;
    }

    public int getBricks() {
        return bricks;
    }

    public void setBricks(int bricks) {
        this.bricks = bricks;
    }

    public int getScience() { return science;}

    public int getBuildingCapacity() { return bcap;}

    public int getArmaments() {
        return armaments;
    }

    public void setArmaments(int armament) {
        this.armaments = armament;
    }

    public void setPeople(int people) {
        this.people = people;
    }
    public void setLand(int land) {
        this.land = land;
    }

    public int get(String key) {
        switch(key) {
            case "Happiness": return happiness;
            case "Gold": return gold;
            case "Luxury": return luxury;
            case "Food": return food;
            case "Bricks": return bricks;
            case "Armaments": return armaments;
            case "People": return people;
            case "Land": return land;
            case "Science": return science;
            case "Bcap": return bcap;
            default: throw new IllegalArgumentException("Invalid index");
        }
    }

    public void addLand(int land) {this.land += land;}
    public void addHappiness(int happiness) {this.happiness += happiness;}
    public void addPeople(int people) {this.people += people;}
    public void addFood(int food) {this.food += food;}
    public void addGold(int gold) {this.gold += gold;}
    public void addBricks(int bricks) {this.bricks += bricks;}
    public void addArmaments(int armaments) {this.armaments += armaments;}
    public void addLuxury(int luxury) {this.luxury += luxury;}
    public void addScience(int science) {this.science += science;}
    public void addBcap(int bcap) {this.bcap += bcap;}
    public void add(String resource, int value) {
        switch(resource) {
            case "Happiness": happiness+=value; return;
            case "Gold":  gold+=value; return;
            case "Luxury":  luxury+=value; return;
            case "Food":  food+=value; return;
            case "Bricks":  bricks+=value; return;
            case "Armaments":  armaments+=value; return;
            case "People":  people+=value; return;
            case "Land":  land+=value; return;
            case "Science":  science+=value; return;
            case "Bcap":  bcap+=value; return;
            default:  throw new IllegalArgumentException("Invalid index");
        }
    }


    /**
     * Clears all resources. Useful when clearing turn object.
     */
    public void clear() {
        happiness = 0;
        gold = 0;
        luxury = 0;
        food = 0;
        bricks = 0;
        armaments = 0;
        people = 0;
        land = 0;
        science = 0;
        bcap = 0;
    }
}
