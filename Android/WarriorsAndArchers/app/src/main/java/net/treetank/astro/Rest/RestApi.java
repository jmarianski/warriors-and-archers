package net.treetank.astro.Rest;

import net.treetank.astro.Rest.Models.Diplomacy.DiplomaticRequest;
import net.treetank.astro.Rest.Models.Diplomacy.DiplomaticResponse;
import net.treetank.astro.Rest.Models.Login.LoginResult;
import net.treetank.astro.Rest.Models.Login.Registration;
import net.treetank.astro.Rest.Models.Social.ChatRequest;
import net.treetank.astro.Rest.Models.Social.ChatResponse;
import net.treetank.astro.Rest.Models.Login.Login;
import net.treetank.astro.Rest.Models.Economy.Summary;
import net.treetank.astro.Rest.Models.Economy.Turn;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RestApi {

    @POST("login")
    Call<LoginResult> loginUser(@Body Login login);

    @POST("registration")
    Call<Summary>  registerUser(@Body Registration registration);

    @POST("turn")
    Call<Summary> sendTurn(@Body Turn turn);

    @POST("msg")
    Call<ChatResponse> chatRequest(@Body ChatRequest request);

    @POST("diplomacy")
    Call<DiplomaticResponse> diplomaticRequest(@Body DiplomaticRequest r);
}