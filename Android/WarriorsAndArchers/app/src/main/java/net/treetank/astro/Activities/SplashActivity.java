package net.treetank.astro.Activities;

import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import net.treetank.astro.Rest.Callbacks.LoginCallback;
import net.treetank.astro.R;
import net.treetank.astro.Rest.RestClient;
import net.treetank.astro.Utils.GlobalData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashActivity extends WarriorsActivity {
    static final int REQUEST_CODE_PICK_ACCOUNT = 1000;
    static final int RC_SIGN_IN = 1001;
    private GoogleSignInClient mGoogleSignInClient;
    private LoginCallback loginCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.server_client_id))
                .build();
        Log.d("token", getString(R.string.server_client_id));
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        GoogleSignIn.getClient(getApplicationContext(), gso);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        setServer();
        loginUser();
        readHelpData();
    }

    private void readHelpData() {
        BufferedReader reader = null;
        String l1 = Locale.getDefault().getLanguage();
        try {
            String[] langs = getAssets().list("help");
            String l2 = "en";
            for(String l: langs)
                if(l==l1)
                    l2 = l;
            GlobalData.help.lang = l2;
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("help/"+l2+"/contents.txt")));

            // do reading, usually loop until end of file reading
            String mLine;
            GlobalData.help.count = Integer.parseInt(reader.readLine());
            while ((mLine = reader.readLine()) != null) {
                Log.d("astro", mLine);
                int page = Integer.parseInt(mLine.substring(0,mLine.indexOf(" ")));
                String code = mLine.substring(mLine.indexOf(" ")+1);
                GlobalData.help.pages.put(code, page);
            }
        } catch (NumberFormatException e) {
            // ignore line
        }catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
    }

    protected void setServer() {
        String sharedPreferencesFile = getResources().getString(R.string.Shared_preference_file);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE);
        String server = sharedPreferences.getString("server_url", RestClient.ROOT);
        RestClient.setupRestClient(server);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                SharedPreferences sp = getSharedPreferences(getString(R.string.Shared_preference_file), MODE_PRIVATE);
                loginUserOnServer(account.getEmail(), account.getIdToken(), sp.getInt("last_kingdom", 0));
            } catch (ApiException e) {
                // The ApiException status code indicates the detailed failure reason.
                // Please refer to the GoogleSignInStatusCodes class reference for more information.
                Log.w("Astro", "signInResult:failed code=" + e.getStatusCode());
                Toast.makeText(this, R.string.Login_required, Toast.LENGTH_SHORT).show();
            }
        } else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(this, R.string.Login_required, Toast.LENGTH_SHORT).show();
        }
    }
    @OnClick(R.id.parent)
    protected void backgroundClick() {
        if(loginCallback==null || !loginCallback.canCall)
            return;
        else
            loginCallback.call();
    }

    private void loginUser() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void loginUserOnServer(String email, String auth, int kingdomid) {
        (loginCallback = new LoginCallback(email, auth, kingdomid)).call();
    }
}
