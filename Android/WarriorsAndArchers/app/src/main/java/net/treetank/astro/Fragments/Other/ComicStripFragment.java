package net.treetank.astro.Fragments.Other;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.treetank.astro.R;

import java.util.ArrayList;

public class ComicStripFragment extends Fragment {

    public static int[] comic_strips = new int[] {R.drawable.comic_01, R.drawable.comic_02, R.drawable.comic_03, R.drawable.comic_04,
            R.drawable.comic_05, R.drawable.comic_06, R.drawable.comic_07, R.drawable.comic_08, R.drawable.comic_09, R.drawable.comic_10,
            R.drawable.comic_11, R.drawable.comic_12, R.drawable.comic_13};
    public static int[] comic_text = new int[] {R.string.comic_01, R.string.comic_02, R.string.comic_03, R.string.comic_04,
            R.string.comic_05, R.string.comic_06, R.string.comic_07, R.string.comic_08, R.string.comic_09, R.string.comic_10,
            R.string.comic_11, R.string.comic_12, R.string.comic_13};
    private int my_number = 0;

    public ComicStripFragment() {
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        my_number = args.getInt("number");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_comic_strip, container, false);
        int strip = comic_strips[my_number];
        String text = getResources().getString(comic_text[my_number]);
        ((ImageView)(v.findViewById(R.id.image))).setImageResource(strip);
        ((TextView)(v.findViewById(R.id.text))).setText(text);
        if(text.length()<1)
            ((v.findViewById(R.id.text_holder))).setVisibility(View.GONE);
        else
            ((v.findViewById(R.id.text_holder))).setVisibility(View.VISIBLE);
        return v;
    }



}
