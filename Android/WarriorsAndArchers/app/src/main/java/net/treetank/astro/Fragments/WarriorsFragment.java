package net.treetank.astro.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.EditText;

import net.treetank.astro.Activities.SummaryActivity;
import net.treetank.astro.R;
import net.treetank.astro.Utils.GlobalData;

import butterknife.Bind;
import butterknife.OnTextChanged;

/**
 * Created by Jacek on 29.01.2017.
 */

public abstract class WarriorsFragment extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalData.getViewManager().fragment = this;
    }


    SharedPreferences getSharedPreferences() {
        return  getContext().getSharedPreferences(getString(R.string.Shared_preference_file), Context.MODE_PRIVATE);
    }

    public boolean onBackPressed() {
        return false;
    }

    public abstract void updateUI();
}
