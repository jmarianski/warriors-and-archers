package net.treetank.astro.Rest.Models;

import android.widget.Toast;

import net.treetank.astro.Fragments.DiplomacyFragment;
import net.treetank.astro.Fragments.Other.DiplomacyListAdapter;
import net.treetank.astro.Fragments.SingleEntities.Pager;
import net.treetank.astro.Logic.DiplomacyLogic;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Callbacks.DiplomacyCallback;
import net.treetank.astro.Rest.Models.Diplomacy.Battle;
import net.treetank.astro.Rest.Models.Diplomacy.Kingdom;
import net.treetank.astro.Rest.Models.Diplomacy.Soldiers;
import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;

import java.util.List;

/**
 * Created by Jacek on 19.03.2017.
 */

public class DiplomacyRequestManager {

    public DiplomacyListAdapter allPlayers;
    public DiplomacyCallback callback = new DiplomacyCallback();
    public DiplomacyLogic logic = new DiplomacyLogic();
    public List<Battle> battles;
    public Battle battle;
    public Pager current;
    public List<Kingdom> players;
    public String solar_system;

    public boolean canShowBattles() {
        return battles!=null;
    }

    public void loadBattles() {
        callback.getBattles();
    }

    public void battlesLoaded() {

        if(GlobalData.getViewManager().fragment instanceof DiplomacyFragment) {
            DiplomacyFragment f = (DiplomacyFragment) GlobalData.getViewManager().fragment;
            f.hideLoading();
            f.showBattles();
        }
    }

    public void playersLoaded() {
        if(GlobalData.getViewManager().fragment instanceof DiplomacyFragment) {
            DiplomacyFragment f = (DiplomacyFragment) GlobalData.getViewManager().fragment;
            f.hideLoading();
            GlobalData.diplomacy.allPlayers.notifyDataSetChanged();

        }
    }

    public void loadPlayers() {
        callback.getPlayers();
    }

    public void kingdomClicked(Kingdom o) {
        if(GlobalData.getViewManager().fragment instanceof DiplomacyFragment) {
            DiplomacyFragment f = (DiplomacyFragment) GlobalData.getViewManager().fragment;
            f.playerClicked(o);
            f.getChildFragmentManager().popBackStack();
            battle.compareWithSaved();
        }
    }

    public int availableSoldiers(int type) {
        int all = logic.getAllSoldiers(type);
        for(Battle b:battles) {
            for(Soldiers s:b.soldiers)
                if(Battle.getTypes().indexOf(BuildingType.fromString(s.building))==type)
                    all -=s.number;
        }
        return all;
    }

    public void removeBattle(int hash) {
        GlobalData.diplomacy.current.removeItem(hash);
        Toast.makeText(GlobalData.getViewManager().activity, GlobalData.getViewManager().activity.getString(R.string.Battle_deleted), Toast.LENGTH_SHORT).show();
    }

    public void battleSaved() {
        Toast.makeText(GlobalData.getViewManager().activity, GlobalData.getViewManager().activity.getString(R.string.Battle_saved), Toast.LENGTH_SHORT).show();
    }

    public void clearData() {
        battles = null;

    }
}
