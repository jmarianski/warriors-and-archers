package net.treetank.astro.Rest.Models.Diplomacy;

import com.google.gson.annotations.SerializedName;
import net.treetank.astro.Utils.GlobalData;

/**
 * Created by Jacek on 05.03.2017.
 */

public class DiplomaticRequest {

    @SerializedName("kingdom_id")
    private int kingdom_id;
    @SerializedName("requestType")
    private int requestType;
    @SerializedName("Recount")
    private String Recount;
    @SerializedName("battle")
    private Battle battle;
    public DiplomaticRequest(int requestType, Battle b) {
        this.requestType = requestType;
        this.battle = b;
        this.Recount = GlobalData.getSummary().getRecount();
        this.kingdom_id = GlobalData.getSummary().getKingdomId();
    }

}
