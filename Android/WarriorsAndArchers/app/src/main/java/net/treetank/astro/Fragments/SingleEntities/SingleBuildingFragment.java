package net.treetank.astro.Fragments.SingleEntities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.treetank.astro.Logic.BuildingsLogic;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Economy.Building;
import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SingleBuildingFragment extends SingleEntity {

    Building building, buildingChanges;
    BuildingsLogic logic = new BuildingsLogic();
    @Bind(R.id.name)
    TextView name;

    @Bind(R.id.value)
    TextView value;

    @Bind(R.id.image)
    ImageView image;

    @Bind(R.id.you_can_build)
    TextView you_can_build;

    @Bind(R.id.change)
    TextView change;

    @Bind(R.id.brief_description)
    TextView desc;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.single_building, container, false);
        ButterKnife.bind(this, rootView);
        Bundle args = getArguments();
        BuildingType b = BuildingType.fromString(args.getString("building"));
        if(b!=BuildingType.ERROR) {
            building = GlobalData.getSummary().getBuilding(b);
            buildingChanges = GlobalData.getCurrentTurn().getBuilding(b);
            name.setText(building.getName(this.getContext()));
            value.setText(String.format("%d", building.getNumber()));
            image.setImageDrawable(getContext().getResources().getDrawable(building.getBuildingDrawableId()));
            change.setText(String.format("%d", buildingChanges.getNumber()));
            desc.setText(building.getBuildingDescription());
        }
        updateYouCanBuild();
        setBackgroundOnClick(rootView);
        return rootView;
    }

    /**
     * Sets actions for clicking background
     * @param v
     */
    private void setBackgroundOnClick(View v) {
        v.findViewById(R.id.background).setOnClickListener(new View.OnClickListener() {
           public void onClick(View view) {getParentFragment().getChildFragmentManager().popBackStack();}
        });
    }

    @OnClick(R.id.youcanbuild_text)
    public void youcanbuild_text_click() {
        String text = logic.buildableReason(building.getType());
        Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
    }
    @OnClick(R.id.plus)
    public void plus() {
        int changeValue = Integer.parseInt(change_value2.getText().toString());
        changeValue = logic.changeAmountOfBuildings(building.getType(), changeValue);
        updateChangeTextView(changeValue);
    }
    @OnClick(R.id.minus)
    public void minus() {
        int changeValue = Integer.parseInt(change_value2.getText().toString());
        changeValue = logic.changeAmountOfBuildings(building.getType(), -changeValue);
        updateChangeTextView(changeValue);
    }

    private void updateChangeTextView(int changeValue) {
        change.setText(String.format("%d", changeValue));
        TextView currentChangeTextView = getTextViewToUpdate();
        GlobalData.getViewManager().fragment.updateUI();
        LinearLayout tl = getTable();
        if(tl==null)
            return;
        if(currentChangeTextView!=null)
            currentChangeTextView.setText(String.format("%d", changeValue));
        updateYouCanBuild();
        tl.requestLayout();
        tl.invalidate();
    }

    private void updateYouCanBuild() {
        int value = logic.canBuildAmount(building.getType());
        if(getActivity()!=null) {
            TextView you_can_build = ((TextView) (getActivity().findViewById(R.id.you_can_build_summary)));
            if(you_can_build!=null)
                you_can_build.setText(String.format("%d", value));
            this.you_can_build.setText(String.format("%d", value));
        }
        // TODO
    }

    TextView getTextViewToUpdate() {
        switch(building.getType()) {
            case HOUSE: return (TextView) getActivity().findViewById(R.id.Houses_change);
            case FIELD: return (TextView) getActivity().findViewById(R.id.Fields_change);
            case GOLDMINE: return (TextView) getActivity().findViewById(R.id.GoldMines_change);
            case BUILDERS_GUILD: return (TextView) getActivity().findViewById(R.id.BuildersGuilds_change);
            case MASONS_WORKSHOP: return (TextView) getActivity().findViewById(R.id.MasonsWorkshops_change);
            case UNIVERSITY: return (TextView) getActivity().findViewById(R.id.Universities_change);
            case ARMORY: return (TextView) getActivity().findViewById(R.id.Armories_change);
            case BARRACKS: return (TextView) getActivity().findViewById(R.id.Barracks_change);
            case ARCHERHOUSE: return (TextView) getActivity().findViewById(R.id.ArcherHouses_change);
            case ERROR: return null;
            default: return null;

        }
    }

    LinearLayout getTable() {
        return (LinearLayout) getActivity().findViewById(R.id.all_the_stuff);
    }

    @Override
    public void updateUI() {
        updateYouCanBuild();
    }
}
