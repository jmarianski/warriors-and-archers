package net.treetank.astro.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import net.treetank.astro.R;
import net.treetank.astro.Rest.RestApi;
import net.treetank.astro.Rest.RestClient;
import net.treetank.astro.Utils.GlobalData;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.OnTouch;


public class SettingsFragment extends WarriorsFragment {

    @Bind(R.id.ResetTutorial)
    public Button tutorials;
    @Bind(R.id.server)
    public EditText server;
    @Bind(R.id.textSizeText)
    public TextView textSizeText;
    @Bind(R.id.musicText)
    public TextView musicText;
    @Bind(R.id.soundText)
    public TextView soundText;
    @Bind(R.id.textSizeSeekBar)
    public SeekBar textSeekBar;
    @Bind(R.id.MusicVolumeBar)
    public SeekBar musicSeekBar;
    @Bind(R.id.SoundVolumeBar)
    public SeekBar SoundSeekBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);
        SharedPreferences sp = getSharedPreferences();
        if(sp.contains("tutorials"))
            tutorials.setVisibility(View.VISIBLE);
        else
            tutorials.setVisibility(View.GONE);
        updateTextViews();
        updateSeekBars();
        return view;
    }

    public void updateTextViews() {
        server.setText(getSharedPreferences().getString("server_url", RestClient.ROOT));
        textSizeText.setText(String.valueOf(getSharedPreferences().getInt("settings_text_size", 11)));
        musicText.setText(String.valueOf(getSharedPreferences().getInt("settings_music_volume", 50)));
        soundText.setText(String.valueOf(getSharedPreferences().getInt("settings_sound_volume", 50)));
    }
    public void updateSeekBars() {
        textSeekBar.setProgress(getSharedPreferences().getInt("settings_text_size", 11)-9);
        musicSeekBar.setProgress(getSharedPreferences().getInt("settings_music_volume", 50));
        SoundSeekBar.setProgress(getSharedPreferences().getInt("settings_sound_volume", 50));

    }

    @OnTouch({R.id.SoundVolumeBar,
            R.id.MusicVolumeBar,
            R.id.textSizeSeekBar})

    public boolean onTouch(View v, MotionEvent event) {
        if(event.getAction()!=MotionEvent.ACTION_UP)
            return false;
        SeekBar seek = (SeekBar) v;
        int val = 100 * seek.getProgress() / seek.getMax();
        String var = "";
        switch (v.getId()) {
            case R.id.textSizeSeekBar:
                var = "settings_text_size";
                val = 9 + seek.getProgress();
                break;
            case R.id.MusicVolumeBar:
                var = "settings_music_volume";
                break;
            case R.id.SoundVolumeBar:
                var = "settings_sound_volume";
                break;
        }
        getSharedPreferences().edit().putInt(var, val).apply();
        if (v.getId() == R.id.MusicVolumeBar) {
            GlobalData.player.updateVolumeAndPlay();
        }
        if (v.getId() == R.id.SoundVolumeBar) {
            GlobalData.player.updateSoundVolume();
        }
        if (v.getId() == R.id.textSizeSeekBar) {
            GlobalData.updateTheme(getContext());
        }
        updateTextViews();
        return false;
    }

    @OnTextChanged(R.id.server)
    public void onServerUpdate() {
        String text = String.valueOf(server.getText());
        getSharedPreferences().edit().putString("server_url", text).apply();
    }

    @OnClick({ R.id.ResetTutorial})
    public void resetTutorial() {
        SharedPreferences sp = getSharedPreferences();
        sp.edit().remove("tutorials").apply();
        tutorials.setVisibility(View.GONE);
    }

    public SharedPreferences getSharedPreferences() {
        String sharedPreferencesFile = getResources().getString(R.string.Shared_preference_file);
        return getContext().getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE);
    }

    @Override
    public void updateUI() {

    }
}
