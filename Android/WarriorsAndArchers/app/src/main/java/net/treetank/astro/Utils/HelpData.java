package net.treetank.astro.Utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jack on 15.04.2018.
 */

public class HelpData {
    public int count = 0;
    public String lang = "en";
    public Map<String, Integer> pages; // maps code string to page number

    HelpData() {
        pages = new HashMap<>();
    }

}
