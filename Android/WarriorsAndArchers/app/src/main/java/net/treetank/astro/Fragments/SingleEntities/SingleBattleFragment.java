package net.treetank.astro.Fragments.SingleEntities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.treetank.astro.Fragments.DiplomacyFragment;
import net.treetank.astro.Logic.DiplomacyLogic;
import net.treetank.astro.Logic.EmploymentLogic;
import net.treetank.astro.R;
import net.treetank.astro.Rest.Models.Diplomacy.Battle;
import net.treetank.astro.Rest.Models.Diplomacy.Kingdom;
import net.treetank.astro.Rest.Models.Diplomacy.Soldiers;
import net.treetank.astro.Rest.Models.Economy.Building;
import net.treetank.astro.Utils.BuildingType;
import net.treetank.astro.Utils.GlobalData;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SingleBattleFragment extends SingleEntity{


    @Bind(R.id.topPanel)
    public RelativeLayout top;
    @Bind(R.id.target)
    public TextView target;
    @Bind({R.id.amt_0, R.id.amt_1})
    public List<TextView> amts;
    @Bind({R.id.av_0, R.id.av_1})
    public List<TextView> avs;
    @Bind({ R.id.minus_0,
            R.id.minus_1 })
    public List<Button> minuses;
    @Bind({ R.id.plus_0,
            R.id.plus_1 })
    public List<Button> pluses;
    @Bind(R.id.saved)
    public RelativeLayout saved;
    @Bind(R.id.text_saved)
    public TextView text_saved;
    private int b;
    private View root;
    private Battle battle;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.single_attack, container, false);
        setRetainInstance(false);
        ButterKnife.bind(this, rootView);
        Bundle args = getArguments();
        b = args.getInt("battle");
        if(b<GlobalData.diplomacy.battles.size()) {
            battle = GlobalData.diplomacy.battles.get(b);
            fillLayout(battle);
        }
        top.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {clickTop();}
        });
        setBackgroundOnClick(rootView);
        root = rootView;
        updateUI();
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void fillLayout(Battle battle) {
        update_text_saved();
        String name = null;
        for(Kingdom k:GlobalData.diplomacy.players)
            if(k.id==battle.idto)
                name = k.name;
        if(name!=null)
            target.setText(String.format(getString(R.string.Battle_target), name));
        else
            target.setText(R.string.Battle_no_target);
        for(Soldiers s: battle.soldiers) {
            int index = Battle.getTypes().indexOf(BuildingType.fromString(s.building));
            if(index<0)
                continue;
            amts.get(index).setText(String.valueOf(s.number));
            avs.get(index).setText(String.valueOf(GlobalData.diplomacy.availableSoldiers(index)));
        }
    }

    private void update_text_saved() {
        if(battle.saved)
            saved.setVisibility(View.GONE);
        else
            saved.setVisibility(View.VISIBLE);
        if(battle.id>0)
            text_saved.setText(R.string.Battle_unsaved);
        else
            text_saved.setText(R.string.Battle_unadded);
        if(battle.saved && !battle.isSendable()) {
            text_saved.setText(R.string.Battle_unsent);
            saved.setVisibility(View.VISIBLE);
        }
    }

    public void clickTop() {
        Fragment f = GlobalData.getViewManager().fragment;
        GlobalData.diplomacy.battle = battle;
        if(f instanceof DiplomacyFragment) {
            DiplomacyFragment df = (DiplomacyFragment) f;
            df.clickPlayers();
        }
    }

    @OnClick({ R.id.minus_0,
            R.id.minus_1 })
    public void onClickMinus(Button button) {
        // use list minuses to get type (index), with index add value to battles soldier
        int type = minuses.indexOf(button);
        int change = Integer.parseInt(this.change_value2.getText().toString());
        int current = battle.minus(type, change);
        updateCurrent(type, current);
        updateAvailable(type);
        battle.compareWithSaved();
    }

    @OnClick({ R.id.plus_0,
            R.id.plus_1 })
    public void onClickPlus(Button button) {
        int type = pluses.indexOf(button);
        int change = Integer.parseInt(this.change_value2.getText().toString());
        int current = battle.plus(type, change);
        updateCurrent(type, current);
        updateAvailable(type);
        battle.compareWithSaved();

    }

    @OnClick({ R.id.save })
    public void onClickSave(Button button) {
        if(battle.id>0) {
            GlobalData.diplomacy.callback.editBattle(battle);
        } else {
            battle.hash = battle.hashCode();
            GlobalData.diplomacy.callback.addBattle(battle);
        }
        battle.saving = true;
        text_saved.setText(R.string.Battle_saving);
    }
    @OnClick({ R.id.cancel })
    public void onClickCancel(Button button) {
        if(battle.saving)
            return;
        battle.hash = battle.hashCode();
        if(!battle.saved) {
            battle.restoreBackup();
        }
         else if(battle.id>0) {
            GlobalData.diplomacy.callback.deleteBattle(battle);
        } else {
            GlobalData.diplomacy.removeBattle(battle.hash);
        }
    }

    public void updateAvailable(int type) {
        int av = GlobalData.diplomacy.availableSoldiers(type);
        avs.get(type).setText(String.valueOf(av));
    }

    public void updateCurrent(int type, int value) {
        amts.get(type).setText(String.valueOf(value));
    }



    public void updateUI() {
        if(getActivity()!=null) {
            fillLayout(battle);
        }
    }

    /**
     * Sets actions for clicking background
     * @param v
     */
    private void setBackgroundOnClick(View v) {
        v.findViewById(R.id.background).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {getParentFragment().getChildFragmentManager().popBackStack();}
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
