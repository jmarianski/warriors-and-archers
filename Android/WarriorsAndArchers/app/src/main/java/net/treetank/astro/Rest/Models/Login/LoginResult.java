package net.treetank.astro.Rest.Models.Login;

import com.google.gson.annotations.SerializedName;

import net.treetank.astro.Rest.Models.Economy.Summary;

import java.util.List;

/**
 * Created by Jack on 04.02.2018.
 */

public class LoginResult {
    public Summary summary;
    @SerializedName("solar_systems")
    public List<Solar_system> solar_systems;
    public List<Race> races;
    public boolean Recount;
}
