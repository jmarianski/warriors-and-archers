-- MySQL dump 10.13  Distrib 5.7.14, for Win64 (x86_64)
--
-- Host: 192.168.1.7    Database: warriors
-- ------------------------------------------------------
-- Server version	5.5.57-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `battles`
--

DROP TABLE IF EXISTS `battles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `battles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idkingdom` int(11) NOT NULL,
  `idfrom` int(11) NOT NULL,
  `idto` int(11) NOT NULL,
  `sent` tinyint(1) NOT NULL,
  `function` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `battles`
--

LOCK TABLES `battles` WRITE;
/*!40000 ALTER TABLE `battles` DISABLE KEYS */;
INSERT INTO `battles` VALUES (16,16,16,16,0,''),(18,16,16,16,0,'');
/*!40000 ALTER TABLE `battles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buildings`
--

DROP TABLE IF EXISTS `buildings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buildings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idkingdom` int(11) NOT NULL,
  `idtype` int(11) NOT NULL,
  `number` int(11) NOT NULL DEFAULT '0',
  `employment` int(11) NOT NULL DEFAULT '0',
  `advancement` float NOT NULL DEFAULT '100',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buildings`
--

LOCK TABLES `buildings` WRITE;
/*!40000 ALTER TABLE `buildings` DISABLE KEYS */;
INSERT INTO `buildings` VALUES (66,16,1,90,0,1),(67,16,2,50,40,1.275),(68,16,3,50,38,1.28947),(69,16,4,40,10,2.1),(70,16,5,40,10,2.1),(71,16,6,40,10,1),(72,16,8,20,20,1.55),(73,16,9,50,40,1.275),(74,16,10,50,19,1.57895);
/*!40000 ALTER TABLE `buildings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buildings_cache`
--

DROP TABLE IF EXISTS `buildings_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buildings_cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idkingdom` int(11) NOT NULL,
  `idtype` int(11) NOT NULL,
  `number` int(11) NOT NULL DEFAULT '0',
  `employment` int(11) NOT NULL DEFAULT '0',
  `advancement` float NOT NULL DEFAULT '100',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buildings_cache`
--

LOCK TABLES `buildings_cache` WRITE;
/*!40000 ALTER TABLE `buildings_cache` DISABLE KEYS */;
INSERT INTO `buildings_cache` VALUES (1,16,1,0,0,0),(2,16,2,0,0,0),(3,16,3,0,0,-0.00000631809),(4,16,4,0,0,0),(5,16,5,0,0,0),(6,16,6,0,0,0),(7,16,8,0,0,0),(8,16,9,0,0,0),(9,16,10,0,0,-0.0000026226);
/*!40000 ALTER TABLE `buildings_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buildingtypes`
--

DROP TABLE IF EXISTS `buildingtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buildingtypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `race` int(2) NOT NULL DEFAULT '0',
  `name` text NOT NULL,
  `number` int(11) NOT NULL,
  `employment` int(11) NOT NULL,
  `advancement` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buildingtypes`
--

LOCK TABLES `buildingtypes` WRITE;
/*!40000 ALTER TABLE `buildingtypes` DISABLE KEYS */;
INSERT INTO `buildingtypes` VALUES (1,1,'House',50,0,0.7),(2,1,'Field',30,30,0.7),(3,1,'Goldmine',10,10,0.7),(4,1,'MasonsWorkshop',10,10,0.7),(5,1,'BuildersGuild',10,10,0.7),(6,1,'University',10,10,0.7),(8,1,'Armory',0,0,0.7),(9,1,'Barracks',0,0,1),(10,1,'ArcherHouse',0,0,1);
/*!40000 ALTER TABLE `buildingtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_channels`
--

DROP TABLE IF EXISTS `chat_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat_channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channelname` varchar(30) NOT NULL,
  `channeldesc` text NOT NULL,
  `permissions` int(3) NOT NULL,
  `owner` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_channels`
--

LOCK TABLES `chat_channels` WRITE;
/*!40000 ALTER TABLE `chat_channels` DISABLE KEYS */;
INSERT INTO `chat_channels` VALUES (1,'Test','Pierwszy kanał joł',777,-1,'2017-11-05 00:25:11'),(2,'Test','Drugi kanał',777,-1,'2017-02-19 01:21:40'),(3,'Test3','Trzeci kanał',777,-1,'2018-01-12 15:42:51'),(4,'Test4','Czwarty',777,-1,'2017-02-12 21:33:25');
/*!40000 ALTER TABLE `chat_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_group`
--

DROP TABLE IF EXISTS `chat_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subgroup` int(11) NOT NULL,
  `channelid` int(11) NOT NULL,
  `permission` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_group`
--

LOCK TABLES `chat_group` WRITE;
/*!40000 ALTER TABLE `chat_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `chat_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_member`
--

DROP TABLE IF EXISTS `chat_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat_member` (
  `id` int(11) NOT NULL,
  `groupid` int(11) NOT NULL,
  `kingdomid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_member`
--

LOCK TABLES `chat_member` WRITE;
/*!40000 ALTER TABLE `chat_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `chat_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_messages`
--

DROP TABLE IF EXISTS `chat_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idchannel` int(11) NOT NULL DEFAULT '-1',
  `idkingdom` int(11) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_messages`
--

LOCK TABLES `chat_messages` WRITE;
/*!40000 ALTER TABLE `chat_messages` DISABLE KEYS */;
INSERT INTO `chat_messages` VALUES (1,'2017-02-05 23:37:35',1,1,'test'),(2,'2017-02-05 23:38:54',1,1,'test'),(3,'2017-02-05 23:39:10',1,1,'test'),(4,'2017-02-05 23:39:45',1,1,'test'),(5,'2017-02-05 23:41:34',1,1,'test'),(6,'2017-02-05 23:45:55',1,1,'test'),(7,'2017-02-05 23:46:48',1,1,'test'),(8,'2017-02-05 23:48:02',1,1,'test'),(9,'2017-02-05 23:58:48',1,1,'test'),(10,'2017-02-11 01:26:38',1,14,'test'),(11,'2017-02-12 01:20:30',1,14,'test'),(12,'2017-02-12 20:30:57',1,14,'testtt'),(13,'2017-02-12 20:33:25',1,14,'testttttt'),(14,'2017-02-12 21:05:15',1,14,'ttt'),(15,'2017-02-12 21:06:30',1,14,'tttjhmhg'),(16,'2017-02-12 21:45:32',1,0,'test'),(17,'2017-02-12 22:05:03',1,14,'test2'),(18,'2017-02-12 22:17:31',1,14,'tessdvcd'),(19,'2017-02-12 22:19:19',1,14,'hgdxjjxbn'),(20,'2017-02-12 22:19:40',2,14,'fhhfhi'),(21,'2017-02-12 22:39:34',2,14,'tesy'),(22,'2017-02-19 00:21:40',2,14,'jgfshjd'),(23,'2017-02-25 19:27:25',1,14,'test'),(24,'2017-03-16 02:01:23',1,16,'test'),(25,'2017-03-16 20:56:02',1,16,'gdhfdhj'),(26,'2017-03-30 20:09:35',1,16,'aaaaa'),(27,'2017-11-04 23:25:11',1,16,'Å¼Ã³Å‚Ä‡'),(28,'2018-01-12 14:42:17',3,16,'test'),(29,'2018-01-12 14:42:51',3,16,'Å¼Ã³Å‚Ä‡ Äeska');
/*!40000 ALTER TABLE `chat_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kingdoms`
--

DROP TABLE IF EXISTS `kingdoms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kingdoms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `name` text NOT NULL,
  `land` int(11) NOT NULL DEFAULT '1000',
  `people` int(11) NOT NULL DEFAULT '100',
  `turn` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kingdoms`
--

LOCK TABLES `kingdoms` WRITE;
/*!40000 ALTER TABLE `kingdoms` DISABLE KEYS */;
INSERT INTO `kingdoms` VALUES (16,27,'jack',1000,100,5);
/*!40000 ALTER TABLE `kingdoms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `races`
--

DROP TABLE IF EXISTS `races`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `races` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `races`
--

LOCK TABLES `races` WRITE;
/*!40000 ALTER TABLE `races` DISABLE KEYS */;
INSERT INTO `races` VALUES (1,'human');
/*!40000 ALTER TABLE `races` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idkingdom` int(11) NOT NULL,
  `idresource` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources`
--

LOCK TABLES `resources` WRITE;
/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
INSERT INTO `resources` VALUES (73,16,1,98),(74,16,2,2575),(75,16,3,2724),(76,16,4,0),(77,16,5,1222),(78,16,6,425),(79,16,7,1000),(80,16,8,230);
/*!40000 ALTER TABLE `resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources_cache`
--

DROP TABLE IF EXISTS `resources_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources_cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idkingdom` int(11) NOT NULL,
  `idresource` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources_cache`
--

LOCK TABLES `resources_cache` WRITE;
/*!40000 ALTER TABLE `resources_cache` DISABLE KEYS */;
INSERT INTO `resources_cache` VALUES (73,16,1,0),(74,16,2,98),(75,16,3,28),(76,16,4,0),(77,16,5,62),(78,16,6,31),(79,16,7,0),(80,16,8,3);
/*!40000 ALTER TABLE `resources_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resourcetypes`
--

DROP TABLE IF EXISTS `resourcetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resourcetypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `race` int(11) NOT NULL DEFAULT '1',
  `defaults` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resourcetypes`
--

LOCK TABLES `resourcetypes` WRITE;
/*!40000 ALTER TABLE `resourcetypes` DISABLE KEYS */;
INSERT INTO `resourcetypes` VALUES (1,'Happiness',1,75),(2,'Gold',1,1000),(3,'Food',1,1000),(4,'Luxury',1,0),(5,'Bricks',1,100),(6,'Armament',1,0),(7,'Land',1,1000),(8,'People',1,100);
/*!40000 ALTER TABLE `resourcetypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `key1` varchar(30) NOT NULL,
  `val` text NOT NULL,
  PRIMARY KEY (`key1`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES ('day','294'),('recount.in_progress','yes'),('recount.time','2018-01-11 17:01:31');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soldiers`
--

DROP TABLE IF EXISTS `soldiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soldiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idbattle` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `advancement` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soldiers`
--

LOCK TABLES `soldiers` WRITE;
/*!40000 ALTER TABLE `soldiers` DISABLE KEYS */;
INSERT INTO `soldiers` VALUES (71,16,0,0,0),(72,16,1,19,0),(75,18,0,0,0),(76,18,1,0,0);
/*!40000 ALTER TABLE `soldiers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `hash` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (27,'userjack','marianski.jacek@gmail.com','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-12 17:01:46
