<?php
$app = app();
?>
<style>
    @font-face {
        font-family: 'FixedsysExcelsior301Regular';
        src: url('<?=$app["url"]("/web/assets/fonts/")?>fsex300-webfont.eot');
        src: url('<?=$app["url"]("/web/assets/fonts/")?>fsex300-webfont.eot?#iefix') format('embedded-opentype'),
        url('<?=$app["url"]("/web/assets/fonts/")?>fsex300-webfont.woff') format('woff'),
        url('<?=$app["url"]("/web/assets/fonts/")?>fsex300-webfont.ttf') format('truetype'),
        url('<?=$app["url"]("/web/assets/fonts/")?>fsex300-webfont.svg#FixedsysExcelsior301Regular') format('svg');
        font-weight: normal;
        font-style: normal;
        -webkit-font-smoothing: none;
        -moz-osx-font-smoothing: auto;
        font-smooth:none;
    }
    html {
    }
    body {
        background-color:black;
        color:white;
        font-size: 16px;
        font-size: 3vh;
        font-family:monospace;
        line-height: inherit;
    }
    .go {
        width:100px;
        height:100px;
        background: transparent;
        border:0;
        position: fixed;
        top:50%;
        -webkit-transform: translateY(-50%);
        transform: translateY(-50%);
    }
    .go div {
        position:absolute;
        top:25px;
        width:50px;
        height:50px;
        border-bottom:1px solid white;
        border-right:1px solid white;
    }
    .left {
        left:0%;
    }
    .right {
        right:0%;
    }
    .left div {
        left:25px;
        -webkit-transform: rotate(135deg);
        -moz-transform: rotate(135deg);
        -ms-transform: rotate(135deg);
        -o-transform: rotate(135deg);
        transform: rotate(135deg);
    }
     .right div {
         right:25px;
              -webkit-transform: rotate(-45deg);
              -moz-transform: rotate(-45deg);
              -ms-transform: rotate(-45deg);
              -o-transform: rotate(-45deg);
              transform: rotate(-45deg);
    }
    .go:hover div {
        border-bottom:3px solid white;
        border-right:3px solid white;
    }
    .story_time {
        position:absolute;
        width:100%;
        height:100%;
        top:0;
        left:0;
        overflow: hidden;
    }
    .children {
        position:relative;
        height:100%;
        top:0;
        left:0;
        transition:all 0.7s ease-in-out;
    }
    .image_holder {
        position:absolute;
        width:100%;
        height:100%;
        top:0;
    }
    .image_being_hold {
        position:absolute;
        height:100%;
        top:0;
        left:50%;
        -webkit-transform: translateX(-50%);
        -moz-transform: translateX(-50%);
        -ms-transform: translateX(-50%);
        -o-transform: translateX(-50%);
        transform: translateX(-50%);
    }
    .border {
        position:absolute;
        width:60vh;
        height:100vh;
        border:1px solid gray;
        left:50%;
        -webkit-transform: translateX(-50%);
        -moz-transform: translateX(-50%);
        -ms-transform: translateX(-50%);
        -o-transform: translateX(-50%);
        transform: translateX(-50%);
    }
    .text {
        position: absolute;
        text-align:center;
        background: rgba(0,0,0,0.5);
        bottom:5%;
        left:50%;
        padding:10px;
        width:50vh;
        -webkit-transform: translateX(-50%);
        -moz-transform: translateX(-50%);
        -ms-transform: translateX(-50%);
        -o-transform: translateX(-50%);
        transform: translateX(-50%);
    }
    .text button {
        font-size: 16px;
        font-size: 3vh;
        font-family:monospace;

    }
    input.email {
        border-radius:3px;
        border:1px solid black;
        padding:5px;
    }
    a img {
        width:4vh;
        height:4vh;
    }
</style>
<div class="story_time">
    <div class="children">
        <div class="image_holder" id="lastone" style="text-align: center;">
            <div class="border"></div>
            <div class="text" style="top:50%; bottom:auto; -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); -ms-transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); transform: translate(-50%, -50%);">
                Would you like to register to mailing list?<BR>
                <form action="<?=$app["url"]("/email/subscribe")?>" method="post"><input type="email" class="email" name="email" placeholder="example@email.com"><BR>
                <button class="btn btn-primary">Submit</button></form>
                Visit our blog:<BR>
                <a href="<?=$app["url"]("/blog/astro")?>">Click here</a><BR>
                <a href="https://www.patreon.com/treetank"><img src="<?=$app["url"]("/web/assets/images/pateron.png")?>"></a>
                <a href="https://twitter.com/treetanknet"><img src="<?=$app["url"]("/web/assets/images/twitter.png")?>"></a>
            </div>
        </div>
    </div>
</div>
<button class="go left"><div/></button>
<button class="go right"><div/></button>
<script>
    $(document).ready(function(){
        var current = 0;
        var imgs = [];
        var comics = <?=json_encode($data["comic"])?>;
        var index = -1;
        for(var i=0; i<comics.length; i++)
            if(comics[i].substr(-3)=="txt")
                index = i;
        var url_text = comics.splice(index,1)[0];
        var text;
        console.log(text);
        console.log(comics);
        $.ajax({
            url: url_text
        }).success(on_text_loaded);
        function on_text_loaded(data) {
            load_images();
            text = data.split("\n");
            console.log(data);
        }
        function load_images() {
            for(var i=0; i<comics.length; i++) {
                $("<img class='image_being_hold'/>").attr("src", comics[i]).on("load", function(){
                    console.log("loaded image "+imgs.length);
                    imgs.push($(this));
                    if(imgs.length==comics.length)
                        on_images_loaded();
                });
            }
        }
        function on_images_loaded() {
            var i;
            for(i=0; i<comics.length; i++) {
                var img;
                for(var j=0; j<imgs.length; j++)
                    if(imgs[j].attr("src")==comics[i])
                        img = imgs[j];
                var div = $("<div class='image_holder' style='left:"+i*100+"%'></div>");
                div.append(img);
                if(text[i].length>1)
                    div.append("<div class='text'>"+text[i]+"</div>")
                $("div.story_time div.children").append(div);
            }
            $("#lastone").css("left", i*100+"%");

        }
        $(".go.left").click(function() {
            if(current==0)
                return;
            current--;
            $('.story_time .children').css("left", -current*100+"%");
        });
        $(".go.right").click(function() {
            if(current>=imgs.length)
                return;
            current++;
            $('.story_time .children').css("left", -current*100+"%");
        });
        $("form").submit(function(e){
            e.preventDefault();
            var data = $(this).serialize();
            $.ajax({
                url:"<?=$app["url"]("/email/subscribe")?>",
                dataType:"json",
                data: data
            }).done(function(msg){
                alert(msg.result);
            });
        })
    });

</script>