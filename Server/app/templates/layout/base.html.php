<?php
$app = app();
?>
<!DOCTYPE html>
<html>
<head>
  <title>Astroturfers</title>
    <link href="<?= $app["url"]('/web/assets/styles/main.css') ?>" rel="stylesheet" type="text/css" />
    <link rel="icon" href="<?= $app["url"]('/favicon.ico') ?>" type="image/x-icon"/>
    <!-- JavaScripts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
</head>
<?php
$folder = "web/assets/images/blog";
if(isset($section_folder))
    $folder = "web/assets/images/blog/$section_folder";
$files = scandir($folder);
foreach($files as $k=>$f)
    if(!is_file("$folder/$f"))
        unset($files[$k]);
    else
        $files[$k] = $app["url"]("/$folder/$f");
$files = array_values($files);
$crc = abs(crc32($image_hash));
$which = $crc%count($files);
$img = $files[$which];
$color1 = $crc%256;
$color2 = $crc/256%256;
$color3 = $crc/256/256%256;
// $color_string = "$color1, $color2, $color3";

$color_string = "0,40,0";
?>
<body style="background-color: rgb(<?=$color_string?>)">
<?php if(isset($urls)) { $u_first = true; ?>
<div class="top_links">
<?php foreach($urls as $u_name=>$u_url) {?>
    <?=!$u_first?" | ":""?><a href="<?=$u_url?>"><?=$u_name?></a>
<?php $u_first = false; } ?>
</div>
<?php } ?>
<div class="topbg" style="background-image: url('<?=$img?>')">
    <img src="<?=$img?>">
    <div class="gradient" style="background: linear-gradient(rgba(<?=$color_string?>, 0) 80%, rgba(<?=$color_string?>, 1)) 100%">
    </div>
</div>
<div class="container">
    <h1><a href="<?=$base_url?>"><?=$title?></a></h1>
    <?php echo $yield; ?>
</div>
</body>
</html>
