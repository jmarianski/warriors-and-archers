<?php
$app = app();
?>
<!DOCTYPE html>
<html>
<head>
  <title>Astroturfers</title>
    <link href="<?= $app["url"]('/web/assets/styles/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
    <link rel="icon" href="<?= $app["url"]('/favicon.ico') ?>" type="image/x-icon"/>
    <!-- JavaScripts -->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo $app["url"]('/web/assets/scripts/bootstrap.min.js'); ?>"></script>
    <meta name="description" content="Astroturfers by Treetank is an upcoming android multiplayer economic game, where you wage astral wars between players across the world." />
</head>
<body>
          <?php echo $yield; ?>
</body>
</html>
