<?php
$app = app();
?>
<div class="row">
    <div class="note col-md-12">
        <div class="note_title">
            <h2 style="text-align:center"><?=$note["title"]?></h2>
        </div>
        <div class="note_date" style="text-align:center">
            <?=substr($note["post_time"], 0, 10)?>, <?=$note["author"]?>
        </div>
        <div class="note_content">
            <p><?=implode("</p>\n<p>", explode("\n", $note["content"]))?></p>
        </div>
    </div>
    <div class="note col-md-12">
        <h3><?=t("comments")?></h3>
    </div>
    <?php if(isset($note["comments"])) { foreach($note["comments"] as $comment) { ?>
        <div class="note col-md-12">
            <HR>
            <div class="note_date">
                <?=$comment["author"]?>, <?=$comment["post_time"]?>
            </div>
            <div class="note_content">
                <p><?=implode("</p>\n<p>", explode("\n\n", $comment["content"]))?></p>
            </div>
        </div>
    <?php } } ?>
    <HR>
    <div class="note col-md-12">
        <HR>
        <form method="post">
            <input type="hidden"  name="id_note" value="<?=$note["id_note"]?>">
            <div class="note_content">
                <textarea id="summernote" name="content"></textarea>
                <input style="width:40%; border-radius:3px; border:1px solid black; padding:3px" name="author" placeholder="<?=t("author")?>">
                <button class="btn btn-primary" style="width:20%"><?=t("send")?></button>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function(){

        $.ajax({
            url: 'https://api.github.com/emojis',
            async: false
        }).then(function(data) {
            window.emojis = Object.keys(data);
            window.emojiUrls = data;
        });
        $("#summernote").summernote({
            tabsize: 2, height:200,
            hint: {
                match: /:([\-+\w]+)$/,
                search: function (keyword, callback) {
                    callback($.grep(emojis, function (item) {
                        return item.indexOf(keyword)  === 0;
                    }));
                },
                template: function (item) {
                    var content = emojiUrls[item];
                    return '<img src="' + content + '" width="20" /> :' + item + ':';
                },
                content: function (item) {
                    var url = emojiUrls[item];
                    if (url) {
                        return $('<img />').attr('src', url).css('width', 20)[0];
                    }
                    return '';
                }
            }
        });
    });

</script>