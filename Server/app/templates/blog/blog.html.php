<?php
$app = app();
?>
<div class="row">
    <?php foreach($news as $n) { ?>
        <div class="note col-md-12">
            <div class="note_title">
                <h2><?=$n["title"]?></h2>
            </div>
            <div class="note_date">
                <a href="<?=$app["url"]("/blog/note/".$n["id_note"])?>"><?=substr($n["post_time"], 0, 10)?>, <?=$n["author"]?></a>
            </div>
            <div class="note_content">
                <?=$n["lead"]?>
            </div>
            <div class="tags">
                <?=$n["tags"]?>
            </div>
            <HR>
        </div>
    <?php } ?>
</div>