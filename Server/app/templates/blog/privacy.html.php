<?php
$app = app();
?>
<div class="row">
    <div class="note col-md-12">
        <ul>
            <li>We collect only your email data for the purpose of saving your game progress on the server.</li>
            <li>Your email is transfered via google sign-in token, it is impossible to obtain it via man-in-the-middle attack.</li>
            <li>We use cookies to store your session data. That way we don't have to ask your cellphone who you are each time you send a request.</li>
        </ul>
        That's all :) We value your data and time.
    </div>
</div>