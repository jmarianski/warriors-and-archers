<?php
$app = app();
?>
<style>
    input, select, button {
        width:100%;
        margin-top:5px;
        margin-bottom: 5px;
    }
</style>
<?php if(strlen($app->request()->get("message"))>0) { ?>
<div class="row" style="background-color: #f7ecb5; margin-bottom:20px; padding-top:5px; padding-bottom:5px">
    <div class="col-md-12" style="text-align: center;">
        <?=strip_tags($app->request()->get("message"))?>
    </div>
</div>
<?php } ?>
<div class="row">
    <div class="col-md-12" style="text-align:center">
        <h3>What would you like to do?</h3>
        <form method="post" action="<?=$app["url"]("/email/subscribe")?>">
            <div style="width:25%; position:relative; left:50%; -webkit-transform: translateX(-50%);-moz-transform: translateX(-50%);-ms-transform: translateX(-50%);-o-transform: translateX(-50%);transform: translateX(-50%);">
                <select name="action" style="width:100%">
                    <option value="subscribe">Subscribe</option>
                    <option value="unsubscribe">Unsubscribe</option>
                </select><BR>
                <input type="email" name="email" placeholder="email@example.com"  style="width:100%">
                <button class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("form").submit(function(e) {
            e.preventDefault();
            var action = "<?=$app["url"]("/email/")?>"+$(this).find("[name='action']").val();
            var data = {'email':$("[name='email']").val()};
            $.ajax({
                url:action,
                data:data,
                type:'post',
                dataType:"json"
            }).done(function(msg){
                alert(msg.result);
            })
        });
    });
</script>