<?php
/*
POST /api/turns
        idzie podsumowanie tur (patrz niżej)
header: Content-Type application/json
        zwraca 200 + OK

Podsumowanie tur - każde pole x zawiera wartość o którą się zmieniło
{
"Login": "Tu unikalny mail","Land": x,"People": x,"Turns": x,
"Buildings": [
    {"Type": "House","Number": x,"Employment": x,Advancement": 0},
    {"Type": "Field","Number": x,"Employment": x,"Advancement": 0},
    tu reszta],
"Resources": 
{"Happiness": x,"Gold": x,"Food": x,"Luxury": x,"Bricks": x,"Armament": x}
}
        */
// PATH /messages
$app->path('turn', function() use($app, $request, $DB) {

    // GET (list all messages)
    $app->post(function($request) use($app, $DB) {
        $params = $request->params();
        $login = $DB->Social->isLoggedInEmail($params['Login']);
        if(!$login)
            return $app->response(403, "unauthorized");
        if($params["Id"]!=$_SESSION["id"])
            return $app->response(400, "You're not logged in on that kingdom");
        $result = $DB->Turn->onTurn($params);
        if($result)
            return $app->response(400, $result);
        return $DB->Kingdom->getKingdom($_SESSION["id"]);
    });
});
