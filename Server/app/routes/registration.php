<?php
// PATH /messages
/*
Te które zostaną:
POST /api/registration
{
  "Username": "Tu nieunikalna nazwa",
  "Login": "Tu unikalny mail",
          "KingdomName": "Tu nieunikalna nazwa królestwa"
}
header: Content-Type application/json
zwraca: 200 / 400 / 500 i podsumowanie

Podsumowanie:
{
"Username": "user name","KingdomName": "kingdom name","Land": 1050,"People": 150,"Turns": 7,
"Buildings": [
    {"Type": "House","Number": 30,"Employment": 90,"Advancement": 0.7},
    {"Type": "Field","Number": 31,"Employment": 91,"Advancement": 0.71},
    tu reszta],
"Resources": {
 "Happiness": 75,"Gold": 1000,"Food": 1000,"Luxury": 0,"Bricks": 100,"Armament": 0}
}


*/
$app->path('registration', function() use($app, $request, $DB) {
    // GET (list all messages)
    $app->post(function($request) use($app, $DB) {
      $p = $request->params();
      $lang = $p["lang"];
      $result = $DB->Kingdom->register($p['username'], $p['kingdomName'], $p['auth'], $lang, $p["id_solar_system"], $p["idrace"]);
        if($result==null)
            return $app->response(403, false);
        if($result==-1)
            return $app->response(405, false);
        $_SESSION["id"] = $result["KingdomId"];
		  return $app->response(200, $result);
    });
});
