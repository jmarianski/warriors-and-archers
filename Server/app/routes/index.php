<?php
// Options from root URL (should expose all available user choices)
$app->path(array('', 'astro', 'index'), function($request) use($app) {
    
    $app->get(function($request) use($app) {
        $data = array(
            'rel' => array('index'),
        );
        $app->format('html', function() use($app, $data) {
            $comic_strips = scandir(BULLET_ROOT."/web/assets/images/comics/");
            foreach($comic_strips as $c) {
                if(is_file(BULLET_ROOT."/web/assets/images/comics/".$c))
                    $data["comic"][] = $app["url"]("/web/assets/images/comics/".$c);
            }
            sort($data["comic"]);
            return $app->template('index', compact('data'));
        });
    });
});

    $dir = scandir("app/src/Controller/");
    $routes = [];
    foreach($dir as $f) {
        $class = "Controller\\".substr($f, 0, -4);
        if(is_file("app/src/Controller/".$f) && is_subclass_of($class, "Controller\\Controller")) {
            $routes[$class::$route] = $class;
        }
    }
    $app->path(array_keys($routes), function($request) use($app, $DB, $routes) {
        $path = $app->currentPath();
        $class = $routes[$path];
        $app->param("slug", function($request, $name) use($app, $DB, $class) {
            $action = $name;
            $app->method(array("GET", "POST"), function($request) use($app, $name, $DB, $class) {
                if($name=="")
                    $name = "index";
                $controller = new $class($DB);
                return $controller->dispatch($name);
            });
            $app->param("slug", function($request, $param) use($app, $DB, $class, $action) {
                $controller = new $class($DB);
                return $controller->dispatchWithParam($action, $param);
            });
        });
        $app->method(array("GET", "POST"), function($request) use($app, $DB, $class) {
            $controller = new $class($DB);
            return $controller->dispatch("index");
        });
    });