<?php
// PATH /messages
$app->path('diplomacy', function($request) use($app, $DB) {

    $app->post(function($request) use($app, $DB) {
        $params = $request->params();
        $login = $DB->Social->isLoggedIn($params["kingdom_id"]);
        $kingdomid = $params["kingdom_id"];
        if(!$login)
            return $app->response(403, "unauthorized");
        $result = null;
        switch($params["requestType"]) {
            case 1:
                $players = $DB->Social->getPlayers($kingdomid);
                $result =  $DB->Social->diplomaticResult(1, 1, $players, null, null, null);
            break;
            case 2:
                    $battles = $DB->Social->getBattles($kingdomid);
                    $result =  $DB->Social->diplomaticResult(2, 1, null, null, null, $battles);
                
            break;
            case 3:
                $battle = $params["battle"];
                if($login) {
                    $b = $DB->Social->addBattle($kingdomid, $battle);
                    $result =  $DB->Social->diplomaticResult(3, 1, null, null, $b, null);
                }
                else {
                    $result =  $DB->Social->diplomaticResult(3, 0, null, null, null, null);
                }
                break;
            case 4:
                $battle = $params["battle"];
                if($login && $DB->Social->updateBattle($kingdomid, $battle)) {
                    $result =  $DB->Social->diplomaticResult(4, 1, null, null, $battle, null);
                }
                else {
                    $result =  $DB->Social->diplomaticResult(4, 0, null, null, null, null);
                }
                break;
            case 5:
                $battle = $params["battle"];
                if($login && $DB->Social->deleteBattle($kingdomid, $battle)) {
                    $result =  $DB->Social->diplomaticResult(5, 1, null, null, $battle, null);
                }
                else {
                    $result =  $DB->Social->diplomaticResult(5, 0, null, null, null, null);
                }
                break;


        }
        if($result==null)
            return $app->response(200, "null");
        return $app->response(200, $result);
    });
});
