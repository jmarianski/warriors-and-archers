<?php
// PATH /api
$app->path('api', function($request) use($app, $request, $DB) {
	if($DB->Recount->recount($request->url())) {
		echo "{\"Recount\":true}";
		exit;
	}
	require 'login.php';
	require 'registration.php';
	require 'turns.php';
	require 'messages.php';
	require 'diplomacy.php';
    $app->path('recount', function($request) use($app, $request) {
        $PhpCommand = new Model\PhpCommand();
        return $PhpCommand->recount();
    });
    $app->path('recount_test', function($request) use($app, $request, $DB) {
        $DB->Recount->executeRecount();
        return 200;
    });
});
