<?php
// PATH /messages
$app->path('msg', function($request) use($app, $DB) {

    // GET (list all messages)
    /*
    Request API: 
    public int requestType;
    public int channel;
    public int kingdom_id;
    public int last_update;
    Result API: 
            public int requestType;
            public String message;
            public List<Channel> channels;
            public Channel channel;
            public List<Message> messages;
    */
    $app->post(function($request) use($app, $DB) {
        $params = $request->params();
        $login = $DB->Social->isLoggedIn($params["kingdom_id"]);
        if(!$login)
            return $app->response(403, "unauthorized");
        switch($params["requestType"]) {
            case 0:
            $r = $DB->Social->grabChannels($params["kingdom_id"]);
            if(count($r)==0)
                $result = $DB->Social->chatResult(0, "null", null, null, null);
            else
                $result = $DB->Social->chatResult(0, "ok", $r, null, null);
            break;
            case 1:
            $r = $DB->Social->grabMessages($params["channel"], $params["last_update"]);
            $ch = $DB->Social->grabChannel($params["channel"], $params["kingdom_id"]);
            if(count($r)==0)
                $result = $DB->Social->chatResult(1, "null", null, $ch, null);
            else
                $result = $DB->Social->chatResult(1, "ok", null, $ch, $r);
            break;
            case 2:
            $DB->Social->sendMessage($params["channel"], $params["kingdom_id"], $params["text"]);
            $r = $DB->Social->grabMessages($params["channel"], $params["last_update"]);
            $ch = $DB->Social->grabChannel($params["channel"]);
            if(count($r)==0)
                $result = $DB->Social->chatResult(2, "null", null, $ch, null);
            else
                $result = $DB->Social->chatResult(2, "ok", null, $ch, $r);
            break;
        }
        if($result==null)
            return $app->response(200, "null");
        return $app->response(200, $result);
    });
});
