<?php
// PATH /messages
/*
POST /api/login
{
  "Login": "Tu unikalny mail"
}
header: Content-Type application/json
zwraca: 200 / 400 / 500 i podsumowanie

Podsumowanie:
{
"Username": "user name","KingdomName": "kingdom name","Land": 1050,"People": 150,"Turns": 7,
"Buildings": [
    {"Type": "House","Number": 30,"Employment": 90,"Advancement": 0.7},
    {"Type": "Field","Number": 31,"Employment": 91,"Advancement": 0.71},
    tu reszta],
"Resources": {
 "Happiness": 75,"Gold": 1000,"Food": 1000,"Luxury": 0,"Bricks": 100,"Armament": 0}
}

*/
$app->path('login', function() use($app, $request, $DB) {

    // GET (list all messages)
    $app->post(function($request) use($app, $DB) {
        // getKingdomForUser
        // 
        $p = $request->params();
        if(strlen($p["auth"])>=4) {
            $result = $DB->Kingdom->getKingdomForAuth($p["auth"], $p["kingdom"]);
            if ($result == -1)
                return $app->response(405, false);
            $_SESSION["id"] = $result["KingdomId"];
        }
        if(count($result)==0 || strlen($p["auth"])<4)
            return $app->response(200, array(
                "solar_systems"=>$DB->Kingdom->getSolarSystems(),
                "races"=>$DB->Kingdom->getRaces(),
            ));
        return $app->response(200, array("summary"=>$result));
    });
});
