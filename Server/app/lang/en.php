<?php
return array(
    "turn" => array(
        "greeting" => "Greetings sir! I will be your adviser. If there is anything going in the city, I'll tell you everything.",
        "first_war_turn" => "The Great Night has come! It is when the war ships set their sails to other countries. We're safe now, god is protecting us, but on any other day we'd be toast.",
        "first_first_turn" => "I've written some reports about the Great Night. If you'd like to read them, please head over to the war room.",
        "neutral_turn" => array(
            "Some of your men constructed a boat and decided to fly into the sun. They came back. In great shock they told me, that they bumped of the sun.",
            "Some merchants appeared and wanted to sell you some old pottery. I've sent them off",
            "There was an uproar in the city council after an elderly citizen condemned your actions. However, apart from using foul language he didn't say anything wrongful.",
            "There was a discussion in the city about recycling. Turns out it was all a trash talk.",
            "I've received an offer from a neighbouring kingdom that wanted my help in running their country. I turned down that offer. I would never betray you, my liege!",
            "You may have not known, but year cycle is broken in these lands. After every month there comes a Great Night. That is when everyone attacks each other.",
            "There are a lot of barbarians around these parts. They will attack mercilessly, but they are weak. Don't fear them, but be prepared for them.",
            "I've come to the conclusion that I'm using too much sugar in my coffee. It makes me nervous when preparing daily reports for you, sir.",
            "There was a gang of cats that has been terrorizing the population. The problem was dealt with when your farmers started harvesting catnip.",
            "You may have heard of the legend of the lady of the lake? Turns out it was true. However, we found out only when the water from the lake has drained out.",
            "We had a doomsayer in our country. Some men decided to throw him out of the island, but he orbited back to the other side.",
            "Some people claimed that you're not the ruler of this country, but a puppet of a godlike creature controlling you with a rectangular glowing box. I told them it was ridiculous claim and that they should go home.",
            "There was a huge card tournament in your city, but unfortunately a huge fight has erupted. The tournament was cancelled and no one was declared the winner. Ah, the spirit of sportsmanship!",
            "There was a huge card tournament in your city. After the winner was announced, a huge fight has erupted. Tournament organizers declared, that they will never re-organize it.",
            "There was a huge card tournament in your city. The winners were announced and everyone thought it was a fair and square win, so it went down peacefully. Ah, the spirit of sportsmanship!",
            "There was a dice tournament in your city. Some people decided to come with knifes, as they though it was a cooking tournament. Some people never learn.",
            //""
        ),
        "unhappiness_reason" => array(
            "Gold"=>"It seems some of the people are fleeing you, because you haven't paid them their money. Try to leave some gold at the end of the turn for your people.",
            "Food"=>"It seems some of the people are fleeing you, because they are hungry. Try producing more of the food in advance, so that when the time is dire, they can dine on reserves.",
            "high_employment"=>"It seems some of the people are unhappy, because you started employing elderly and children. Reduce your employment rate",
            "low_employment"=>"It seems some of the people are unhappy, because people are jobless and can't afford the life they used to have. Try increasing your employment rate.",
        ),
        "war_turn"=> array(
            "The war drums are beating! The time to send out ships for war is now!",
            "The Great Night is coming! We must prepare ourselves.",
            "I hear distant screams! The Great Night is coming.",
            "I see ships on the horizons! They must be coming from the land of ice and stone, where there is a midnight sun and hot springs flow.",
            "I hear the war drums! I hope Ragnarok is not upon us."
        ),
        "first_turn" => array(
            "The night is finally over! Let's check our stocks and prepare ourselves for another day.",
            "The ships have sailed away! We're safe. Gods, blessed be you!",
            "The day has come! Oh, thank gods!",
            "The war drums have gone silent. We can finally rest for a while."
        ),
    ),
    "bot_kingdoms" => array(
        "base" => array(
            "Visigoths",
            "Ostrogoths",
            "Vagoths",
            "Slavs",
            "Bretons",
            "Vikings",
            "Pagans",
            "Huns",
            "Persians",
            "Saxons",
            "Franks",
            "Vandals",
            "Ascomanni",
            "Lochlannach",

        ),
        "attribute" => array(
            "Brave",
            "Courageous",
            "Relentless",
            "Bloodthirsty",
            "Harsh",
            "Ruthless",
            "Ravenous",
            "Brutal",
            "Handsome",
            "Dangerous",
            "Cunning",
            "Vicious",
            "Unforgiving",
            "Merciless",
            "Vengeful",
            "Deadly",
            "Furious",
            "Wrathful",
            "Malevolent",
            "Prideful",
            "Rabid",
            "Valorous",
            "Triumphant",
            "Fierce",
            "Pale",
            "Coldblooded"
        )
    ),
    "solar_systems" => array(
        "Alkarab", "Alkes", "Al Kurud", "Almaaz", "Almach", "Al Minliar al Asad", "Alnair", "Alnasl", "Alnilam", "Alnitak", "Alniyat",
        "Alphard", "Alphecca", "Alpheratz", "Arrakis", "Alrescha", "Alsafi", "Alsciaukat", "Alsephina", "Alshain", "Alshat", "Altair", "Altais",
        "Altarf", "Alterf", "Aludra", "Alula Australis", "Alula Borealis", "Alya", "Alzirr", "Ancha", "Angetenar", "Ankaa", "Anser",
        "Antares", "Arcturus", "Arkab Posterior", "Arkab Prior", "Arneb", "Ascella", "Asellus Australis", "Asellus Borealis", "Asellus Primus",
        "Asellus Secundus", "Asellus Thertius", "Asmidiske", "Aspidiske", "Asterope", "Athebyne", "Atik", "Atlas", "Atria", "Avior",
        "Azelfafage", "Azha", "Barnard's Star", "Baten Kaitos", "Beemim", "Beid", "Bellatrix", "Betelgeuse", "Bharani", "Biham", "Botein", "Brachium",
        "Canopus", "Capella", "Caph", "Castor", "Castula", "Cebalrai", "Celaeno", "Cervantes", "Chalawan", "Chamukuy", "Chara", "Chertan", "Copernicus",
        "Cor Caroli", "Cujam", "Cursa", "Dabih", "Dalim", "Deneb", "Deneb Algedi", "Denebola", "Diadem", "Diphda", "Dschubba", "Dubhe", "Dziban", "Edasich",
        "Electra", "Elnath", "Eltanin", "Enif", "Errai", "Fafnir", "Fang", "Fum al Samakah", "Fomalhaut", "Fulu", "Furud", "Fuyue", "Gacrux",
        "Garnet Star", "Giausar", "Gienah", "Ginan", "Gomeisa", "Graffias", "Grumium", "Hadar", "Haedus", "Hamal", "Hassaleh", "Hatysa", "Helvetios",
        "Heze", "Homam", "Iklil", "Intercrus", "Izar", "Jabbah", "Jishui", "Kaffaljidhma", "Kang", "Kaus Australis", "Kaus Borealis", "Kaus Media",
        "Keid", "Khambalia", "Kitalpha", "Kochab", "Kornephoros", "Kraz", "Kuma", "Kurhah", "La Superba", "Larawag", "Lesath", "Libertas", "Lich",
        "Lilii Borea", "Maasym", "Mahasim", "Maia", "Marfark", "Marfik", "Markab", "Markeb", "Marsic", "Matar", "Mebsuta", "Megrez", "Meissa", "Mekbuda",
        "Meleph", "Menkalinan", "Menkar", "Menkent", "Menkib", "Merak", "Merga", "Meridiana", "Merope", "Mesarthim", "Miaplacidus", "Mimosa", "Minchir",
        "Minelauva", "Mintaka", "Mira", "Mirach", "Miram", "Mirfak", "Mirzam", "Misam", "Mizar", "Mothallah", "Muliphein", "Muphrid", "Muscida", "Musica",
        "Naos", "Nashira", "Navi", "Nekkar", "Nembus", "Nihal", "Nunki", "Nusakan", "Ogma", "Peacock", "Phact", "Phecda", "Pherkad", "Pipirima", "Pleione",
        "Polaris", "Polaris Australis", "Polis", "Pollux", "Porrima", "Praecipua", "Prima Hyadum", "Procyon", "Propus", "Proxima Centauri", "Ran", "Rana",
        "Rasalas", "Rasalgethi", "Rasalhague", "Rastaban", "Regor ", "Regulus", "Revati", "Rigel", "Rigil Kentaurus", "Rotanev", "Ruchbah",
        "Rukbat", "Sabik", "Saclateni", "Sadachbia", "Sadalbari", "Sadalmelik", "Sadalsuud", "Sadr", "Saiph", "Salm", "Sargas", "Sarin", "Sarir", "Sceptrum",
        "Scheat", "Schedar", "Secunda Hyadum", "Segin", "Seginus", "Sham", "Shaula", "Sheliak", "Sheratan", "Sirius", "Situla", "Skat", "Spica", "Sualocin",
        "Subra", "Suhail", "Sulafat", "Syrma", "Tabit", "Taiyangshou", "Taiyi", "Talitha", "Tania Australis", "Tania Borealis", "Tarazed", "Taygeta", "Tegmine",
        "Tejat", "Terebellum", "Thabit", "Theemin", "Thuban", "Tiaki", "Tianguan", "Tianyi", "Titawin", "Tonatiuh", "Torcular", "Tureis", "Unukalhai",
        "Unurgunite", "Vega", "Veritate", "Vindemiatrix", "Wasat", "Wazn", "Wezen", "Wurren", "Xamidimura", "Xuange", "Yed Posterior", "Yed Prior", "Yildun",
        "Zaniah", "Zaurak", "Zavijava", "Zhang", "Zibal", "Zosma", "Zubenelgenubi", "Zubenelhakrabi", "Zubeneschamali"
    ),
    "war" => array(
        "unknown" => "barbaric horde",
        "attack" => array(
            "battle" => "There was a battle between %s against %s. ",
            "of" => "%s of %s",
            "defense" => "We've been attacked by %s! ",
            "lost"=>"We've lost %s. ", // %d %s divided by commas.
            "attack" => "We have attacked %s!",
            "gain" => "We've gained %s. ",
            "nothing" => "nothing",
            "success" => array(
                "It was a tremendous success!",
                "It was a great success!",
                "It was a slight success!",
                "It was a success!"
            ),
            "failure" => array(
                "It was a terrible loss!",
                "It was a great loss!",
                "It was a little failure.",
                "It was a failure."
            ),
            "neither" => "The result was inconclusive."
        ),
        "attack_string" => "Attack",
        "defense_string" => "Defense"
    ),
    "attack" => array(
        "intro" => array(
            "defense" => array(
                "It was a dark night when the army arrived. Distant stars were waking up and only their fade light alarmed the defending kingdom of the forces at their gates.",
                "It was a rainy night. Water poured out of the sky like it was meant to drown all of the people on this piece of land. The attack was the least pleasurable thing that could happen right now.",
                "It was a cold night. The air was freezing and everyone was staring aimlessly before themselves.",
                "It was a pleasant night. Distant stars were blinking merrily in the sky above the kingdom."
            ),
            "attack" => array(
                "It was a dark night when we arrived at the shores. Distant stars were waking up and only their fade light alarmed the defending kingdom of our forces.",
                "It was a rainy night. Water poured out of the sky like it was meant to drown all of the people on this boat. Only the thought of the glory of battle seemed like a pleasant thing right now.",
                "It was a cold night. The air was freezing and everyone was staring aimlessly before themselves.",
                "It was a pleasant night. Distant stars were blinking merrily in the sky above the ships."
            ),
        ),
        "general" => array(
            "defense" => array(
                "As the ships went ashore, the general decided to speak to his army: ",
                "The battle was about to start, when the general decided to stop everyone. His voice rang across the valley as he spoke: ",
                "The general spoke these words: ",
                "As everyone readied their arms, general spoke these words: ",
            ),
            "attack" => array(
                "As the soldiers were moving out of the ships, the general spoke: ",
                "The battle was about to start, when the general decided to stop everyone. His voice rang across the valley as he spoke: ",
                "When the ships were closing in, the general decided to speak from the steering wheel: ",
                "The general spoke these words: ",
                "As everyone readied their arms, general spoke these words: ",
            ),
        ),
        "speech" => array(
            "defense" => array(
                "\"The night is young, my brothers and sisters. Let's make those bastards pay that they dared to attack us!\"",
                "\"It is a sorry thing, that those bastards decided to attack us. Let us make sure they will pay for attacking us!\"",
                "\"Alas, we're under attack. Many of us will surely die. But let's make everyone count. To arms, my soldiers!\"",
                "\"It is a worrisome day. We're attacked, unannounced, by those brutes. But we won't make it easy for them. We will fight, till the last drop of our blood! Till we breathe our last breath. To arms!\"",
                "\"I'm at loss of my words. Our noble nation gets attacked by those brutes! It cannot go unpunished, my people, my warriors, my brothers and sisters. To arms! Make them pay with their blood!\"",
                "\"On the right and left abyss encloses us, without even a single ship for escape. The river around us; the mountains behind us. Your enemy is before you, protected by an innumerable army; he has men in abundance, but vou, as your only aid, have your own swords, and, as your only chance for life, such chance as you can snatch from the hands of your enemy. If the absolute want to which you are reduced is prolonged ever so little, if you delay to seize immediate success, your good fortune will vanish, and your enemies, whom your very presence has filled with fear, will take courage. Put far from you the disgrace from which you flee in dreams, and attack this monarch who decided to strike out at our stronghold. Here is a splendid opportunity to defeat him, if you will consent to expose yourselves freelv to death. Do not believe that I desire to incite you to face dangers which I shall refuse to share with you. In the attack I myself will be in the fore, where the chance of life is always least.\"",
                "\"We have nowhere to go, wether we want to or not - and so we must fight. So let us bring no shame to our homeland, but lay our bones down here, for the dead can bear no shame. If we run, however, dishonor will be upon us. So run we shall not, but stand firm and I shall go in front of you: if my head falls, then you’re free to care for yours as you see fit.\"",
                "\"I have, myself, full confidence that if all do their duty, if nothing is neglected, and if the best arrangements are made, as they are being made, we shall prove ourselves once again able to defend our Island home, to ride out the storm of war, and to outlive the menace of tyranny, if necessary for years, if necessary alone. At any rate, that is what we are going to try to do. That is the resolve of our ruler. That is the will of our governors and the nation. We will defend to the death our native soil, aiding each other like good comrades to the utmost of our strength. To arms!\"",
                "\"Upon this battle depends the survival of our civilization. Upon it depends our own lives, and the long continuity of our institutions and our kingdom. The whole fury and might of the enemy must very soon be turned on us. Let us therefore brace ourselves to our duties, and so bear ourselves that if our legacy lasts for a thousand years, men will still say, ‘This was their finest hour.’\"",
                "\"The battle, is not to the strong alone; it is to the vigilant, the active, the brave. It is now too late to retire from the contest. There is no retreat but in submission and slavery! Our chains are forged! Their clanking may be heard on the plains of nothingness! Is life so dear, or peace so sweet, as to be purchased at the price of chains and slavery? Forbid it, almighty gods! I know not what course others may take; but as for me, give me liberty, or give me death!\"",
                "\"What is our aim? I can answer in one word. It is victory. Victory at all costs – Victory in spite of all terrors – Victory, however long and hard the road may be, for without victory there is no survival. To arms!\"",
                "\"The path we have chosen for the present is full of hazards, as all paths are; but it is the one most consistent with our character and courage as a nation and our commitments around here. The cost of freedom is always high, but we have always paid it. And one path we shall never choose, and that is the path of surrender or submission. To arms!\"",

                ),
            "attack" => array(
                "\"It is a good day, my brothers. The time has come, to shed some blood! Burn their houses, slaughter their children, let none survive!\"",
                "\"There are two things ahead of us: our great victory, or greatness in the afterlife. To arms!\"",
                "\"We have arrived, my brethren. It means, there is only one thing ahead of us: glory! To arms!\"",
                "\"Sometimes I wonder if it is a good idea to go out into the battlefield. We are dying for our masters for the piece of land. But seeing you bathe in the blood of our enemies with the smile on your faces makes me welcome every day with open hands. Let us make haste! The enemy awaits us, and I want to see those smiles again. To arms!\"",
                "\"Tonight is a very special night. We fight for the glory of our people. For our husbands and wives, for our children! Let them know, that the mightiest have fought for their peace. To arms, my brother and sisters!\"",
                "\"The weather is terrible, people. It is a shame we didn't have better luck on our side. Doesn't matter. The wind may howl, the rain may pour, the snow may cover ur eyes, but we will not falter! To arms, brothers and sisters!\"",
                "\"There are some who are alive at this moment who will not be alive shortly. Those who do not wish to go on that journey, we will not send. As for the others I expect you to rock their world. Wipe them out if that is what they choose. But if you are ferocious in battle remember to be magnanimous in victory. The ones who wish to fight, well, we aim to please. Our business now is up on this shore.\"",
                "\"What is our aim? I can answer in one word. It is victory. Victory at all costs – Victory in spite of all terrors – Victory, however long and hard the road may be, for without victory there is no survival. To arms!\"",
                "\"The path we have chosen for the present is full of hazards, as all paths are; but it is the one most consistent with our character and courage as a nation and our commitments around here. The cost of freedom is always high, but we have always paid it. To arms!\"",
                "\"We shall go our way into battle. And we shall be accompanied by the spirit of millions of our martyrs, our ancestors tortured and burned for their faith, our murdered fathers and butchered mothers, our murdered brothers and strangled children. To arms!\"",
                "\"You are about to embark upon the Great Crusade, toward which we have striven these many months. The eyes of all worlds are upon you. The hopes and prayers of liberty-loving people everywhere march with you. In company with our brothers-in-arms on other fronts, you will bring about the destruction of the vicious war machine, the eliminations of tyranny over oppressed people of all the skyscape. To arms!\"",
                ),
        ),
        "battle_intro" => array(
            "The soldiers have rushed towards each other.",
            "With song on their mouths soldiers rushed forward.",
            "Shouting, the army rushed forward.",
            "Army went forward in tight chic.",
        ),
        "battle_end_before_start" => array(
            "defense_won" => array(
                "However, as the general stopped speaking, the attacking armies broke out. The differences were too great to risk anything, and even the morale inducing speech couldn't make them fight together.",
                "Attacking army was shaking with fear and when the moment arrived to attack, the armies collapsed. They returned to ships without hesitation, since attack with such disadvantage would be foolish to continue."
                ),
            "attack_won" => array(
                "However, as the general stopped speaking, the defending armies broke out. The differences were too great to risk anything, and even the morale inducing speech couldn't make them fight together.",
                "Defending army was shaking with fear and when the moment arrived to attack, the armies collapsed. They returned to their homes without hesitation, since defense with such disadvantage would be foolish."
            ),
        ),
        "archers_intro" => array (
            "The arrows rained down from the sky.",
            "From the horizon you saw archers shooting toward your people.",
            "Arrows from your enemies were relentless.",
        ),
        "archers_outcome" => array(
            "Before the warriors could even clash their arms, archers have killed %d of our soldiers.",
            "Before they could react, archers have slain %d of our soldiers.",
        ),
        "abrupt_end" => array(
            "However, soon after the battle was over. Army differences were too great to be inconsiderable.",
            "The battle was over before it even started. The arrows were enough to crush the morale of the opposing force."
        ),
        "random_intersection" => array(
            "The battle was taking longer than expected. Even the clouds felt it, pouring all of it's might down on the armies of men cooling their hatred towards each other.",
            "One of the generals was struck down by a lonely arrow. His lungs pierced disabled him to speak his last words. It was a sorry death.",
            "For a while it didn't seem like the battle was about to stop, dust was flying everywhere, blood was soaking into the battlefield. ",
            "Battle seemed like it wouldn't end. The metal clashing and people shouting were unending and everywhere there was something happening.",
            "The battle lasted so long, even the crows started gathering above the armies.",
            "The storm came and with it, thunders started striking, killing several on the battlefield.",
            "Wind howled relentlessly, slightly repressing the sound of men fighting each other.",

        ),
        "middle_results" => array(
            "After a while, our soldiers have dropped in numbers, as we've lost %s.",
            "It was clear, we had our losses, as we've lost %s.",
            "Unfortunately, every battle has it's costs, for we've lost %s.",
            "In the meantime we've lost %s.",
            "The time has passed and we've lost %s.",
        ),
        "battle_was_won" => array(
            "As soon as the dust has settled it was clear what has happened. The victory was ours!",
            "The battle was won!",
            "When the soldiers came back to the garrison, it was clear. We won the battle!"
        ),
        "battle_was_lost" => array(
            "As soon as the dust has settled it was clear what has happened. The battle was lost.",
            "The battle was lost.",
            "When the soldiers came back to the garrison, it was clear. We have lost the battle."
        ),
        "lost_soldiers" => array(
            "Regretfully, we've lost %s.",
            "Unfortunately, we've lost %s.",
            "Soldiers that were lost: %s",
            "I have to pronounce the death of %s."
        ),
        "no_loss" => array(
            "It seems we haven't lost any soldier in this battle.",
            "What a great day! We haven't lost any soldier in this battle."
        ),
        "enemy_lost_soldiers" => array(
            "Sacrifice of our soldiers were not in vain, as we killed %s.",
            "We have slain %s.",
        ),
        "no_enemy_loss" => array(
            "The enemy haven't lost a soldiers. What a pity!",
            "The enemy haven't lost anyone.",
        ),
        "soldiers_names" => array(
            "Barracks" => "warriors",
            "ArcherHouse" => "archers"
        )
    ),
    "races" => array(
        "1"=>"Men"
    ),
    "resources" => array(
        "Land" => "Land",
        "People" => "People",
        "Happiness" => "Happiness",
        "Food" => "Food",
        "Gold" => "Gold",
        "Luxury" => "Luxury",
        "Armaments" => "Armaments",
        "Bricks" => "Bricks",
        "Science" => "Science",
        "Bcap" => "Building capability"
    ),
    "most_suited_for_you" => "Most suited for you",
    "create_new_ss" => "Create new solar system",
    "title_blog_index" =>"All notes",
    "title_blog_write" => "Write",
    "title_blog_login" => "Login",
    "title_blog_register" => "Register",
    "comments"=>"Comments",
    "author"=>"Author",
    "title"=>"Title",
    "send"=>"Send",
    "lead"=>"Lead",
    "note_text"=>"Note contents",
    "author_name"=>"Author name",
    "login"=>"login",
    "password"=>"password",
    "log_in"=>"Login",
    "register"=>"Register",
    "title_email_index" => "Emails",
    "title_blog_privacy" => "Privacy policy",
    "chat_channel_desc" => "Automatically generated chat for solar system %s",
);
