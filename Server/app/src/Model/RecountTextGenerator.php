<?php
/**
 * Created by PhpStorm.
 * User: Jack
 * Date: 28.02.2018
 * Time: 00:04
 */

namespace Model;


class RecountTextGenerator
{
    public function generateAttackMessage($attackers, $defenders, $me, $victory, $turn, $changes, $turns) {
        $lang = $me["Info"]["lang"];
        $war = t("war", $lang);
        if(in_array($me, $attackers)) {
            $title = $war["attack_string"];
        } else {
            $title = $war["defense_string"];
        }
        $resources = t("war", $lang);
        $attackers_titles = [];
        foreach($attackers as $attacker) {
            $user = strlen($attacker["Info"]["user"])==0?$war["unknown"]:$attacker["Info"]["user"];
            $attackers_titles[] = sprintf($war["attack"]["of"], $user, $attacker["Info"]["name"]);
        }
        $defenders_titles = [];
        foreach($defenders as $defender) {
            $user = strlen($defender["Info"]["user"])==0?$war["unknown"]:$defender["Info"]["user"];
            $defenders_titles[] = sprintf($war["attack"]["of"], $user, $defender["Info"]["name"]);
        }
        $message = sprintf($war["attack"]["battle"], implode(", ", $attackers_titles), implode(", ", $defenders_titles));
        if($victory==1)
            $message .= sprintf($war["attack"]["success"][$turn/5]);
        else if($victory==-1)
            $message .= sprintf($war["attack"]["failure"][$turn/5]);
        else
            $message .= sprintf($war["attack"]["neither"]);
        $message .=" ";
        $gained = [];
        $lost = [];
        foreach($changes as $key=>$value) {
            $name = array_key_exists($key, $resources)?$resources[$key]:$key;
            if($value<0)
                $lost[] = ((int)-$value)." $name";
            else if($value>0)
                $gained[] = ((int)$value)." $name";
        }
        if(count($gained)>0)
            $message .= sprintf($war["attack"]["gain"], implode(", ", $gained));
        if(count($lost)>0)
            $message .= sprintf($war["attack"]["lost"], implode(", ", $lost));
        $message .= "\n\n".$this->generateTurnByTurn($lang, $attackers, $defenders, $turns, $me["Info"]["id"], $victory, in_array($me["Info"]["id"], array_keys($attackers))?"attack":"defense");
        echo $message;
        return [
            "message" => $message,
            "title"=>$title
        ];
    }

    function random_array($array) {
        return $array[rand(0, count($array)-1)];
    }
    function random_array_and_remove(&$array) {
        $key = rand(0, count($array)-1);
        $text = $array[$key];
        unset($array[$key]);
        $array = array_values($array);
        return $text;
    }

    private function generateTurnByTurn($lang, $attackers, $defenders, $turns, $me_id, $victory, $type = "defense") {
        $message = array();
        $war = t("attack", $lang);
        $message[] = $this->random_array($war["intro"][$type]);
        $message[] = $this->random_array($war["general"][$type]);
        $message[] = $this->random_array($war["speech"][$type])."\n\n";
        if(count($turns)==0) {
            $message[] = $this->random_array($war["battle_end_before_start"][$type.($victory==1?"_won":"_lost")]);
        } else {
            $message[] = $this->random_array($war["battle_intro"]);
            $lost = 0;
            for($i=0; $i<5; $i++) {
                foreach($turns[$i]["lost"][$me_id] as $number)
                    $lost += $number;
            }
            $message[] = $this->random_array($war["archers_intro"]);
            $message[] = sprintf($this->random_array($war["archers_outcome"]), $lost);
            if(!isset($turns[5])) {
                // turn 5 signifies the beginning of warriors attack
                $message[] = $this->random_array($war["abrupt_end"]);
            } else {
                $texts = $war["random_intersection"];
                for($i=5; $i<20 && isset($turns[$i]); $i+=4) {
                    //block
                    $lost = [];
                    for($j=0; $j<4; $j++) {
                        foreach($turns[$i+$j]["lost"][$me_id] as $building=>$number)
                            $lost[$building] += $number;
                    }
                    $lost_soldiers = [];
                    foreach($lost as $building=>$number) {
                        $lost_soldiers[] = $number." ".$war["soldiers_names"][$building];
                    }
                    $message[] = "\n\n".$this->random_array_and_remove($texts);
                    if(random()>0.2)
                        $message[] = $this->random_array_and_remove($texts);
                    $message[] = sprintf($this->random_array($war["middle_results"]), implode(", ", $lost_soldiers));
                }
            }
            if($victory==1)
                $message[] = "\n\n".$this->random_array($war["battle_was_won"]);
            else
                $message[] = "\n\n".$this->random_array($war["battle_was_lost"]);
            $lost = [];
            for($i=0; isset($turns[$i]); $i++) {
                    foreach($turns[$i]["lost"][$me_id] as $building=>$number)
                        $lost[$building] += $number;
                }
            $lost_soldiers = [];
            foreach($lost as $building=>$number) {
                $lost_soldiers[] = $number." ".$war["soldiers_names"][$building];
            }
            $message[] = sprintf($this->random_array($war["lost_soldiers"]), implode(", ", $lost_soldiers));
        }
        return trim(implode(" ",$message));
    }

}