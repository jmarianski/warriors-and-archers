<?php

namespace Model;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mail extends PHPMailer {

	public function __construct() {
		require(BULLET_ROOT."/app/config/mail.php");
		$this->isSMTP();                                      // Set mailer to use SMTP
		$this->Host = $mail["host"];  // Specify main and backup SMTP servers
		$this->SMTPAuth = true;                               // Enable SMTP authentication
		$this->Username = $mail["username"];                 // SMTP username
		$this->Password = $mail["password"];                           // SMTP password
		$this->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		$this->Port = 465; 
		$this->setFrom($mail["from"]);
		$this->isHTML(true);        
		$this->CharSet = 'UTF-8';
		$this->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true
		    )
		);
	}
}