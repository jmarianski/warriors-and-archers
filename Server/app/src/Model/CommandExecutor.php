<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Model;

/**
 * Description of CommandExecuter
 *
 * @author Jacek
 */
class CommandExecutor {
    private $options;
    public function __construct(){
        $this->setDefaults();
    }
	/**
    * Sets default values for function
    */
    public function setDefaults(){
        // do nothing in default implementation
    }
    /**
     * Sets option for wkhtmltopdf. They will be concatendated using this
     * formula: "--$key $value --$key2 $value2 (...)"
     * @param type $key name
     * @param type $value value
     */
    public function set_option($key,$value){
            $this->options[$key] = $value;
    }
    /**
     * Resets all options. If you want default ones, use setDefaults immediately
     * after reset.
     */
    public function reset() {
        unset($this->options);
    }
    /**
     * Gets all options concatenated using this
     * formula: "--$key $value --$key2 $value2 (...)"
     * @return type
     */
    public function get_opts() {
        $opts = "";
        foreach($this->options as $key=>$val){
                $opts.=" $key $val ";
        }
        return $opts;
    }
        /**
     * Generates default path for generated documents. Will use random number
     * and prefix to generate file name. 
     * @param string $ext
     * @param string $prefix
     */
    public function generatePath($ext, $prefix = 0) {
        $tmp = new TmpFile("",$ext);
        return $tmp->getPath();
        if($prefix==0)
            $prefix = "gen";
        $to = ROOT_PATH."/pdftmp/".$prefix.rand(10000,99999).".".$ext;
        return $to;
    }

     /**
     * Runs default command in the system. Provides basic error handling.
     * If you'd like to print error as soon as possible, uncomment section 
     * below.
     * @param string $command Command to run
     */
    public function run_default_command($command) {
        $descriptors = array(
            1   => array('pipe','w'),
            2   => array('pipe','w'),
        );
        $pipes = array();
        $process = proc_open($command, $descriptors, $pipes, null, null, array('bypass_shell'=>false));
        if (is_resource($process)) {

            $stderr = stream_get_contents($pipes[2]);
            $out = stream_get_contents($pipes[1]);
            fclose($pipes[2]);
            $result = proc_close($process);
                        
            if ($result==-1) {
                $error = "Warning: an error occured.\n"
                        . "Command: ".$command
                        . "\n$result\n$stderr";
            }
            else
                return $out;
            
        } else {
            $error = "Could not run command $command";
        }
        if(isset($error)) {
            throw new \Exception($error);
        }
    }
    
    function execInBackground($cmd) {
        if (substr(php_uname(), 0, 7) == "Windows"){
            pclose(popen("start /B ".$cmd, 'r'));
        }
        else {
            exec($cmd . " > /dev/null &");  
        }
    } 
}
