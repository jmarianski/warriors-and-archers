<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Model;

/**
 * Description of PhpCommand
 *
 * @author Jacek
 */
class PhpCommand extends CommandExecutor {
    //put your code here
    // changes for test push
    public function dump_db($host, $user, $password) {
        $split = explode(":", $host);
        if(count($split)>1)
            $port = "--port ".$split[1];
        if(isset($user))
            $u = "-u $user";
        if(isset($password))
            $p = "--password=\"$password\"";
        $cmd = "mysqldump --host ".$split[0]." $port $u $p warriors > \"".getcwd()."/db/".date("Y-m-d-H-i").".sql\"";
        $this->run_default_command($cmd);
    }
    
    public function recount() {
        $this->execInBackground("php recount.php");
    }
}
