<?php
namespace Controller;

class EmailController extends Controller {

	public static $route = "email";

	public function indexAction($page = 0) {
	    return ["result"=>"ok"];
	}

    public function subscribeAction() {

        $email = app()->request()->get("email");
        $this->DB->News->addToMailingList($email);
        if(!app()->request()->isAjax())
            return app()->response()->redirect("index?message=Email added successfully!");
        return ["result"=>"Email added successfully!"];
    }
    public function unsubscribeAction() {
        $email = app()->request()->get("email");
        $this->DB->News->removeFromMailingList($email);
        if(!app()->request()->isAjax())
            return app()->response()->redirect("index?message=Email removed successfully!");
        return ["result"=>"Email removed successfully!"];
    }
}