<?php
namespace Controller;

use Model\Mail;

class CronController extends Controller {

	public static $route = "cron";

    public function minutes1Action() {
        set_time_limit(3600);
        $list = $this->DB->News->getMailingList();
        $notes = $this->DB->News->getNotesToSend();
        foreach ($notes as $note) {
            //in case this occurs in the same time as other request
            $notes2 = $this->DB->News->getNotesToSend();
            $found = false;
            foreach($notes2 as $row)
                if($row["ID"]==$note["ID"])
                    $found = true;
            if(!$found)
                continue;
            else
                $this->DB->News->updateNoteIsSent($note["ID"]);
            // end if note occurs at the same time
            foreach($list as $mail_recipent) {
                $email = $mail_recipent["email"];
                $mail = new Mail();
                $mail->addAddress($email);
                $mail->Subject = $note["post_title"];
                $mail->Body = implode("</p>\n<p>", explode("\n", $note["post_content"]));
                $mail->send();
            }
        }
        return [];
    }
    public function hourAction() {
        return [];
    }
    public function dayAction() {
        return [];
    }
}