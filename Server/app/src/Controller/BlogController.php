<?php
namespace Controller;

class BlogController extends Controller {

	public static $route = "blog";

	public function indexAction($page = 0) {
		return ["news"=>$this->DB->News->getNews(0,0,10, $page*10), "image_hash"=>rand(0,100)];
	}
	public function dispatchWithParam($action, $param)
    {
        if(method_exists($this, "{$action}Action"))
            return parent::dispatchWithParam($action, $param);
        $sections = $this->DB->News->getAllSections();
		$section_id = 0;
        foreach($sections as $s)
            if($s["url"]==$action)
                $section = $s;
        if(!isset($section))
            return 404;
        if($param!="write")
            return $this->present("blog", $this->blog_section($section, $param));
        else
            return $this->present("write", $this->writeAction($section["id"]));
    }


    public function dispatch($action)
    {
        if(method_exists($this, "{$action}Action"))
            return parent::dispatch($action);
        $sections = $this->DB->News->getAllSections();
        foreach($sections as $s)
            if($s["url"]==$action)
                $section = $s;
        if(!isset($section))
            return 404;
        return $this->present("blog", $this->blog_section($section, 1));
    }

    public function blog_section($section, $page=1) {
        $app = app();
	    $notes = $this->DB->News->getNews($section["id"],0,10, ($page-1)*10);
        return ["news"=>$notes,
            "title"=>$section["title"],
            "section_folder"=>$section["url"],
            "base_url" => $app["url"]("/".$this::$route."/".$section["url"]),
            "image_hash"=>$notes[0]["note_title"]];
    }

    public function noteAction($id=0) {
        $app = app();
	    $post = app()->request()->post();
	    if(count($post)>0) {
	        $this->DB->News->addComment($post["id_note"], $post["author"], $post["content"]);
        }
	    $note = $this->DB->News->getNote($id);
	    if($note==null)
	        return 404;
        return ["note"=>$note,
            "title"=>$note["sections"][0]["title"],
            "section_folder"=>$note["sections"][0]["url"],
            "base_url" => $app["url"]("/".$this::$route."/".$note["sections"][0]["url"]),
            "image_hash"=>$note["title"]];
    }

    public function privacyAction() {
	    return [];
    }
    public function writeAction() {
	    return [];
    }
}