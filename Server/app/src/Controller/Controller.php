<?php
namespace Controller;

abstract class Controller {

	protected $DB;

	public function __construct($DB) {
		if(strlen($this::$route)<1)
			throw new \Exception("Wrongly configured controller");
		$this->DB = $DB;
	}
	public static $route = "";
    public function dispatch($action) {
        $method = $action."Action";
        if(method_exists($this, $method)) {
            $data = $this->$method();
            return $this->present($action, $data);
        } else {
            return 404;
        }
    }
    public function dispatchWithParam($action, $param) {
        $method = $action."Action";
        if(method_exists($this, $method)) {
            $data = $this->$method($param);
            return $this->present($action, $data);
        } else {
            return 404;
        }
    }
	public function present($action, $data) {
        $app = app();
        if(!is_array($data))
            return $data;
        if(!isset($data["title"]))
            $data["title"] = t("title_".$this::$route."_".$action);
        if(!isset($data["base_url"]))
            $data["base_url"] = $app["url"]("/".$this::$route."/index");
        if(!is_file("app/templates/".$this::$route."/".$action.".html.php"))
            return $data;
        $resp = app()->template($this::$route."/".$action, $data)->layout("base");
        return $resp;
    }
}