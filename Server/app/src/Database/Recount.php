<?php

namespace Database;

use Model\RecountTextGenerator;

class Recount extends Database {

    private $valid_urls = array("api/login", "api/registration", "api/turn");
    /** @var Kingdom */
    private $Kingdom;
    /** @var Turn */
    private $Turn;
    /** @var Social */
    private $Social;

    function __construct() {
        $this->Kingdom = (new DbWrapper())->Kingdom;
        $this->Turn = (new DbWrapper())->Turn;
        $this->Social = (new DbWrapper())->Social;
        parent::__construct();
    }

    public function recount($url) {
        if(!in_array($url, $this->valid_urls))
            return false;
        $result = $this->getConfig(array("recount.time", "recount.in_progress"));
        if($result["recount.in_progress"]=="yes")
            return true;
        $dt1 = strtotime($result["recount.time"]);
        $dt2 = strtotime("now");
        if(($dt2-$dt1)/3600>23) {
            $this->updateConfig("recount.in_progress", "yes");
            $cmd = new \Model\PhpCommand();
            $cmd->recount();
            return true;
        }
        return false;
    }
        
    public function executeRecount() {
        $this->beforeRecount();
        $this->zeroOutTurns();
        $this->updateBuildings();
        $this->executeAttacks();
        $this->spawnBots();
        $this->executeBotActions();
        $this->despawnBots();
        $this->updateSolarSystems();
        $this->afterRecount();
    }


    private function zeroOutTurns() {

        $sql = "UPDATE kingdoms SET turn = 0";
        $this->execute($sql, array());
    }

    private function beforeRecount() {
        $cmd = new \Model\PhpCommand();
        $cmd->dump_db($this->database["address"], $this->database["user"], $this->database["password"]);

    }
    private function afterRecount() {
        $this->updateConfig("recount.in_progress", "no");
        $this->updateConfig("recount.time", date("Y-m-d H:i:s"));
    }

    private function updateBuildings() {

        $sql = "SELECT buildingtypes.*, kingdoms.id as iduser FROM `buildingtypes` JOIN `kingdoms` WHERE buildingtypes.id NOT IN (SELECT buildings.idtype FROM buildings WHERE buildings.idkingdom=kingdoms.id)";
        $res = $this->execute($sql, array());
        foreach($res as $r) {
            $this->execute("INSERT INTO buildings (idkingdom, idtype, number, employment, advancement) VALUES (?, ?, ?, ?, ?)", 
                array($r["iduser"], $r['id'], $r['number'], $r['employment'], $r['advancement']));
        }
    }

    private function executeAttacks() {
        $solar_systems = $this->generateSolarSystemData();
        $attacks = $this->generateAttacksData();
        $kingdoms_data = $this->generateKingdomsData($attacks);
        $land_balance = [];
        $TextGen = new RecountTextGenerator();
        foreach($attacks["Attacks"] as $idkingdom=>$attacks_list) {
            $defender = &$kingdoms_data[$idkingdom];
            foreach ($attacks_list as $key=>$attack) {
                if (!$this->attackValid($attack, $solar_systems, $defender))
                    unset($attacks["Attacks"][$idkingdom][$key]); // if attack is invalid, remove
            }
            if(empty($attacks["Attacks"][$idkingdom]))
                unset($attacks["Attacks"][$idkingdom]);
        }
        foreach($attacks["Attacks"] as $idkingdom=>&$attacks_list) {
            $attackers = array();
            foreach($attacks_list as $attack) {
                $attacker =& $kingdoms_data[$attack["idkingdom"]];
                $attackers[$attack["idkingdom"]] = &$attacker;
            }
            echo "Attack on $idkingdom<BR>";
            $defender = &$kingdoms_data[$idkingdom];
            $defender["idkingdom"] = $defender["Info"]["id"];
            $defenders = [&$defender];
            $force_defense = $this->calculateForce($defenders);
            $force_attack = $this->calculateForce($attacks_list);
            echo "Attacking forces: $force_attack<BR>";
            echo "Defending forces: $force_defense<BR>";
            $attack_now = $force_attack;
            $defense_now = $force_defense;
            $i = 0;
            $turn_data = [];
            while($i<20 && $attack_now>$force_attack*0.6 && $defense_now>$force_defense*0.5 && $force_attack/10<$force_defense) {
                echo "Turn $i<BR>";
                if($i%2==0)
                    echo "Defenders attack<BR>";
                else
                    echo "Attackers attack<BR>";
                if($i%2==0)
                    $this->resolveTurn($defenders, $attacks_list, $i, $turn_data);
                else
                    $this->resolveTurn($attacks_list, $defenders, $i, $turn_data);
                $attack_now = $this->calculateForce($attacks_list);
                $defense_now = $this->calculateForce($defenders);
                echo "Attacking forces: $attack_now<BR>";
                echo "Defending forces: $defense_now<BR>";
                $i++;
            } // interruption means someone won
            $defense_success = ($i<20&&$defense_now>$force_defense*0.5 && $force_attack/10<$force_defense)?1:(($i==20)?0:-1);
            $land = 0;
            $share = 0;
            if($i<20) {
                if($defense_success==-1) {
                    if($i == 0)
                        $i = 1;
                    $land = $defender["Resources"]["Land"]*0.01*(30/($i+2));
                    $land_balance[$idkingdom] -=$land;
                    $share = $land/count($attacks_list);
                    foreach ($attacks_list as $attack) {
                        $land_balance[$attack["idkingdom"]] += $share;
                    }
                }
            }
            foreach($defenders as &$def) {
                if($def==$defender && $land>0)
                    $changes = ["Land"=>(-$land)];
                else
                    $changes = [];
                $def["Messages"][] = $TextGen->generateAttackMessage($attackers, $defenders, $def, $defense_success, $i, $changes, $turn_data);
            }
            foreach($attackers as &$attacker) {
                if($land>0)
                    $changes = ["Land"=>$share];
                $attacker["Messages"][] = $TextGen->generateAttackMessage($attackers, $defenders, $attacker, $defense_success*(-1), $i, $changes, $turn_data);
            }
        }
        // foreach attack, return soldiers to their homes, distribute land
        foreach($kingdoms_data as &$kingdom) {
            $land_used = 0;
            $change = 0;
            foreach($kingdom["Soldiers"] as $type=>$row) {
                $change += $row["Number"] - $kingdom["Buildings"][$type]["Employment"];
                $kingdom["Buildings"][$type]["Employment"] = $row["Number"];
            }
            foreach($kingdom["AttacksSent"] as $attack) {
                foreach($attack["Soldiers"] as $type=>$row) {
                    $kingdom["Buildings"][$type]["Employment"] += $row["Number"];
                    $change += $row["Number"];
                }
            }
            foreach($kingdom["Buildings"] as $row) {
                $land_used += $row["Number"]*$kingdom["Rules"]["max_per_object"]["Building"]["Land"];
            }
            $kingdom["Resources"]["Land"] +=$land_balance[$kingdom["Info"]["id"]];
            $kingdom["Resources"]["People"] += $change;
            if($land_used>$kingdom["Resources"]["Land"])
                $this->demolishBuildings($kingdom, $land_used - $kingdom["Resources"]["Land"]);
        }
        $this->resolveChangesToDatabase($kingdoms_data);
    }

    private function resolveChangesToDatabase($kingdoms_data) {
        foreach($kingdoms_data as $idkingdom=>$kingdom) {
            foreach($kingdom["Buildings"] as $building) {
                $this->execute("UPDATE buildings 
                      JOIN buildings_cache ON buildings_cache.idtype=buildings.idtype && buildings.idkingdom=buildings_cache.idkingdom
                      SET buildings.number = ?, buildings.employment = ?, buildings_cache.number = ? - buildings.number, buildings_cache.employment = ? - buildings.employment
                      WHERE buildings.idtype IN (SELECT id FROM buildingtypes WHERE name=? AND race = ?) && buildings.idkingdom = ?",
                    array($building["Number"], $building["Employment"], $building["Number"], $building["Employment"],
                        $building["Type"], $kingdom["Info"]["idrace"], $kingdom["Info"]["id"]));
            }
            foreach($kingdom["Resources"] as $type=>$res) {
                $this->execute("UPDATE resources 
                      JOIN resources_cache ON resources_cache.idresource=resources.idresource && resources.idkingdom=resources_cache.idkingdom
                      SET resources.number = ?, resources_cache.number = ? - resources.number
                      WHERE resources.idresource IN (SELECT id FROM resourcetypes WHERE name=? AND race = ?) && resources.idkingdom = ?",
                    array($res, $res,
                        $type, $kingdom["Info"]["idrace"], $kingdom["Info"]["id"]));
            }
            foreach($kingdom["Messages"] as $message) {
                $this->execute("INSERT INTO war_reports (idkingdom, message, title) VALUES (?, ?, ?)", [$kingdom["Info"]["id"], $message["message"], $message["title"]]);
            }
            $this->execute("UPDATE kingdoms SET land = ?, people = ?, day_old = day_old + 1 WHERE id = ?", [$kingdom["Resources"]["Land"], $kingdom["Resources"]["People"], $idkingdom]);
        }
        $this->execute("DELETE FROM soldiers", []);
        $this->execute("DELETE FROM battles", []);
    }

    private function demolishBuildings(&$kingdom, $buildings) {
        $sum = 0;
        foreach($kingdom["Buildings"] as $building) {
            $sum += $building["Number"];
        }
        $rest = 0;
        foreach($kingdom["Buildings"] as $type=>$building) {
            $result = $buildings*$building["Number"]/$sum + $rest;
            $floor = floor($result);
            $rest = $result - $floor;
            $kingdom["Buildings"][$type]["Number"] -= $floor;
            $max_empl = $kingdom["Buildings"][$type]["Number"]*$kingdom["Rules"]["other"]["employees_per_building"];
            if($max_empl<$kingdom["Buildings"][$type]["Employment"])
                $kingdom["Buildings"][$type]["Employment"] = $max_empl;
        }
    }

    private function calculateForce($list) {
        $sum = 0;
        foreach($list as $force) {
            foreach($force["Soldiers"] as $soldiers) {
                $sum += $soldiers["Number"];
            }
        }
        return $sum;
    }

    private function calculateAttack($attacks, $turn = 0)
    {
        $sum = 0;
        foreach ($attacks as $attack) {
            foreach ($attack["Soldiers"] as $type => $row) {
                if ($row["turn_of_attack"] <= $turn)
                    $sum += $row["Number"] * $row["Advancement"] * $row["attack"];
            }
        }
        return $sum;
    }

    private function resolveTurn(&$attackers, &$defenders, $turn_num, &$turn_data) {
        $force_sum = $this->calculateAttack($attackers, $turn_num);
        $num_sum = $this->calculateForce($defenders);
        $damage_per_soldier = $force_sum/$num_sum*(1+random()*0.3);
        $turn_data[$turn_num]["force_sum"] = $force_sum;
        $turn_data[$turn_num]["num_sum"] = $num_sum;
        foreach($defenders as &$defender) {
            foreach($defender["Soldiers"] as &$soldier) {
                echo "damage per soldier: $damage_per_soldier<BR>";
                $soldier["sum_health"] -= $damage_per_soldier*$soldier["Number"];
                if($soldier["sum_health"]<0)
                    $soldier["sum_health"] = 0;
                $fallen = $soldier["Number"] - ceil($soldier["sum_health"]/$soldier["health"]);
                $turn_data[$turn_num]["lost"][$defender["idkingdom"]][$soldier["Type"]] = $fallen;
                echo $soldier["Type"].": $fallen died<BR>";
                $soldier["Number"] = ceil($soldier["sum_health"]/$soldier["health"]); // 1hp = alive
                if($soldier["Number"]<0) {
                    $soldier["Number"] = 0;
                    $soldier["sum_health"] = 0;
                }
            }
        }
    }

    private function generateKingdomsData($attacks) {
        $kingdoms = [];
        $buildings = $this->Kingdom->getAllBuildings();
        $resources = $this->Kingdom->getAllResources();
        $info = $this->Kingdom->getAllInfo();
        $rules = $this->Kingdom->getAllRules();
        foreach($info as $idkingdom=>$i) {
            $kingdoms[$idkingdom]["Info"] = $i;
            $kingdoms[$idkingdom]["Buildings"] = $buildings[$idkingdom];
            $kingdoms[$idkingdom]["Resources"] = $resources[$idkingdom];
            $kingdoms[$idkingdom]["Attacks"] = $attacks["Attacks"][$idkingdom];
            $kingdoms[$idkingdom]["AttacksSent"] = $attacks["AttacksSent"][$idkingdom];
            $kingdoms[$idkingdom]["Rules"] = $rules[$i["idrace"]];
            $r = $rules[$i["idrace"]];
            foreach($kingdoms[$idkingdom]["Buildings"] as $type => $row) {
                if(isset($r["war"]["health"][$type]))
                    $kingdoms[$idkingdom]["Soldiers"][$type] = [
                        "Type" => $type,
                        "Number" => $row["Employment"],
                        "Advancement" => $row["Advancement"],
                        "turn_of_attack" => $r["war"]["turn_attack"][$type],
                        "attack" => $r["war"]["attack"][$type],
                        "health" => $r["war"]["health"][$type],
                        "sum_health" => $row["Employment"]*$r["war"]["health"][$type]
                    ];
            }
            foreach($kingdoms[$idkingdom]["AttacksSent"] as &$attack) {
                foreach($attack["Soldiers"] as $type=>$soldiers) {
                    $kingdoms[$idkingdom]["Soldiers"][$type]["Number"] -= $soldiers["Number"];
                    $kingdoms[$idkingdom]["Soldiers"][$type]["sum_health"] -= $soldiers["Number"]*$r["war"]["health"][$type];
                    $attack["Soldiers"][$type]["Advancement"] = $kingdoms[$idkingdom]["Buildings"][$type]["Advancement"];
                    $attack["Soldiers"][$type]["turn_of_attack"] = $r["war"]["turn_attack"][$type];
                    $attack["Soldiers"][$type]["attack"] = $r["war"]["attack"][$type];
                    $attack["Soldiers"][$type]["health"] = $r["war"]["health"][$type];
                    $attack["Soldiers"][$type]["sum_health"] = $r["war"]["health"][$type]*$soldiers["Number"];
                }
            }
        }
        return $kingdoms;
    }

    private function attackValid($attack, $solar_systems, $defender) {
        foreach($solar_systems as $system)
            if(in_array($attack["idkingdom"], $system) || in_array($attack["idto"], $system) || in_array($attack["idfrom"], $system)) // at least one to find
                return ($defender["Info"]["day_old"]>0 || $defender["Info"]["iduser"]==0) && in_array($attack["idkingdom"], $system) && in_array($attack["idto"], $system) && in_array($attack["idfrom"], $system); // must be all
    }

    private function generateSolarSystemData() {
        $ss =  $this->query("SELECT id, id_solar_system FROM kingdoms");
        $solar_systems = [];
        foreach($ss as $s) {
            $solar_systems[$s["id_solar_system"]][] = $s["id"];
        }
        return $solar_systems;
    }

    private function generateAttacksData() {
        $attacks = $this->query("SELECT * FROM battles");
        $soldiers = $this->query("SELECT idbattle, buildingtypes.name as `Type`, soldiers.number as `Number`, soldiers.advancement as `Advancement` FROM soldiers LEFT JOIN buildingtypes ON soldiers.type=buildingtypes.id");
        $result = [];
        $all = [];
        foreach($attacks as $key=>&$att) {
            $all[$att["id"]] = &$att;
            $result["Attacks"][$att["idto"]][] = &$att;
            $result["AttacksSent"][$att["idkingdom"]][] = &$att;
        }
        foreach($soldiers as $res) {
            $all[$res["idbattle"]]["Soldiers"][$res["Type"]] = $res;
        }
        return $result;
    }

    private function probabilityBasedOnPopulation($pop) {
        if($pop<=1)
            return 1;
        if($pop==2)
            return 0.15;
        return 0.02*(pow($pop,0.5)) + 0.03;
    }

    private function spawnBots() {
        error_log("Spawn bots");
        $solar_systems = $this->Kingdom->getSolarSystems();
        $solar_systems = array_slice($solar_systems, 2);
        foreach($solar_systems as $ss) {
            if(random()<$this->probabilityBasedOnPopulation($ss["number_of_players"])) {
                $this->spawnBot($ss);
            }
        }
    }

    private function spawnBot($solar_system) {
        error_log("Spawning bot to system ".$solar_system["name"]);
        $n = t("bot_kingdoms", $solar_system["lang"]);
        $name = $n["attribute"][rand(0, count($n["attribute"])-1)] ." ". $n["base"][rand(0, count($n["base"])-1)];
        $this->Kingdom->createNewKingdom($solar_system["id_solar_system"], 0, 1, $name, $solar_system["lang"], $solar_system["average_size"]);
    }

    private function despawnBots() {}

    private function executeBotActions()
    {
        $kingdoms = $this->generateKingdomsData([]);
        foreach($kingdoms as &$kingdom) {
            if($kingdom["Info"]["iduser"]==0) {
                $this->botActions($kingdom, $kingdoms);
            }
        }
    }

    private function max_buyland_per_soil($land, $gold, $price_per_soil_for_soil) {
        $x = $price_per_soil_for_soil;
        $sx = sqrt($x);
        if($gold<=0)
            return 0;
        $N = (sqrt(8*$gold + 4*$land*$land*$x + 4*$land*$x + $x) - 2*$land*$sx - $sx)/2/$sx;
        return $N;
    }

    private function generatePerfectKingdom($kingdom) { // $kingdom as a reference
        $people = $kingdom["Resources"]["People"];
        $land = $kingdom["Resources"]["Land"];
        $popularity = $kingdom["Resources"]["Happiness"]*0.001;
        $epb = $kingdom["Rules"]["other"]["employees_per_building"];
        $opt = $kingdom["Rules"]["happiness"]["other"]["employment_optimum"];
        $lconst = $kingdom["Rules"]["happiness"]["other"]["people_land_constant"];
        $house_houses = $kingdom["Rules"]["happiness"]["other"]["house_houses"];
        $opt_houses = $land * (1 - $opt*$lconst/$epb) / (1 + $opt * $popularity * $house_houses / $epb);
        $employees = $people*$opt;
        $farms = $people*$kingdom["Rules"]["turn_cost"]["Person"]["Food"]/$kingdom["Rules"]["employees_earn"]["Field"]["Food"]/$epb;
        $goldmines = $employees/$kingdom["Rules"]["turn_cost"]["Employee"]["Gold"]/$epb;
        $universities = 0.1*$people/$epb;
        $builders = 0.05*$people/$epb; // also brickmakers and forges
        $rest = $land - ($opt_houses + $farms + $goldmines + $universities + $builders*3);
        $goldmines += $rest*0.4 + $rest * 2.0/(5+$kingdom["Info"]["day_old"]);
        $war = $rest/2 * (1 - 0.4 - 2.0/($kingdom["Info"]["day_old"]+5));
        $empl_calc = ($rest + $farms + $goldmines + $universities + $builders*3)*$epb;
        $mult = 1;
        if($employees<$empl_calc)
            $mult = $employees/$empl_calc;
        $buildings = array(
            array("Type"=>"House", "Number"=>$opt_houses, "Employment"=>0),
            array("Type"=>"Field", "Number"=>$farms, "Employment"=>$farms*$epb*$mult),
            array("Type"=>"Goldmine", "Number"=>$goldmines, "Employment"=>$goldmines*$epb*$mult),
            array("Type"=>"MasonsWorkshop", "Number"=>$builders, "Employment"=>$builders*$epb*$mult),
            array("Type"=>"BuildersGuild", "Number"=>$builders, "Employment"=>$builders*$epb*$mult),
            array("Type"=>"University", "Number"=>$universities, "Employment"=>$farms*$epb*$mult),
            array("Type"=>"Armory", "Number"=>$builders, "Employment"=>$builders*$epb*$mult),
            array("Type"=>"Barracks", "Number"=>$war, "Employment"=>$war*$epb*$mult),
            array("Type"=>"ArcherHouse", "Number"=>$war, "Employment"=>$war*$epb*$mult),
        );
        $new_soil = 0.1*$this->max_buyland_per_soil($kingdom["Resources"]["Land"], $kingdom["Resources"]["Gold"], $kingdom["Rules"]["other"]["price_for_soil_per_soil"]);
        $kingdom["Resources"]["Land"] += $new_soil;
        $new_kingdom = array(
            "Buildings" => $buildings,
            "Resources" => $kingdom["Resources"] // just in cases
        );
        return $new_kingdom;
    }

    private function generateDifferences($kingdom, $perfect_kingdom, $interpolation = 1.0) {
        $differences = [];
        foreach($kingdom["Buildings"] as $b1) {
            foreach($perfect_kingdom["Buildings"] as $b2) {
                if($b1["Type"]==$b2["Type"])
                    $differences["Buildings"][] = array(
                        "Type"=>$b1["Type"],
                        "Number"=>$interpolation*($b2["Number"]-$b1["Number"]),
                        "Employment"=>$interpolation*($b2["Employment"]-$b1["Employment"]),
                        "Advancement"=>$interpolation*($b2["Advancement"]-$b1["Advancement"])); // just in cases
            }
        }
        foreach($kingdom["Resources"] as $key=>$val) {// just in cases
            $val2 = $perfect_kingdom["Resources"][$key];
            $differences["Resources"][$key] = ($val2 - $val)*$interpolation;
        }
        return $differences;
    }

    private function updateKingdom(&$kingdom, $differences) {
        foreach($kingdom["Buildings"] as &$b1) {
            foreach($differences["Buildings"] as $b2) {
                if($b1["Type"]==$b2["Type"]) {
                    $b1["Number"] += $b2["Number"];
                    $b1["Employment"] += $b2["Employment"];
                    $b1["Advancement"] += $b2["Advancement"];
                }
            }
        }
        foreach($kingdom["Resources"] as $key=>$val) {
            $kingdom["Resources"][$key] += $differences["Resources"][$key];
        }

    }

    private function findInterpolation($kingdom, $perfect_kingdom) {
        $min = 0;
        $max = 1;
        $i = 0;
        $differences = $this->generateDifferences($kingdom, $perfect_kingdom, 1);
        $copy = $kingdom;
        $result = $this->Turn->executeTurn($copy, $differences, $kingdom["Rules"]);
        if($result==false)
            return $differences;
        $working_differences = null;
        do {
            $avg = ($min + $max)*0.5;
            $differences = $this->generateDifferences($kingdom, $perfect_kingdom, $avg);
            $result = $this->Turn->executeTurn($kingdom, $differences, $kingdom["Rules"]);
            if($result)
                $max = $avg; // not working - lower the bar
            else {
                $min = $avg; // working - higher bar
                $working_differences = $differences;
            }
            $i++;
        } while($i<10);
        if($working_differences==null) {
            $working_differences = $this->generateDifferences($kingdom, $kingdom, 1); // generate zeroes;
            $this->Turn->executeTurn($kingdom, $working_differences, $kingdom["Rules"]);
        }
        return $working_differences;
    }

    private function botActions(&$kingdom, $all_kingdoms)
    {
        $copy = $kingdom;
        for($i=0; $i<$kingdom["Rules"]["other"]["num_turns_per_recount"]; $i++) {
            $perfect_kingdom = $this->generatePerfectKingdom($copy);
            $diff = $this->findInterpolation($copy, $perfect_kingdom);
            $this->updateKingdom($copy, $diff);
        }
        $diff = $this->generateDifferences($kingdom, $copy, 1);
        $diff["Id"] = $kingdom["Info"]["id"];
        $this->Turn->updateWithTurn($diff);
        $this->sendAttack($copy, $all_kingdoms);

    }
    public function addBattle($kingdomid, $battle) {
        $this->execute("INSERT INTO battles (idkingdom, idfrom, idto, sent) VALUES (?, ?, ?, ?)",
            array($kingdomid, $battle["idfrom"], $battle["idto"], $battle["sent"]));
        $battleid = $this->lastInsertId();
        foreach($battle["soldiers"] as $soldiers) {
            $this->execute("INSERT INTO soldiers (idbattle, type, number, advancement) VALUES (?, (SELECT id FROM buildingtypes WHERE name=?), ?, ?)",
                array($battleid, $soldiers["building"], $soldiers["number"], $soldiers["advancement"]));
        }
        $battle["id"] = $battleid;
        return $battle;
    }

    private function sendAttack($bot, $kingdoms) {
        $ss = [];
        foreach($kingdoms as $kingdom) {
            if($kingdom["Info"]["id_solar_system"] == $bot["Info"]["id_solar_system"])
                $ss[] = $kingdom;
        }
        $target = $ss[rand(0, count($ss)-1)];
        if($target["Info"]["id"]==$bot["Info"]["id"])
            return; // exit early to avoid confusion
        $prop = 0.2 + random()*0.6;
        $prop2 = 0.2 + random()*0.6;
        $attack = array(
            "idkingdom"=>$bot["Info"]["id"],
            "idfrom"=>$bot["Info"]["id"],
            "idto"=>$target["Info"]["id"],
            "sent" => true,
            "soldiers" => array(
                array(
                    "building"=>"Barracks",
                    "number" => 0,
                    "advancement" => 0
                ),
                array(
                    "building"=>"ArcherHouse",
                    "number" => 0,
                    "advancement" => 0
                ),
            )
        );
        foreach($bot["Buildings"] as $b) {
            if($b["Type"]=="Barracks")
                $attack["soldiers"][0]["number"] = ($prop+$prop2)/2*$b["Employment"];
            if($b["Type"]=="ArcherHouse")
                $attack["soldiers"][1]["number"] = ($prop + (1-$prop2))/2*$b["Employment"];
        }
        $this->Social->addBattle($bot["Info"]["id"], $attack);
    }


    private function updateSolarSystems()
    {
        $this->Kingdom->updateSolarSystems();
    }

}