<?php

namespace Database;

class Kingdom extends Database {
	public function register($user, $kingdom, $auth, $lang, $id_solar_system, $idrace) {
        $client = new \Google_Client(['client_id' => $GLOBALS["GOOGLE_CLIENT"]]);
        try {
        	$payload = $client->verifyIdToken($auth);
    	} catch (\Exception $e) {
            error_log($e->getMessage());
    		return -1;
    	}
        if(!$payload)
            return -1;
        $mail = $payload["email"];
        if(!$this->checkExists("users", "email", $mail)
            && $this->checkExists("users", "name", $user)
            || $this->checkExists("kingdoms", "name", $kingdom)
            || !$this->checkExists("races", "id", $idrace))
        	return null;
        if(!$this->checkExists("solar_systems", "id_solar_system", $id_solar_system)) {
            if($id_solar_system==0)
                $id_solar_system = $this->create_new_solar_system($lang);
            else
                $id_solar_system = $this->choose_best_solar_system($lang);
        }
        if(!$this->checkExists("users", "email", $mail)) {
            $this->execute("INSERT INTO users (name, email) VALUES (?, ?)", array($user, $mail));
            $id = $this->lastInsertId();
        } else {
            $id = $this->getUserId($mail);
        }
		$kingdomid = $this->createNewKingdom($id_solar_system, $id, $idrace, $kingdom, $lang);
        $this->updateSolarSystems();
		return $this->getKingdom($kingdomid);
	}

	public function createNewKingdom($id_solar_system, $user_id, $idrace, $kingdom_name, $lang, $land = null) {
        $id = $user_id;
        $this->execute("INSERT INTO kingdoms (name, iduser, id_solar_system, idrace, lang, turn_message) VALUES (?, ?, ?, ?, ?, ?)", array($kingdom_name, $id, $id_solar_system, $idrace, $lang, t("turn")["greeting"]));
        $kingdomid = $this->lastInsertId();
        $kingdom = $this->getDefaults($idrace, $land);
        foreach($kingdom["Resources"] as $name=>$r) {
            $this->execute("INSERT INTO resources (idkingdom, idresource, `number`) VALUES (?, ?, ?)",
                array($kingdomid, $r['id'], $r['Number']));
            $this->execute("INSERT INTO resources_cache (idkingdom, idresource, `number`) VALUES (?, ?, ?)",
                array($kingdomid, $r['id'], $r['Number']));
            if($name=="Land")
                $land = $r["defaults"];
        }
        $this->execute("UPDATE kingdoms SET land=? WHERE id=?", array($land, $kingdomid));
        foreach($kingdom["Buildings"] as $r) {
            $this->execute("INSERT INTO buildings (idkingdom, idtype, number, employment, advancement) VALUES (?, ?, ?, ?, ?)",
                array($kingdomid, $r['id'], $r['Number'], $r['Employment'], $r['Advancement']));
            $this->execute("INSERT INTO buildings_cache (idkingdom, idtype, number, employment, advancement) VALUES (?, ?, ?, ?, ?)",
                array($kingdomid, $r['id'], $r['Number'], $r['Employment'], $r['Advancement']));
        }
	    return $kingdomid;
    }


	public function checkExists($table, $key, $value) {
		$rows = $this->execute("SELECT COUNT(*) FROM $table WHERE $key=?", array($value));
		return $rows[0]["COUNT(*)"]>0;
	}

    public function getDefaults($idrace, $size=null) {
        static $new_kingdom;
        if($new_kingdom==null) {
            $new_kingdom = array();
            $new_kingdom["Buildings"] = $this->getDefaultBuildings($idrace);
            $new_kingdom["Resources"] = $this->getDefaultResources($idrace);
        }
        $kingdom = $new_kingdom;
        if($size==null)
            $multi = 1;
        else
            $multi = 1.0*$size/$kingdom["Resources"]["Land"]["Number"];
        foreach($kingdom["Buildings"] as &$building) {
            $building["Number"] *= $multi;
            $building["Employment"] *= $multi;
        }
        foreach($kingdom["Resources"] as &$resource) {
            if($resource["Type"]!="Happiness")
                continue;
            $resource["Number"] *= $multi;
        }
        return $kingdom;
    }
	public function getDefaultResources($idrace) {
	    $array = [];
        $Resources = $this->execute("SELECT resourcetypes.id, resourcetypes.name as `Type`, resourcetypes.defaults as `Number` FROM resourcetypes WHERE race=?", array($idrace));
        foreach($Resources as $row) {
            $array[$row["Type"]] = $row;
        }
        return $array;
	}

	public function getDefaultBuildings($idrace) {
        $array = $this->execute("SELECT buildingtypes.id, buildingtypes.name as `Type`, buildingtypes.number as `Number`, buildingtypes.employment as `Employment`, buildingtypes.advancement as `Advancement`
		FROM buildingtypes WHERE race=?", array($idrace));
        return $array;
	}

    public function create_new_solar_system($lang) {
        $names = t("solar_systems", $lang);
        $name = $names[rand(0, count($names)-1)];
        $this->execute("INSERT INTO solar_systems (`name`, lang, average_size, number_of_players) VALUES (?, ?, ?, ?)", [$name, $lang, 1000, 1]);
        $id = $this->lastInsertId();
        $this->execute("INSERT INTO chat_channels (`channelname`, channeldesc, id_solar_system) VALUES (?, ?, ?)", [$name, sprintf(t("chat_channel_desc", $lang), $name), $id]);
        return $id;
    }

    public function choose_best_solar_system($lang) {
        $solar_systems = $this->getSolarSystemsByLang($lang);
        $min_score = INF;
        error_log(print_r($solar_systems, true));
        foreach($solar_systems as $key=>$system) {
            if($system["population"]>15 || $system["average_size"]>2000)
                unset($solar_systems[$key]);
            $score = $system["average_size"]/10 + $system["population"]*10;
            if($score<$min_score)
                $min_score = $score;
            $solar_systems[$key]["score"] = $score;
        }
        error_log(print_r($solar_systems, true));
        foreach($solar_systems as $key=>$system) {
            if($system["score"]/$min_score >1.1)
                unset($solar_systems[$key]);
        }
        error_log(print_r($solar_systems, true));
        $cnt = count($solar_systems);
        if($cnt==0)
            return $this->create_new_solar_system($lang);
        $system = $solar_systems[rand(0, $cnt-1)];
        return $system["id_solar_system"];
    }

	public function getKingdomForAuth($auth, $kingdom_id=null) {
        $client = new \Google_Client(['client_id' => $GLOBALS["GOOGLE_CLIENT"]]);
        try {
        	$payload = $client->verifyIdToken($auth);
    	} catch (\Exception $e) {
            error_log($e->getMessage());
            return -1;
    	}
        if(!$payload)
            return -1;
		$id = $this->getKingdomId($payload["email"], $kingdom_id);
		$user_id = $this->getUserId($payload["email"]);
		if($id!=null)
                $kingdom = $this->getKingdom($id, $user_id);
        else
            return null;
		return $kingdom;
	}

	public function getUserId($mail) {
        $rows = $this->execute("SELECT id FROM users WHERE email = ?", array($mail));
        $id = $rows[0]["id"];
        return $id;
    }

	public function getKingdomId($mail, $kingdom_id=null) {
		if($kingdom_id==null)
			$ids = $this->execute("SELECT kingdoms.id FROM users, kingdoms WHERE users.email=? && (users.id=kingdoms.iduser || users.id=1)", array($mail));
		else
			$ids = $this->execute("SELECT kingdoms.id FROM users, kingdoms WHERE users.email=? && (users.id=kingdoms.iduser || users.id=1) && kingdoms.id=?", array($mail, $kingdom_id));
		if(count($ids)==0)
                    return;
        return $ids[0]['id'];
	}

	public function updateKingdom($turn) {
		if($turn["Recount"]!= $this->getConfig("recount.time"))
			return false;
		$kingdomid = $turn['Id'];
		$turns = $turn['Turns'];
		$sql = "UPDATE kingdoms SET turn = turn + 1 WHERE id=?";
		$this->execute($sql, array($turns, $kingdomid));
		foreach($turn['Buildings'] as $b) {
			$sql = "UPDATE buildings, buildingtypes SET buildings.number = buildings.number + ?,
			buildings.employment = buildings.employment + ?, buildings.advancement = buildings.advancement + ? 
			WHERE buildingtypes.name=? AND buildingtypes.id=buildings.idtype AND buildings.idkingdom=?";
			$params = array($b['Number'], $b['Employment'], $b['Advancement'], $b['Type'], $kingdomid);
			$this->execute($sql, $params);
			$sql = "UPDATE buildings_cache, buildingtypes SET buildings_cache.number =  ?,
			buildings_cache.employment =  ?, buildings_cache.advancement =  ? 
			WHERE buildingtypes.name=? AND buildingtypes.id=buildings_cache.idtype AND buildings_cache.idkingdom=?";
			$params = array($b['Number'], $b['Employment'], $b['Advancement'], $b['Type'], $kingdomid);
			$this->execute($sql, $params);
		}
		foreach($turn['Resources'] as $key=>$val) {
			$sql = "UPDATE resources, resourcetypes SET resources.number = resources.number + ?
			WHERE resourcetypes.name=? AND resourcetypes.id=resources.idresource AND resources.idkingdom=?";
			$params = array($val, $key, $kingdomid);
			$this->execute($sql, $params);
			$sql = "UPDATE resources_cache, resourcetypes SET resources_cache.number =  ?
			WHERE resourcetypes.name=? AND resourcetypes.id=resources_cache.idresource AND resources_cache.idkingdom=?";
			$params = array($val, $key, $kingdomid);
			$this->execute($sql, $params);
		}
		return true;
	}

    public function getSolarSystems($lang="en") {
        $sql = $this->query("SELECT * FROM solar_systems");
        $sql[-2] = array(
            "id_solar_system"=>-1,
            "name"=>t("most_suited_for_you"),
            "lang"=>$lang,
            "number_of_players"=> "?",
            "average_size"=> "?"
        );
        $sql[-1] = array(
            "id_solar_system"=>0,
            "name"=>t("create_new_ss"),
            "lang"=>$lang,
            "number_of_players"=> "?",
            "average_size"=> "?"
        );
        ksort($sql);
        return array_values($sql);
    }
    public function getSolarSystemsByLang($lang="en") {
        $sql = $this->execute("SELECT * FROM solar_systems WHERE lang=?", [$lang]);
        return $sql;
    }

    public function getRaces($lang='en') {
        $sql = $this->query("SELECT * FROM races");
        foreach($sql as $key=>$row) {
            $sql[$key]["name"] = t("races")[$row["id"]];
        }
        return $sql;
    }
/*
Podsumowanie:
{
"Username": "user name","KingdomName": "kingdom name","Land": 1050,"People": 150,"Turns": 7,
"Buildings": [
    {"Type": "House","Number": 30,"Employment": 90,"Advancement": 0.7},
    {"Type": "Field","Number": 31,"Employment": 91,"Advancement": 0.71},
    tu reszta],
"Resources": {
 "Happiness": 75,"Gold": 1000,"Food": 1000,"Luxury": 0,"Bricks": 100,"Armament": 0}
}
*/
	public function getKingdom($kingdomid, $user_id = 0) {
		$result = $this->getKingdomSimple($kingdomid);
		if($result==null)
		    return null;
        if($user_id==0)
            $user_id =$result["Iduser"];
            $recount = $this->getConfig("recount.time");
        $result['Recount'] = $recount;
        $result['Kingdoms'] = $this->getAvailableKingdoms($user_id);
        $result["Changes"] = $this->getLastTurn($kingdomid);
        $result["Reports"] = $this->getWarReports($kingdomid);
        $result["Rules"] = $this->getRulesFlat($result["Idrace"]);
        $result["Download"] = $this->getDownload();
        return $result;
	}

	public function getKingdomSimple($kingdomid) {
        $req1 = $this->execute("SELECT kingdoms.id, kingdoms.iduser, kingdoms.idrace, users.email, users.name as username, 
			kingdoms.name as kingdomname, kingdoms.land, kingdoms.people, kingdoms.turn, kingdoms.turn_message  
			FROM users, kingdoms WHERE kingdoms.id=? && (users.id=kingdoms.iduser || users.id=1)", array($kingdomid));
        if(count($req1)>0) {
            $req1 = $req1[0];
            $result["KingdomId"] = $req1["id"];
            $result["Idrace"] = $req1["idrace"];
            $result["Iduser"] = $req1["iduser"];
            $result['Username'] = $req1['email'];
            $result['KingdomName'] = $req1['kingdomname'];
            $result['People'] = $req1['people'];
            $result['Turns'] = $req1['turn'];
            $result['Land'] = $req1['land'];
            $result["Days_Old"] = $req1["day_old"];
            $result['Turn_Message'] = $req1['turn_message'];
            $result['Buildings'] = $this->getBuildings($kingdomid);
            $result['Resources'] = $this->getResources($kingdomid);
        }
        return $result;
    }

    public function getAllRules() {
	    $sql = $this->query("SELECT id FROM races");
	    $rules = [];
	    foreach($sql as $row)
	        $rules[$row["id"]] = $this->getRules($row["id"]);
	    return $rules;
    }

	public function getRules($idrace) {
		$rules = $this->getRulesFlat($idrace);
		$rule_set = [];
		foreach($rules as $row) {
			if(strlen($row["subcategory"])>0)
				$rule_set[$row["category"]][$row["subcategory"]][$row["constant"]] = $row["value"];
			else
				$rule_set[$row["category"]][$row["constant"]] = $row["value"];
		}
		return $rule_set;
	}

	public function getRulesFlat($idrace) {
		$rules = $this->execute("SELECT category, subcategory, constant, value from constants as C WHERE idrace=? || 
			(idrace=0 && NOT EXISTS 
				(SELECT 1 from constants as C2 WHERE idrace=? && C2.category=C.category && C2.constant=C.constant)
			)", array($idrace, $idrace));
		return $rules;
	}

    public function getDownload() {
        $files = scandir(BULLET_ROOT."/web/assets/music/");
        foreach($files as $key=>$value)
            if(substr($values, 0, 1)=="." || is_dir(BULLET_ROOT."/web/assets/music/".$value))
                unset($files[$key]);
        $only_files = array_values($files);
        foreach($only_files as $key=>$file) {
            $only_files[$key] = trim("http://".$_SERVER["HTTP_HOST"].BULLET_BASE_URL."/web/assets/music/".str_replace(" ", "%20", $file));
        }
        $download = array("Music"=>$only_files);

        $files = scandir(BULLET_ROOT."/web/assets/sounds/");
        foreach($files as $key=>$value)
            if(substr($values, 0, 1)=="." || is_dir(BULLET_ROOT."/web/assets/sounds/".$value))
                unset($files[$key]);
        $only_files = array_values($files);
        foreach($only_files as $key=>$file) {
            $only_files[$key] = trim("http://".$_SERVER["HTTP_HOST"].BULLET_BASE_URL."/web/assets/sounds/".str_replace(" ", "%20", $file));
        }
        $download["Sounds"] = $only_files;

        return $download;
    }


    public function getLastTurn($kingdomid) {
		$result = array();
			$result['Buildings'] = $this->getBuildingsCache($kingdomid);
			$result['Resources'] = $this->getResourcesCache($kingdomid);
			return $result;
	}

    public function getResources($kingdomid) {
        $req = $this->execute("SELECT * FROM resources, resourcetypes WHERE idkingdom=? 
			AND resourcetypes.id=resources.idresource", array($kingdomid));
        $resources = array();
        foreach($req as $row) {
            $resources[$row['name']] = $row['number'];
        }
        return $resources;
    }

    public function getBuildings($kingdomid) {
        $req = $this->execute("SELECT buildingtypes.name, buildings.number, buildings.employment, buildings.advancement
		FROM (SELECT * FROM buildings WHERE buildings.idkingdom=?) as buildings RIGHT JOIN buildingtypes ON buildingtypes.id=buildings.idtype", array($kingdomid));
        $buildings = array();
        foreach($req as $row) {
            $buildings[] = array("Type"=>$row['name'],
                "Number"=>$row['number'],
                "Employment"=>$row['employment'],
                "Advancement"=>$row['advancement']);
        }
        return $buildings;
    }

    public function getAllInfo() {
        $req = $this->query("SELECT kingdoms.*, users.name as `user` FROM kingdoms LEFT JOIN users ON users.id=kingdoms.iduser");
        $info = array();
        foreach($req as $row) {
            $info[$row["id"]] = $row;
        }
        return $info;
    }

    public function getAllResources() {
        $req = $this->query("SELECT idkingdom, `name`, `number` FROM resources LEFT JOIN resourcetypes ON resourcetypes.id=resources.idresource");
        $resources = array();
        foreach($req as $row) {
            $resources[$row["idkingdom"]][$row['name']] = $row['number'];
        }
        return $resources;
    }

    public function getAllBuildings() {
        $req = $this->query("SELECT buildings.idkingdom,  buildingtypes.name, buildings.number, buildings.employment, buildings.advancement
		FROM buildings LEFT JOIN buildingtypes ON buildingtypes.id=buildings.idtype");
        $buildings = array();
        foreach($req as $row) {
            $buildings[$row['idkingdom']][$row['name']] = array( // note to self: different variable schema
                "Type"=>$row['name'],
                "Number"=>$row['number'],
                "Employment"=>$row['employment'],
                "Advancement"=>$row['advancement']);
        }
        return $buildings;
    }

    public function updateSolarSystems() {
	    $land = $this->execute("SELECT resourcetypes.id FROM resourcetypes WHERE name = \"Land\"", [])[0]["id"];
	    $this->execute("UPDATE kingdoms LEFT JOIN 
	    (SELECT idkingdom, `number` FROM resources WHERE idresource=?) as resources 
	    ON resources.idkingdom=kingdoms.id
	    SET kingdoms.land = resources.number", [$land]);
        $this->execute("UPDATE solar_systems JOIN  
      (SELECT id_solar_system, AVG(land) as `avg`, COUNT(*) as `cnt` FROM kingdoms GROUP BY id_solar_system) as kingdoms_data 
        ON solar_systems.id_solar_system=kingdoms_data.id_solar_system 
        SET average_size=`avg`, number_of_players=`cnt`", []);
    }

	public function getResourcesCache($kingdomid) {
		$req = $this->execute("SELECT * FROM resources_cache, resourcetypes WHERE idkingdom=? 
			AND resourcetypes.id=resources_cache.idresource", array($kingdomid));
		$resources = array();
		foreach($req as $row) {
			$resources[$row['name']] = $row['number'];
		}
		return $resources;
	}

	public function getBuildingsCache($kingdomid) {
		$req = $this->execute("SELECT buildingtypes.name, buildings_cache.number, buildings_cache.employment, buildings_cache.advancement
		FROM (SELECT * FROM buildings_cache WHERE buildings_cache.idkingdom=?) as buildings_cache RIGHT JOIN buildingtypes ON buildingtypes.id=buildings_cache.idtype", array($kingdomid));
		$buildings = array();
		foreach($req as $row) {
			$buildings[] = array("Type"=>$row['name'], 
								"Number"=>$row['number'], 
								"Employment"=>$row['employment'], 
								"Advancement"=>$row['advancement']);
		}
		return $buildings;
	}


    private function getWarReports($kingdomid)
    {
        return $this->execute("SELECT * FROM war_reports WHERE idkingdom=? ORDER BY post_time DESC", array($kingdomid));
    }

    private function getAvailableKingdoms($Iduser)
    {
        $req1 = $this->execute("SELECT kingdoms.id, kingdoms.idrace, kingdoms.name, kingdoms.land, kingdoms.people, kingdoms.turn  
			FROM kingdoms WHERE kingdoms.iduser=? || 1=?", array($Iduser, $Iduser));
        return $req1;
    }

}
