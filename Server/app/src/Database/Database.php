<?php

namespace Database;

class Database {

    static private $mysqli;
    protected $database;

    public function __construct($which = 1) {
        require BULLET_APP_ROOT."config/database.php";
        if($which==1)
            $this->database = $database;
        else
            $this->database = $database2;
        if(self::$mysqli==null) {
            self::$mysqli = new \mysqli($this->database['address'], $this->database['user'],
                $this->database['password'], $this->database['dbname']);
            self::$mysqli->set_charset("utf8");
        }
        if (self::$mysqli->connect_error) 
            throw new \Exception('Connect Error (' . self::$mysqli->connect_errno . ') '
                .self::$mysqli->connect_error);
    }

    public function __destruct() {
        if(self::$mysqli!=null) {
            self::$mysqli->close();
            self::$mysqli = null;
        }
    }
    
    public function execute($sql, $params) {
        $prepare = self::$mysqli->prepare($sql);
        $par = array();
        if(!$prepare)
            throw new \Exception("You have an error in your query: ".$sql);
        $par[0] = $prepare;
        $par[1] = "";
        foreach($params as &$p) {
            $par[1] .=$this->getType($p);
            $help = 0;
            if($p==null)
                $par[] = &$help;
            else
                $par[] = &$p;
        }
        if(count($params)>0)
            call_user_func_array('mysqli_stmt_bind_param',$par);
        $exec = $prepare->execute();
        $q = $prepare->get_result();
        if($q) {
            $arr = array();
            while($row = $q->fetch_array(MYSQLI_ASSOC))
                $arr[] = $row;
            $prepare->close();
            return $arr;
        }
        return array();
    }
    
    public function query($sql) {
        $q = self::$mysqli->query($sql);
        if (self::$mysqli->connect_errno) {
            throw new \Exception("Connect failed: ". self::$mysqli->connect_error);
        }
        $arr = array();
        if($q) {
            while($row = $q->fetch_array(MYSQLI_ASSOC))
                $arr[] = $row;
            $q->free();
        }
        return $arr;
    }

    public function lastInsertId() {
        return self::$mysqli->insert_id;
    }
    
    private function getType($value) {
        if(is_array($value))
            throw new \Exception("Database error: Passed value of a wrong type");
        //if(strlen($value)>65535)
        //    return "b";
        if(is_string($value))
            return "s";
        if(is_int($value))
            return 'i';
        if(is_double($value))
            return "d";
        if(is_bool($value))
            return "i";
        if(is_null($value))
            return "i";
        else
            throw new \Exception("Database error: Passed value of a wrong type");
        
    }



    /**
     * Updates config in database
     * @param type $name
     * @param type $value
     */
    protected function updateConfig($name, $value) {
        $sql = "REPLACE INTO settings (key1, val) VALUES (?,?)";
        $params = array($name, $value);
        $this->execute($sql, $params);
    }
    
    public function getConfig($result) {
        
        if(is_array($result))
            foreach($result as $val)
                if(strpos($val, " ")!==false || strpos($val, "'")!==false || !is_string($val))
                    throw new \Exception ("no u dont");
        if(is_array($result))
            $arg = implode("','", $result);
        else
            $arg = $result;
        $sql = "SELECT key1, val FROM settings WHERE key1 IN ('$arg')";
        $query = $this->query($sql);
        if(count($query)==1)
            return $query[0]["val"];
        if(count($query)>1){
            $result = array();
            foreach($query as $row)
                $result[$row["key1"]] = $row["val"];
            return $result;
        }
    }

   
}