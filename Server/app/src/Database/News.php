<?php

namespace Database;

class News extends Database {

    public function __construct()
    {
        parent::__construct(2);
    }

    public function getNews($section_id = 0, $author_id=0, $limit = 5, $offset=0) {
        $sql = "SELECT * FROM blog_notes WHERE status='published'";
        $params = [];
        if($section_id>0) {
            $params[] = $section_id;
            $sql .= " AND `id_section` = ?";
        }
        if($author_id>0) {
            $params[] = $author_id;
            $sql .= " AND author = ?";
        }
        $sql .= " ORDER BY post_time DESC LIMIT ?";
        $params[] = $limit;
        $sql .= " OFFSET ?";
        $params[] = $offset;
        $res = $this->execute($sql, $params);
        $this->getAuthors($res);
        $this->getSections($res);
        return $res;
    }

    public function addComment($id_note, $author, $content) {
        if(strlen($author)<4 || strlen($content)>65535 || strlen(strip_tags($content))<10)
            return;
        if(!stristr("$content", "<script"))
            $this->execute("INSERT INTO blog_comments (id_note, author, content, post_time) VALUES (?, ?, ?, NOW())", array($id_note, strip_tags($author), $content));
        return;
    }

    public function getAllSections() {
        return $this->query("SELECT * FROM blog_sections");
    }

    private function getAuthors(&$res) {
        $authors = [];
        foreach($res as $key=>$row) {
            if(!array_key_exists($row["post_author"], $authors)) {
                $r = $this->execute("SELECT * FROM blog_authors WHERE `id_author` = ?", array($row["id_author"]))[0];
                $authors[$row["id_author"]] = $r["name"];
            }
            $res[$key]["author"] = $authors[$row["id_author"]];
        }
    }

    private function getSections(&$res) {
        $sections = [];
        foreach($res as $key=>$row) {
         $r = $this->execute("SELECT * FROM blog_sections WHERE id = ?", [$row["id_note"]]);
            $res[$key]["sections"] = $r;
        }
    }

    private function getComments(&$res) {
        foreach($res as $key=>$row) {
            $r = $this->execute("SELECT * FROM blog_comments WHERE id_note = ?", [$row["id_note"]]);
            $res[$key]["comments"] = $r;
        }
    }

    public function getNote($id_note) {
        $sql = "SELECT * FROM blog_notes WHERE `id_note`=?";
        $res = $this->execute($sql, [$id_note]);
        $this->getAuthors($res);
        $this->getSections($res);
        $this->getComments($res);
        return $res[0];
    }

    public function addToMailingList($email) {
        $this->execute("INSERT INTO mailing_list (email) VALUES (?)", array($email));
		$id = $this->lastInsertId();
		$this->execute("INSERT INTO mailing_preferences (mail_id, pref_id) VALUES(?, ?)", [$id, 1]);
    }
    public function removeFromMailingList($email) {
        $this->execute("DELETE FROM mailing_list WHERE email = ?", array($email));
    }

    public function getMailingList() {
        $res = $this->query("SELECT * FROM mailing_list");
        return $res;
    }

    public function getNotesToSend() {
        $res = $this->query("SELECT * FROM wp_posts WHERE post_status='publish' && post_type='post' && mail_queued=1");
        return $res;
    }
    public function updateNoteIsSent($id_note) {
        $this->execute("UPDATE wp_posts SET mail_queued=0 WHERE `ID`=?", [$id_note]);
    }

}