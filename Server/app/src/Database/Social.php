<?php

namespace Database;

class Social extends Database {

    public function grabChannels($idkingdom) {
        $array = $this->execute("SELECT * FROM chat_channels WHERE id_solar_system IN (SELECT id_solar_system FROM kingdoms WHERE id=?) OR id_solar_system=0", [$idkingdom]);
        return $array;
    }

    public function grabChannel($id, $idkingdom) {
        $array = $this->execute("SELECT * FROM chat_channels WHERE id=? AND (id_solar_system IN (SELECT id_solar_system FROM kingdoms WHERE id=?) OR id_solar_system=0)", array($id, $idkingdom));
        if(count($array)>0)
            return $array[0];
    }

    public function grabMessages($channel, $last_update) {
        if(strlen($last_update)>0)
            $update = " AND time>'$last_update'";
        $sql = "SELECT chat_messages.*, kingdoms.name FROM chat_messages LEFT JOIN kingdoms ON kingdoms.id=chat_messages.idkingdom WHERE "
              ."idchannel=?".$update." ORDER BY chat_messages.time ASC LIMIT 100";
        $array = $this->execute($sql, array($channel));
        return $array;
    }
    public function sendMessage($channel, $kingdomid, $message) {
        $this->execute("INSERT INTO chat_messages (idchannel, idkingdom, text) VALUES (?, ?, ?)", array($channel, $kingdomid, $message));
        $id = $this->lastInsertId();
        if($id>0) {
            $time = $this->execute("SELECT time FROM chat_messages WHERE id=?", array($id));
            $this->execute("UPDATE chat_channels SET last_update=? WHERE id=?", array($time[0]["time"], $channel));
        }
    }
    public function isLoggedIn($id) {
        return isset($_SESSION["id"]) && $_SESSION["id"]==$id;
    }
    public function isLoggedInEmail($email) {
        $ids = $this->execute("SELECT kingdoms.id FROM users, kingdoms WHERE users.email=? && users.id=kingdoms.iduser", array($email));
        if(count($ids)==0)
                    return;
        $id = $ids[0]['id'];
        return $this->isLoggedIn($id);
    }

    public function getPlayers($kingdomid) {
        $ss = $this->execute("SELECT id_solar_system, `name` FROM solar_systems WHERE id_solar_system IN (SELECT id_solar_system FROM kingdoms WHERE id=?)", [$kingdomid]);
        $result = array("id" => "-1", "name"=>$ss[0]["name"]);
        $result["kingdoms"] = $this->execute("SELECT id, name, land FROM kingdoms WHERE id_solar_system = ? && id != ?", [$ss[0]["id_solar_system"], $kingdomid]);
        return $result;
    }

    public function getBattles($kingdomid) {
        $battles = $this->execute("SELECT * FROM battles WHERE idkingdom=?", array($kingdomid));
        foreach($battles as $key=>$battle) {
            $soldiers = $this->execute("SELECT soldiers.*, buildingtypes.name as building FROM soldiers LEFT JOIN buildingtypes ON soldiers.type=buildingtypes.id WHERE idbattle=?", array($battle["id"]));
            $battles[$key]["soldiers"] = $soldiers;
            $battles[$key]["sent"] = $battles[$key]["sent"]==1;
        }
        return $battles;
    }

    public function addBattle($kingdomid, $battle) {
        $this->execute("INSERT INTO battles (idkingdom, idfrom, idto, sent) VALUES (?, ?, ?, ?)", 
            array($kingdomid, $battle["idfrom"], $battle["idto"], $battle["sent"]));
        $battleid = $this->lastInsertId();
        foreach($battle["soldiers"] as $soldiers) {
            $this->execute("INSERT INTO soldiers (idbattle, type, number, advancement) VALUES (?, (SELECT id FROM buildingtypes WHERE name=?), ?, ?)",
                array($battleid, $soldiers["building"], $soldiers["number"], $soldiers["advancement"]));
        }
        $battle["id"] = $battleid;
        return $battle;
    }

    public function updateBattle($kingdomid, &$battle) {
        $battles = $this->execute("SELECT * FROM battles WHERE id=? AND idkingdom=?", array($battle["id"], $kingdomid));
        if(count($battles)==0)
            return false;
        $this->execute("UPDATE battles SET idfrom=?, idto=?, sent=? WHERE id=? AND idkingdom=?", 
            array($battle["idfrom"], $battle["idto"], $battle["sent"], $battle["id"], $kingdomid));
        $this->execute("DELETE FROM soldiers WHERE idbattle=?", array($battle["id"]));
        foreach($battle["soldiers"] as $soldiers) {
            $this->execute("INSERT INTO soldiers (idbattle, type, number, advancement) VALUES (?, (SELECT id FROM buildingtypes WHERE name=?), ?, ?)",
                array($battle["id"], $soldiers["building"], $soldiers["number"], $soldiers["advancement"]));
        }
        return true;
    }

    public function deleteBattle($kingdomid, &$battle) {
        $battles = $this->execute("SELECT * FROM battles WHERE id=? AND idkingdom=?", array($battle["id"], $kingdomid));
        if(count($battle)==0)
            return false;
        $this->execute("DELETE FROM battles WHERE id=?", array($battle["id"]));
        $this->execute("DELETE FROM soldiers WHERE idbattle=?", array($battle["id"]));
        /*foreach($battle["soldiers"] as $soldiers) {
            $this->execute("INSERT INTO soldiers (idbattle, type, number, advancement) VALUES (?, ?, ?, ?)", 
                array($battle["id"], $soldiers["type"], $soldiers["number"], $soldiers["advancement"]));
        }*/
        return true;
    }
    // test




    /*
    Result API: String message;
                List channels;
                List messages;
    */
                public function chatResult($type, $message, $channels, $channel, $messages) {
                    return array(
                        "RequestType"=>$type,
                        "Message"=>$message,
                        "Channels"=>$channels,
                        "Channel"=>$channel,
                        "Messages"=>$messages
                        );
                }

                public function diplomaticResult($type, $responsetype, $group, $groups, $battle, $battles) {
                    return array(
                        "requestType"=>$type,
                        "responseType"=>$responsetype,
                        "group"=>$group,
                        "groups"=>$groups,
                        "battle"=>$battle,
                        "battles"=>$battles
                        );
                }
}