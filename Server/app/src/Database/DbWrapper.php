<?php


namespace Database;

class DbWrapper {
	private static $functions;
	private static $data = [];

	function __get($name) {
		if(!isset(self::$functions)) {
			self::$functions = array(
				"Kingdom" => function() {return new Kingdom();},
				"Recount" => function() {return new Recount();},
				"Social" => function() {return new Social();},
				"Turn" => function() {return new Turn();},
				"News" => function() {return new News();}
			);
		}
		if(isset(self::$data[$name]))
			return self::$data[$name];
		if(isset(self::$functions[$name])) {
			$closure = self::$functions[$name];
			self::$data[$name] = $closure();
			return self::$data[$name];
		}
	}
	
    public function __construct() {
    }
}
