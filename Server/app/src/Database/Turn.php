<?php

namespace Database;

class Turn extends Database {

	private $Kingdom;
	private $unhappiness_reason = null;
	private $trigger_unhappiness = false;

	function __construct() {
		$this->Kingdom = (new DbWrapper())->Kingdom;
	}

	public function updateKingdom($turn) {
		if($turn["Recount"]!= $this->getConfig("recount.time"))
			return false;
		$kingdomname = $turn['KingdomName'];
		$kingdomidrow = $this->execute("SELECT kingdoms.id, kingdoms.idrace FROM users, kingdoms WHERE users.email=? AND kingdoms.name=?
			AND users.id=kingdoms.iduser", array($turn['Login'], $kingdomname));
		$kingdomid = $kingdomidrow[0]['id'];
		$raceid = $kingdomidrow[0]['idrace'];
		$sql = "UPDATE kingdoms SET turn = turn + 1 WHERE id=?";
		$this->execute($sql, array($turns, $kingdomid));
		foreach($turn['Buildings'] as $b) {
			$sql = "UPDATE buildings, buildingtypes SET buildings.number = buildings.number + ?,
			buildings.employment = buildings.employment + ?, buildings.advancement = buildings.advancement + ? 
			WHERE buildingtypes.name=? AND buildingtypes.id=buildings.idtype AND buildings.idkingdom=?";
			$params = array($b['Number'], $b['Employment'], $b['Advancement'], $b['Type'], $kingdomid);
			$this->execute($sql, $params);
			$sql = "UPDATE buildings_cache, buildingtypes SET buildings_cache.number =  ?,
			buildings_cache.employment =  ?, buildings_cache.advancement =  ? 
			WHERE buildingtypes.name=? AND buildingtypes.id=buildings_cache.idtype AND buildings_cache.idkingdom=?";
			$params = array($b['Number'], $b['Employment'], $b['Advancement'], $b['Type'], $kingdomid);
			$this->execute($sql, $params);
		}
		foreach($turn['Resources'] as $key=>$val) {
			$sql = "UPDATE resources, resourcetypes SET resources.number = resources.number + ?
			WHERE resourcetypes.name=? AND resourcetypes.id=resources.idresource AND resources.idkingdom=?";
			$params = array($val, $key, $kingdomid);
			$this->execute($sql, $params);
			$sql = "UPDATE resources_cache, resourcetypes SET resources_cache.number =  ?
			WHERE resourcetypes.name=? AND resourcetypes.id=resources_cache.idresource AND resources_cache.idkingdom=?";
			$params = array($val, $key, $kingdomid);
			$this->execute($sql, $params);
		}
		return true;
	}

	public function updateWithTurn($turn) { // no questions asked
        $kingdomid = $turn["Id"];
		foreach($turn['Buildings'] as $b) {
			$sql = "UPDATE buildings, buildingtypes SET buildings.number = buildings.number + ?,
			buildings.employment = buildings.employment + ?, buildings.advancement = buildings.advancement + ? 
			WHERE buildingtypes.name=? AND buildingtypes.id=buildings.idtype AND buildings.idkingdom=?";
			$params = array($b['Number'], $b['Employment'], $b['Advancement'], $b['Type'], $kingdomid);
			$this->execute($sql, $params);
			$sql = "UPDATE buildings_cache, buildingtypes SET buildings_cache.number =  ?,
			buildings_cache.employment =  ?, buildings_cache.advancement =  ? 
			WHERE buildingtypes.name=? AND buildingtypes.id=buildings_cache.idtype AND buildings_cache.idkingdom=?";
			$params = array($b['Number'], $b['Employment'], $b['Advancement'], $b['Type'], $kingdomid);
			$this->execute($sql, $params);
		}
		foreach($turn['Resources'] as $key=>$val) {
			$sql = "UPDATE resources, resourcetypes SET resources.number = resources.number + ?
			WHERE resourcetypes.name=? AND resourcetypes.id=resources.idresource AND resources.idkingdom=?";
			$params = array($val, $key, $kingdomid);
			$this->execute($sql, $params);
			$sql = "UPDATE resources_cache, resourcetypes SET resources_cache.number =  ?
			WHERE resourcetypes.name=? AND resourcetypes.id=resources_cache.idresource AND resources_cache.idkingdom=?";
			$params = array($val, $key, $kingdomid);
			$this->execute($sql, $params);
		}
	}

	private function updateTurnNumber($kingdom, $turn, $rules) {
        $kingdomid = $turn["Id"];
        if($this->trigger_unhappiness && $this->unhappiness_reason!=null) {
            $msg = t("turn")["unhappiness_reason"][$this->unhappiness_reason];
        }
        else {
            $msgs = t("turn")["neutral_turn"];
            $msg = $msgs[rand(0, count($msgs)-1)];
        }
        $turns = $rules["other"]["num_turns_per_recount"];
        if($turn["Turn"]==$turns-1) {
            $msgs = t("turn")["war_turn"];
            $msg = $msgs[rand(0, count($msgs)-1)];
        }
        if($turn["Turn"]==$turns-1 && $kingdom["Days_Old"]==0)
            $msg =  t("turn")["first_war_turn"];
        $sql = "UPDATE kingdoms SET turn = turn + 1, turn_message=? WHERE id=?";
        $this->execute($sql, array($msg, $kingdomid));
    }

	public function onTurn($turn) {
	    $this->unhappiness_reason = null;
		$kingdomid = $turn['Id'];
		$kingdom = $this->Kingdom->getKingdom($kingdomid);
		if($turn["Turn"]!=$kingdom["Turns"])
		    return false; // turn doesn't match, lets return correct one

		$Rules = $this->Kingdom->getRules($kingdom["Idrace"]);
		$msg = $this->executeTurn($kingdom, $turn, $Rules);
		if($msg!==false)
		    return $msg;
        $this->updateTurnNumber($kingdom, $turn, $Rules);
        $this->updateWithTurn($turn);
		return false;
	}

	public function executeTurn(&$kingdom, &$turn, &$Rules) {
        if(!$this->checkBasic($kingdom, $turn, $Rules))
            return "Basic test failed: Number of buildings or employees do not meet requirements OR turn is above the maximum";
        $Consumption = $this->generateConsumption($kingdom, $turn, $Rules);
        $Generated = $this->generateResources($kingdom, $Rules);
        $error = $this->calculateResourcesValidity($kingdom, $Generated, $Consumption, $Rules);
        if($error)
            return "Consumption failed: Values generated were less than 0 when illegal: $error";
        $this->changeResources($kingdom, $turn, $Generated, $Consumption);
        $this->updateOther($kingdom, $turn, $Rules);
        $this->updateAdvancement($kingdom, $turn, $Rules);
        return false;

    }

	private function getBuilding($name, $resource, $kingdom, $turn) {
		$num = 0;
		foreach($kingdom["Buildings"] as $b)
			if($b["Type"]==$name || $name==null)
				$num += $b[$resource];
		foreach($turn["Buildings"] as $b)
			if($b["Type"]==$name || $name==null)
				$num += $b[$resource];
		return $num;
	}
	private function getResource($name, $kingdom, $turn) {
		return $kingdom["Resources"][$name] + $turn["Resources"][$name];
	}

	public function updateOther($kingdom, &$turn, $rules) {
		// for happiness, people, (land) and what to do with negative amounts
		// take note, that turn contains all of the changes in resources, including negative ones
		// happiness
		$scarcity = 0;
		$employees = $this->getBuilding(null, "Employment", $kingdom, $turn);
		$employment = $employees *1.0 /$kingdom["Resources"]["People"]; // this many people are affected by gold fluctuations
        $unhappy = 0;
		foreach($turn["Resources"] as $Res=>$_) {
			$resval = $this->getResource($Res, $kingdom, $turn);
			if($resval<0) {
			    $def = 0;
				if($rules["turn_cost"]["Employee"][$Res]>0)
					$def -= $employment*$rules["happiness"]["deficiency_decay"][$Res]*$resval/$rules["turn_cost"]["Employee"][$Res];
				if($rules["turn_cost"]["People"][$Res]>0)
					$def -= $rules["happiness"]["deficiency_decay"][$Res]*$resval/$rules["turn_cost"]["People"][$Res];
                $turn["Resources"][$Res] = 0;
			}
			if($def>$unhappy) {
                $this->unhappiness_reason = $Res;
                $unhappy = $def;
            }
			$scarcity += $def;
		}
		$happy = ($kingdom["Resources"]["People"] - $scarcity)/$kingdom["Resources"]["People"];
		$diff = ($employment - $rules["happiness"]["other"]["employment_optimum"]);
		if(abs($diff*$employees*$rules["happiness"]["other"]["employment_influence"])>$unhappy && abs($diff)>0.05) { // threshold
		    $unhappy = abs($diff*$employees*$rules["happiness"]["other"]["employment_influence"]);
		    $this->unhappiness_reason = $diff>0?"high_employment":"low_employment";
        }
		$happiness_max = 1 - abs($diff*$rules["happiness"]["other"]["employment_influence"]);
		$happiness_max *= $happy;
		// scarcity represents approximately how many people are unhappy by the lacks in gold
		$happ_change  = ($happiness_max*1000.0 - $kingdom["Resources"]["Happiness"])*$rules["happiness"]["other"]["change_between_max_and_now"];
		$happiness_now = $kingdom["Resources"]["Happiness"] + $happ_change;
		$houses = $this->getBuilding("House", "Number", $kingdom, $turn);
		$people_max = ($happiness_now/1000.0)
							*$rules["happiness"]["other"]["happiness_house_mult"]
							*$rules["happiness"]["other"]["house_houses"]
							*$houses
							+ $rules["happiness"]["other"]["people_land_constant"]
							*$this->getResource("Land", $kingdom, $turn);
		$people_change = $people_max - $kingdom["Resources"]["People"];
		if($people_change>0)
			$people_change *= $rules["happiness"]["other"]["people_delay_plus"];
		else
			$people_change *= $rules["happiness"]["other"]["people_delay_minus"];
		if($people_change<=-10)
            $this->trigger_unhappiness = true;
		$turn["Resources"]["People"] = $people_change;
		$turn["Resources"]["Happiness"] = $happ_change;
		if($people_change<0)
		    $this->fireEmployeesAtRandom($kingdom, $turn);
	}

	public function countUnemployed($kingdom,  $turn){
	    $sum = 0;
        foreach($kingdom["Buildings"] as $i=>$b) {
            $proportions[$b["Type"]] = $b["Employment"];
            $sum += $b["Employment"];
        }
        foreach($turn["Buildings"] as $i=>$b) {
            $proportions[$b["Type"]] += $b["Employment"];
            $sum += $b["Employment"];
        }
        $people = $kingdom["Resources"]["People"] - $sum;
        return $people;
    }

	public function fireEmployeesAtRandom($kingdom, &$turn) { // also proportional
	    $proportions = array();
        $people = $kingdom["Resources"]["People"];
        $people_change = round($turn["Resources"]["People"]);
        $sum = 0;
        $proportions["Unemployed"] = $people;
        foreach($kingdom["Buildings"] as $i=>$b) {
            $proportions[$b["Type"]] = $b["Employment"];
            $sum += $b["Employment"];
        }
        foreach($turn["Buildings"] as $i=>$b) {
            $proportions[$b["Type"]] += $b["Employment"];
            $sum += $b["Employment"];
        }
        $proportions["Unemployed"] -= $sum;
        $after_change = $people_change;
        foreach($proportions as $type=>$value) {
            $val = ((int)($value*$people_change/$people));
            foreach($turn["Buildings"] as $key=>$b)
                if($b["Type"]==$type)
                    $turn["Buildings"][$key]["Employment"] +=$val;
            $after_change -= $val; // -i := -i - (-j) = -i + j
        }
        $max = 100; // max 100 tries (we have ~11 jobs, so it should be done in 11 iterations
        while($after_change<0 && $max>0) {
            $rand = random();
            $was = 0;
            $type = null;
            foreach($proportions as $key=>$value) {
                $was += ((float)$value)/$people;
                if($was>$rand && $type==null) {
                    $type = $key;
                }
            }
            if($type=="Unemployed") {
                if($this->countUnemployed($kingdom, $turn)>0) // i dont know if it works
                    $after_change++;
            } else {
                foreach ($kingdom["Buildings"] as $b) {
                    foreach ($turn["Buildings"] as $k=>$b2) {
                        if ($b["Type"] == $b2["Type"] && $b2["Type"] == $type && $b["Employment"] + $b2["Employment"]>0) {
                            $turn["Buildings"][$k]["Employment"]--;
                            $after_change++;
                        }
                    }
                }
            }
            $max--;
        }
    }

// checks if it's possible to employ these employees
	public function checkBasic($kingdom, $turn, $rules) {
	    $employees_per_building = $rules["other"]["employees_per_building"];
	    $turns = $rules["other"]["num_turns_per_recount"];
	    if($turn["Turn"]==$turns)
	        return false;
		$people = $kingdom["Resources"]["People"];
		$land = $kingdom["Resources"]["Land"];
		$sum = 0;
		$sum_b = 0;
		foreach($kingdom["Buildings"] as $building) {
			foreach($turn["Buildings"] as $building2) {
				if($building["Type"]==$building2["Type"]) {
					$e = $building["Employment"]+$building2["Employment"];
					$b = $building["Number"]+$building2["Number"];
					if($e<0 || $b<0 || $e > $b*$employees_per_building)
						return false;
					$sum += $e;
					$sum_b += $b;
				}
			}
		}
		if($sum>$people || $sum_b>$land || $sum<0 || $sum_b<0)
			return false;
		return true;
	}

	public function generateConsumption($kingdom, $turn, $rules) {
		$cost = [];
		if(is_array($rules["turn_cost"]["Person"]))
			foreach($rules["turn_cost"]["Person"] as $res=>$value) // jedzenie na ture
					$cost[$rules["consumed_resources"][$res]][$res] += $kingdom["Resources"]["People"]*$value;

		foreach($kingdom["Buildings"] as $building) {
			if(is_array($rules["turn_cost"]["Employee"]))
				foreach($rules["turn_cost"]["Employee"] as $res=>$value) // gold na ture
					$cost[$rules["consumed_resources"][$res]][$res] += $building["Employment"]*$value;
		}
		foreach($turn["Buildings"] as $building) {
			if(is_array($rules["cost_add_building"]["Building"]))
				foreach($rules["cost_add_building"]["Building"] as $res=>$value) // zasoby per budynek
					$cost[$rules["consumed_resources"][$res]][$res] += $building["Number"]>0?$building["Number"]*$value:0;
			if(is_array($rules["cost_add_building"][$building["Type"]]))
				foreach($rules["cost_add_building"][$building["Type"]] as $res=>$value) // zasoby per budynek (typ)
					$cost[$rules["consumed_resources"][$res]][$res] += $building["Number"]>0?$building["Number"]*$value:0;
			if(is_array($rules["cost_add_employee"]["Employee"]))
				foreach($rules["cost_add_employee"]["Employee"] as $res=>$value) // zasoby per pracownik
					$cost[$rules["consumed_resources"][$res]][$res] += $building["Employment"]>0?$building["Employment"]*$value:0;
			if(is_array($rules["cost_add_employee"][$building["Type"]]))
				foreach($rules["cost_add_employee"][$building["Type"]] as $res=>$value) // zasoby per nowy pracownik (typ)
					$cost[$rules["consumed_resources"][$res]][$res] += $building["Employment"]>0?$building["Employment"]*$value:0;
		}
		// buyland
        $x = $rules["other"]["price_for_soil_per_soil"];
        $land = $kingdom["Resources"]["Land"];
        $land_plus = $turn["Resources"]["Land"];
        $land = $land>0?$land:0; // negative value?!
        $gold_consumed = (2*$land + $land_plus + 1)*$land_plus*$x/2;
        $cost[$rules["consumed_resources"]["Gold"]]["Gold"] += $gold_consumed;
        // end buyland
		return $cost;
	}


	public function changeResources($kingdom, &$turn, $generated, $consumed) {
		foreach($generated as $consumable=>$R) {
			foreach($R as $name => $value) {
				if($consumable=="0") // non consumable - difference between what was
					$turn["Resources"][$name] = $generated[$consumable][$name] - $kingdom["Resources"][$name];
				else // consumable - remove added 
					$turn["Resources"][$name] = $generated[$consumable][$name] - $consumed[$consumable][$name];
			}
		}
		return false;
	}

	public function calculateResourcesValidity($kingdom, $generated, $consumed, $rules) {
        foreach($generated as $consumable=>$R) {
            foreach($R as $name => $value) {
                $val = $generated[$consumable][$name] - $consumed[$consumable][$name];
                if($consumable=="1")
                    $val += $kingdom["Resources"][$name];
                $val = (int)$val;
                if($val<0 && $rules["can_be_sub_zero"][$name]==0)
                    return "$name is $val";
            }
        }
        return false;
    }

	// prepares all resources to be consumed
	public function generateResources($kingdom, $rules) {
		$result = [];
		foreach($kingdom["Buildings"] as $b) {
			$Type = $b["Type"];
			if(is_array($rules["employees_earn"][$Type]))
				foreach($rules["employees_earn"][$Type] as $Resource=>$rate) {
					$result[$rules["consumed_resources"][$Resource]][$Resource] += $b["Employment"]*$b["Advancement"]*$rate + $rules["employees_constant"][$Resource];
			}
		}
		return $result;
	}

	public function updateAdvancement($kingdom, &$turn, $rules) {
		$science = $kingdom["Resources"]["Science"];
		$influence = $science  / log($kingdom["Resources"]["People"], $rules["advancement"]["log_N_pop"]); // hidden influence of population size
		$inf =  $influence / $kingdom["Resources"]["People"];
		$max = $inf + 1;
		foreach($kingdom["Buildings"] as $building) {
			$changes = null;
			$experienced = $building["Advancement"]*$building["Employment"];
			$max_exp = $max * $building["Employment"];
			foreach($turn["Buildings"] as $row_num=>$building2) {
				if($building["Type"] == $building2["Type"]) {
					if($building["Employment"]==0) {
						$turn["Buildings"][$row_num]["Advancement"] = $rules["advancement"]["advancement_of_new_employee"] - $building["Advancement"];
						break;
					}
					$changes = $building2;
					if($changes["Number"]>0) // adding
						$experienced += $rules["advancement"]["advancement_of_new_employee"]*$changes["Number"];
					else { // removing: 
						$experienced -= $building["Advancement"]*$changes["Number"];
					}
					if($experienced>$max_exp)
						$experienced = $rules["advancement"]["overadvancement_decay"]*($experienced - $max_exp) + $max_exp;
					else
						$experienced += $inf * ($max_exp - $experienced);
					$new_adv = $experienced * 1.0 / $building["Employment"];
					$diff = $new_adv - $building["Advancement"];
					$turn["Buildings"][$row_num]["Advancement"] = $diff;
				}
			}
		}
	}

}
