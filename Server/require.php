<?php

require __DIR__ . '/vendor/autoload.php';
define('BULLET_ROOT', __DIR__);
define('BULLET_APP_ROOT', BULLET_ROOT . '/app/');
define('BULLET_SRC_ROOT', BULLET_APP_ROOT . '/src/');
$int = strrpos($_SERVER['REQUEST_URI'], $_GET["u"]);
$int2 = strrpos($_SERVER['REQUEST_URI'], "?");
$base_url = substr($_SERVER['REQUEST_URI'], 0, $int);
$base_url2 = substr($_SERVER['REQUEST_URI'], 0, $int2-1);
if(substr($base_url, -1)=="/")
    $base_url = substr($base_url, 0,-1);
if(substr($base_url2, -1)=="/")
    $base_url2 = substr($base_url2, 0,-1);
if($int!==false)
    define('BULLET_BASE_URL', $base_url);
else if ($int2!==false)
    define('BULLET_BASE_URL', $base_url2);
else {
    if(substr($_SERVER['REQUEST_URI'], -1)=="/") 
        define('BULLET_BASE_URL', substr($_SERVER['REQUEST_URI'], 0,-1));
    else
        define('BULLET_BASE_URL', $_SERVER['REQUEST_URI']);
}
$loader = require BULLET_ROOT . '/vendor/autoload.php';
$local = require(BULLET_ROOT."/app/config/local.php");
$GLOBALS = array_merge($GLOBALS, $local);
// Bullet App
$app = new Bullet\App(require BULLET_APP_ROOT . 'config.php');
$request = new Bullet\Request();
global $DB;
$DB = new Database\DbWrapper();
// Common include

$app["url"] = function($app) {
    return function($url) {return BULLET_BASE_URL.$url;};
};

require BULLET_APP_ROOT . '/common.php';
$routesDir = BULLET_APP_ROOT . '/routes/';
require $routesDir . 'index.php';
require $routesDir . 'api.php';